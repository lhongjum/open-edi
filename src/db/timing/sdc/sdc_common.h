/**
 * @file sdc_common.h
 * @date 2020-11-25
 * @brief
 *
 * Copyright (C) 2020 NIIC EDA
 *
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 *
 * of the BSD license.  See the LICENSE file for details.
 */

#ifndef EDI_DB_TIMING_SDC_SDC_COMMON_H_
#define EDI_DB_TIMING_SDC_SDC_COMMON_H_

#include "db/core/object.h"
#include "db/core/db.h"
#include "util/data_traits.h"
#include "util/util.h"
#include "db/timing/sdc/clock.h"
#include "db/timing/timinglib/libset.h"
#include "db/timing/timinglib/timinglib_cell.h"
#include "db/timing/timinglib/analysis_corner.h"
#include "db/timing/timinglib/timinglib_term.h"
#include "db/timing/timinglib/timinglib_lib.h"
#include <cmath>
#include <limits>
#include <boost/functional/hash.hpp>
#include <unordered_set>
#include <unordered_map>

namespace open_edi {
namespace db {

template<typename T, typename U>
class UnorderedPair {
  public:
    UnorderedPair() {}
    UnorderedPair(T first_value, U second_value) : first(first_value), second(second_value) {}
    bool operator==(const UnorderedPair<T, U> &rhs) const;
    template<T, U>
    friend std::size_t hash_value(const UnorderedPair<T, U> &p);

    //keep the same usage as std::pair
    T first;
    U second;
};

template<typename T, typename U>
bool UnorderedPair<T,U>::operator==(const UnorderedPair<T, U> &rhs) const {
    if ((this->first == rhs.first) and (this->second == rhs.second)) {
        return true;
    }
    if ((this->first == rhs.second) and (this->second == rhs.first )) {
        return true;
    }
    return false;
}

template<typename T, typename U>
std::size_t hash_value(const UnorderedPair<T, U> &p) {
    std::size_t seed1 = 0;
    boost::hash_combine(seed1, p.first);
    std::size_t seed2 = 0;
    boost::hash_combine(seed2, p.second);
    std::size_t seed = (seed1 xor seed2) + p.first + p.second;
    return seed;
}

template <typename T>
bool ediEqual(const T &value1, const T &value2, T eps = std::numeric_limits<T>::epsilon()) {
    return fabs(value1 - value2) < eps;
}

Pin* getPinByFullName(const std::string &full_name);
std::string getPinFullName(const ObjectId &pin_id);

bool isCellInOtherCell(const ObjectId &cell_id, const ObjectId &other_cell_id);
bool isInstInCell(const ObjectId &inst_id, const ObjectId &cell_id);

TCell* getLibCellInCorner(const ObjectId& analysis_corner_id, const std::string &liberty_name, const std::string &cell_name);

void getSdcFileContents(std::string &contents, const std::vector<std::string> &files,  const std::string &mode_name);

class ContainerDataAccess {
  public:
    template<typename U>
    static bool hasLowerInsertPriority(const U &v1, const U &v2) { return v1 == v2; }

    template<typename U>
    static bool hasLowerInsertPriority(const std::shared_ptr<U> &v1, const std::shared_ptr<U> &v2) { return (*v1 == *v2); }

    template<typename U, typename K, typename V>
    static bool updateMapValue(U &map_like, const K &key, const V &value);

    template<typename U, typename V>
    static bool addToPinValueMap(U &pin_map, const std::string &pin_name, const V &value);

    template<typename U, typename V>
    static bool addToPortValueMap(U &port_map, const std::string &port_name, const V &value);

    template<typename U, typename V>
    static bool addToInstValueMap(U &inst_map, const std::string &inst_name, const V &value);

    template<typename U, typename V>
    static bool addToCellValueMap(U &cell_map, const std::string &cell_name, const V &value);

    template<typename U, typename V>
    static bool addToNetValueMap(U &net_map, const std::string &net_name, const V &value);

    template<typename U, typename V>
    static bool addToPowerNetValueMap(U &net_map, const std::string &net_name, const V &value);

    template<typename U, typename V>
    static bool addToClockValueMap(U &clock_map, const ClockId &clock_id, const V &value);

    template<typename U, typename V>
    static bool addToCurrentDesignValueMap(U &design_map, const ObjectId &cell_id, const V &value);

    template<typename U>
    static bool addToPinSet(U &pin_set, const std::string &pin_name);

    template<typename U>
    static bool addToPortSet(U &port_set, const std::string &port_name);

    template<typename U>
    static bool addToInstSet(U &inst_set, const std::string &inst_name);

    template<typename U>
    static bool addToCellSet(U &cell_set, const std::string &cell_name);

    template<typename U>
    static bool addToNetSet(U &net_set, const std::string &net_name);

    template<typename U>
    static bool addToClockSet(U &clock_set, const ClockId &clock_id);

    template<typename U>
    static bool addToCurrentDesignSet(U &design_set, const ObjectId &cell_id);

};

template<typename U, typename K, typename V>
bool ContainerDataAccess::updateMapValue(U &map_like, const K &key, const V &value) {
    auto range = map_like.equal_range(key);
    if (range.first == map_like.end()) {
        map_like.emplace(key, value);
        return true;
    }
    for (auto it = range.first; it != range.second; ++it) {
        const auto &exist_value = it->second;
        if (hasLowerInsertPriority<V>(exist_value, value)) {
            //exist value has lower priority
            it->second = value;
            return true;
        }
    }
    return false;
}

template<typename U, typename V>
bool ContainerDataAccess::addToPinValueMap(U &pin_map, const std::string &pin_name, const V &value) {
    const auto &pin = getPinByFullName(pin_name);
    if (!pin) {
        return false;
    }
    const auto &pin_id = pin->getId();
    return updateMapValue(pin_map, pin_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToPortValueMap(U &port_map, const std::string &port_name, const V &value) {
    const auto &top_cell = getTopCell();
    const auto &port = top_cell->getIOPin(port_name);
    if (!port) {
        return false;
    }
    const auto &port_id = port->getId();
    return updateMapValue(port_map, port_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToInstValueMap(U &inst_map, const std::string &inst_name, const V &value) {
    const auto &top_cell = getTopCell();
    const auto &inst = top_cell->getInstance(inst_name);
    if (!inst) {
        return false;
    }
    const auto &inst_id = inst->getId();
    return updateMapValue(inst_map, inst_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToCellValueMap(U &cell_map, const std::string &cell_name, const V &value) {
    const auto &top_cell = getTopCell();
    const auto &cell = top_cell->getCell(cell_name);
    if (!cell) {
        return false;
    }
    const auto &cell_id = cell->getId();
    return updateMapValue(cell_map, cell_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToNetValueMap(U &net_map, const std::string &net_name, const V &value) {
    const auto &top_cell = getTopCell();
    const auto &net = top_cell->getNet(net_name);
    if (!net) {
        return false;
    }
    const auto &net_id = net->getId();
    return updateMapValue(net_map, net_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToPowerNetValueMap(U &net_map, const std::string &net_name, const V &value) {
    const auto &top_cell = getTopCell();
    const auto &net = top_cell->getSpecialNet(net_name);
    if (!net) {
        return false;
    }
    const auto &net_id = net->getId();
    return updateMapValue(net_map, net_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToClockValueMap(U &clock_map, const ClockId &clock_id, const V &value) {
    return updateMapValue(clock_map, clock_id, value);
}

template<typename U, typename V>
bool ContainerDataAccess::addToCurrentDesignValueMap(U &design_map, const ObjectId &cell_id, const V &value) {
    return updateMapValue(design_map, cell_id, value);
}

template<typename U>
bool ContainerDataAccess::addToPinSet(U &pin_set, const std::string &pin_name) {
    const auto &pin = getPinByFullName(pin_name);
    if (!pin) {
        return false;
    }
    pin_set.emplace(pin->getId());
    return true;
}

template<typename U>
bool ContainerDataAccess::addToPortSet(U &port_set, const std::string &port_name) {
    const auto &top_cell = getTopCell();
    const auto &port = top_cell->getIOPin(port_name);
    if (!port) {
        return false;
    }
    port_set.emplace(port->getId());
    return true;
}

template<typename U>
bool ContainerDataAccess::addToInstSet(U &inst_set, const std::string &inst_name) {
    const auto &top_cell = getTopCell();
    const auto &inst = top_cell->getInstance(inst_name);
    if (!inst) {
        return false;
    }
    inst_set.emplace(inst->getId());
    return true;
}

template<typename U>
bool ContainerDataAccess::addToCellSet(U &cell_set, const std::string &cell_name) {
    const auto &top_cell = getTopCell();
    const auto &cell = top_cell->getCell(cell_name);
    if (!cell) {
        return false;
    }
    cell_set.emplace(cell->getId());
    return true;
}

template<typename U>
bool ContainerDataAccess::addToNetSet(U &net_set, const std::string &net_name) {
    const auto &top_cell = getTopCell();
    const auto &net = top_cell->getNet(net_name);
    if (!net) {
        return false;
    }
    net_set.emplace(net->getId());
    return true;
}

template<typename U>
bool ContainerDataAccess::addToClockSet(U &clock_set, const ClockId &clock_id) {
    clock_set.emplace(clock_id);
    return true;
}

template<typename U>
bool addToCurrentDesignSet(U &design_set, const ObjectId &cell_id) {
    design_set.emplace(cell_id);
    return true;
}

using Flag = std::pair<std::string, bool>;
class ContainerDataPrint {
  public:
    template<typename U>
    static std::string idToName(const ObjectId &id);

    template<typename U>
    static std::string idVectorToNameList(const std::vector<ObjectId> &ids);

    static std::string pinIdToName(const ObjectId &pin_id);
    static std::string termIdToName(const ObjectId &term_id);
    static std::string cellIdToName(const ObjectId &cell_id);
    static std::string instIdToName(const ObjectId &inst_id);
    static std::string pinIdToFullName(const ObjectId &pin_id);

    static std::string pinIdVectorToNameList(const std::vector<ObjectId> &pin_ids);
    static std::string termIdVectorToNameList(const std::vector<ObjectId> &term_ids);
    static std::string cellIdVectorToNameList(const std::vector<ObjectId> &cell_ids);
    static std::string instIdVectorToNameList(const std::vector<ObjectId> &inst_ids);
    static std::string pinIdVectorToFullNameList(const std::vector<ObjectId> &pin_ids);

    static std::string getFlag(const Flag &p);

    template<typename ... Types>
    static std::string getFlag(const Flag &first, Types ... rest);
};

template<typename U>
std::string ContainerDataPrint::idToName(const ObjectId &id) {
    const auto &object = Object::addr<U>(id);
    if (!object) {
        return "";
    }
    return object->getName();
}

template<typename U>
std::string ContainerDataPrint::idVectorToNameList(const std::vector<ObjectId> &ids) {
    std::string ret = "";
    for (const ObjectId &id: ids) {
        ret += idToName<U>(id);
    }
    return ret;
}

template<typename ... Types>
std::string ContainerDataPrint::getFlag (const Flag &first, Types ... rest) {
    return getFlag(first) + " " + getFlag(rest...) + " ";
}


}
}

#endif //EDI_DB_TIMING_SDC_SDC_COMMON_H_
