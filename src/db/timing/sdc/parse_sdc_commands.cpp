/**
 * @file parse_sdc_commands.cpp
 * @date 2020-11-25
 * @brief
 *
 * Copyright (C) 2020 NIIC EDA
 *
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 *
 * of the BSD license.  See the LICENSE file for details.
 */

#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/timing/sdc/parse_sdc_commands.h"
#include "db/timing/sdc/sdc.h"
#include "db/timing/timinglib/analysis_view.h"
#include "db/timing/timinglib/analysis_mode.h"
#include "infra/command.h"
#include "infra/command_manager.h"

namespace open_edi {
namespace db {

using Command = open_edi::infra::Command;
using CommandManager = open_edi::infra::CommandManager;

AnalysisMode* getOrCreateDefaultMode() {
    Timing *timingdb = getTimingLib();
    std::string default_name = "default";
    auto view = timingdb->getAnalysisView(default_name);
    AnalysisMode *mode = nullptr;
    if (!view) {
        mode = timingdb->createAnalysisMode(default_name);
        if (!mode) {
            //TODO messages
            return nullptr;
        }
        auto corner = timingdb->createAnalysisCorner(default_name);
        if (!corner) {
            //TODO messages
            return nullptr;
        }
        view = timingdb->createAnalysisView(default_name);
        if (!view) {
            //TODO messages
            return nullptr;
        }
        view->setAnalysisMode(mode->getId());
        view->setAnalysisCorner(corner->getId());
        view->setActive(true);
        view->setSetup(true);
        view->setHold(true);
        timingdb->addActiveSetupView(view->getId());
        timingdb->addActiveHoldView(view->getId());
        mode->createSdc();
    } else {
        mode = view->getAnalysisMode();
        assert(mode);
        if (!mode) {
            // TODO error messages
            return nullptr;
        }
    }
    return mode;
}

AnalysisMode* getOrCreateModeForSdc(const std::string &analysis_mode_name) {
    Timing *timingdb = getTimingLib();
    if (!timingdb) {
        //TODO messages
        return nullptr;
    }
    AnalysisMode *mode = nullptr;
    if (analysis_mode_name == "") {
        mode = getOrCreateDefaultMode();
    } else {
        mode = timingdb->getAnalysisMode(analysis_mode_name);
        if (!mode) {
            //TODO messages
            return nullptr;
        }
    }
    return mode;
}

SdcPtr getSdcFromCmd(Command* cmd) {
    std::string analysis_mode_name = "";
    if (cmd->isOptionSet("-mode")) {
        bool res = cmd->getOptionValue("-mode", analysis_mode_name);
        if (!res) {
            //TODO messages
            return nullptr;
        }
    }
    AnalysisMode* mode = getOrCreateModeForSdc(analysis_mode_name);
    if (!mode) {
        //TODO messages
        return nullptr;
    }
    return mode->getSdc();
}

// general purpose commands parser

int parseSdcCurrentInstance(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getCurrentInstanceContainer();
    auto inst = container->getData();
    std::string dir = "";
    if (cmd->isOptionSet("instance")) {
        bool res = cmd->getOptionValue("instance", dir);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    const auto &design_container = sdc->getCurrentDesignContainer();
    bool success = inst->cd(design_container->getDesignId(), dir);
    if (!success) {
        message->info("%s\n", (design_container->getDesignName()).c_str());
        return TCL_ERROR;
    }
    message->info("%s\n", (container->getInstName()).c_str());

    return TCL_OK;
}

int parseSdcSetUnits(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getUnitsContainer();
    auto units = container->getData();
    if (cmd->isOptionSet("-capacitance")) {
        std::string capacitance = "";
        bool res = cmd->getOptionValue("-capacitance", capacitance);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        units->setAndCheckCapacitance(capacitance);
    }
    if (cmd->isOptionSet("-resistance")) {
        std::string resistance = "";
        bool res = cmd->getOptionValue("-resistance", resistance);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        units->setAndCheckResistance(resistance);
    }
    if (cmd->isOptionSet("-time")) {
        std::string time = "";
        bool res = cmd->getOptionValue("-time", time);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        units->setAndCheckTime(time);
    }
    if (cmd->isOptionSet("-voltage")) {
        std::string voltage = "";
        bool res = cmd->getOptionValue("-voltage", voltage);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        units->setAndCheckVoltage(voltage);
    }
    if (cmd->isOptionSet("-current")) {
        std::string current = "";
        bool res = cmd->getOptionValue("-current", current);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        units->setAndCheckCurrent(current);
    }
    if (cmd->isOptionSet("-power")) {
        std::string power = "";
        bool res = cmd->getOptionValue("-power", power);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        units->setAndCheckPower(power);
    }
    return TCL_OK;
}

int parseSdcSetHierarchySeparator(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getHierarchySeparatorContainer();
    auto separator = container->getData();
    if (!(cmd->isOptionSet("separator"))) {
        //TODO messages
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("separator")) {
        std::string separator_str = "";
        bool res = cmd->getOptionValue("separator", separator_str);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool success = separator->setAndCheck(separator_str);
        if (!success) {
            //error message
            return TCL_ERROR;
        }
    }
    return TCL_OK;
}

// object access commands
int parseSdcAllClocks(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto &container = sdc->getAllClocksContainer();
    const auto &clock_container = sdc->getClockContainer();
    container->setData(clock_container);
    std::vector<std::string> clock_names;
    container->get_all_clock_names(clock_names);
    std::string output = "";
    output.reserve(10000);
    for (const auto &name : clock_names) {
        output.append(name).append(" ");
    }
    message->info("%s\n", output.c_str());
    return TCL_OK;
}

int parseSdcAllInputs(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-level_sensitive") and cmd->isOptionSet("-edge_triggered"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-level_sensitive")) {
        bool level_sensitive = false;
        bool res = cmd->getOptionValue("-level_sensitive", level_sensitive);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", level_sensitive);
    }
    if (cmd->isOptionSet("-edge_triggered")) {
        bool edge_triggered = false;
        bool res = cmd->getOptionValue("-edge_triggered", edge_triggered);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", edge_triggered);
    }

    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock;
        bool res = cmd->getOptionValue("-clock", clock);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& clock_name : clock) {

            message->info("get value %s \n", clock_name.c_str());
        }
    }
    return TCL_OK;
}

int parseSdcAllOutputs(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-level_sensitive") and cmd->isOptionSet("-edge_triggered"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-level_sensitive")) {
        bool level_sensitive = false;
        bool res = cmd->getOptionValue("-level_sensitive", level_sensitive);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", level_sensitive);
    }

    if (cmd->isOptionSet("-edge_triggered")) {
        bool edge_triggered = false;
        bool res = cmd->getOptionValue("-edge_triggered", edge_triggered);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", edge_triggered);
    }

    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock;
        bool res = cmd->getOptionValue("-clock", clock);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& clock_name : clock) {
            message->info("get value %s \n", clock_name.c_str());
        }
    }
    return TCL_OK;
}

int parseSdcAllRegisters(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (cmd->isOptionSet("-no_hierarchy")) {
        bool no_hierarchy = false;
        bool res = cmd->getOptionValue("-no_hierarchy", no_hierarchy);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", no_hierarchy);
    }
    if (cmd->isOptionSet("-cells")) {
        bool cells = false;
        bool res = cmd->getOptionValue("-cells", cells);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", cells);
    }
    if (cmd->isOptionSet("-data_pins")) {
        bool data_pins = false;
        bool res = cmd->getOptionValue("-data_pins", data_pins);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", data_pins);
    }
    if (cmd->isOptionSet("-clock_pins")) {
        bool clock_pins = false;
        bool res = cmd->getOptionValue("-clock_pins", clock_pins);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", clock_pins);
    }
    if (cmd->isOptionSet("-slave_clock_pins")) {
        bool slave_clock_pins = false;
        bool res = cmd->getOptionValue("-slave_clock_pins", slave_clock_pins);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", slave_clock_pins);
    }
    if (cmd->isOptionSet("-async_pins")) {
        bool async_pins = false;
        bool res = cmd->getOptionValue("-async_pins", async_pins);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", async_pins);
    }
    if (cmd->isOptionSet("-output_pins")) {
        bool output_pins = false;
        bool res = cmd->getOptionValue("-output_pins", output_pins);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", output_pins);
    }
    if (cmd->isOptionSet("-level_sensitive")) {
        bool level_sensitive = false;
        bool res = cmd->getOptionValue("-level_sensitive", level_sensitive);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", level_sensitive);
    }
    if (cmd->isOptionSet("-edge_triggered")) {
        bool edge_triggered = false;
        bool res = cmd->getOptionValue("-edge_triggered", edge_triggered);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", edge_triggered);
    }
    if (cmd->isOptionSet("-master_slave")) {
        bool master_slave = false;
        bool res = cmd->getOptionValue("-master_slave", master_slave);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", master_slave);
    }
    if (cmd->isOptionSet("-hsc")) {
        std::string hsc = "";
        bool res = cmd->getOptionValue("-hsc", hsc);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", hsc.c_str());
    }
    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock;
        bool res = cmd->getOptionValue("-clock", clock);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& clock_name : clock) {
            message->info("get value %s \n", clock_name.c_str());
        }
    }

    if (cmd->isOptionSet("-rise_clock")) {
        std::vector<std::string> rise_clock;
        bool res = cmd->getOptionValue("-rise_clock", rise_clock);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& rise_clock_name : rise_clock) {
            message->info("get value %s \n", rise_clock_name.c_str());
        }
    }
    if (cmd->isOptionSet("-fall_clock")) {
        std::vector<std::string> fall_clock;
        bool res = cmd->getOptionValue("-fall_clock", fall_clock);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& fall_clock_name : fall_clock) {
            message->info("get value %s \n", fall_clock_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcCurrentDesign(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getCurrentDesignContainer();
    auto current_design = container->getData();
    std::string hier_cell_name = "";
    if (cmd->isOptionSet("design_name")) {
        bool res = cmd->getOptionValue("design_name", hier_cell_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    } else {
        message->info("%s\n", (container->getDesignName()).c_str());
        return TCL_OK;
    }
    bool success = current_design->cd(hier_cell_name);
    if (!success) {
        //TODO error messages;
        return TCL_ERROR;
    }
    message->info("%s\n", (container->getDesignName()).c_str());
    return TCL_OK;
}

int parseSdcGetCells(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if(!((cmd->isOptionSet("patterns") and !(cmd->isOptionSet("-of_objects"))) or 
        (cmd->isOptionSet("-of_objects") and !(cmd->isOptionSet("-of_objects"))) or 
        !(cmd->isOptionSet("patterns")))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-hsc")) {
        std::string hsc = "";
        bool res = cmd->getOptionValue("-hsc", hsc);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", hsc.c_str());
    }
    if (cmd->isOptionSet("-hierarchical")) {
        bool hierarchical = false;
        bool res = cmd->getOptionValue("-hierarchical", hierarchical);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", hierarchical);
    }
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-of_objects")) {
        std::vector<std::string> of_objects;
        bool res = cmd->getOptionValue("-of_objects", of_objects);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& of_objects_name : of_objects) {
            message->info("get value %s \n", of_objects_name.c_str());
        }
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcGetClocks(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcGetLibCells(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("patterns"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-hsc")) {
        std::string hsc = "";
        bool res = cmd->getOptionValue("-hsc", hsc);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", hsc.c_str());
    }
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcGetLibPins(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("patterns"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcGetLibs(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }
    return TCL_OK;
}

int parseSdcGetNets(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!((cmd->isOptionSet("patterns") and !(cmd->isOptionSet("-of_objects"))) or 
        (cmd->isOptionSet("-of_objects") and !(cmd->isOptionSet("-of_objects"))) or 
        !(cmd->isOptionSet("patterns")))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-hsc")) {
        std::string hsc = "";
        bool res = cmd->getOptionValue("-hsc", hsc);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", hsc.c_str());
    }
    if (cmd->isOptionSet("-hierarchical")) {
        bool hierarchical = false;
        bool res = cmd->getOptionValue("-hierarchical", hierarchical);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", hierarchical);
    }
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-of_objects")) {
        std::vector<std::string> of_objects;
        bool res = cmd->getOptionValue("-of_objects", of_objects);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& of_objects_name : of_objects) {
            message->info("get value %s \n", of_objects_name.c_str());
        }
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcGetPins(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (cmd->isOptionSet("-hsc")) {
        std::string hsc = "";
        bool res = cmd->getOptionValue("-hsc", hsc);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", hsc.c_str());
    }
    if (cmd->isOptionSet("-hierarchical")) {
        bool hierarchical = false;
        bool res = cmd->getOptionValue("-hierarchical", hierarchical);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", hierarchical);
    }
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-of_objects")) {
        std::vector<std::string> of_objects;
        bool res = cmd->getOptionValue("-of_objects", of_objects);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& of_objects_name : of_objects) {
            message->info("get value %s \n", of_objects_name.c_str());
        }
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcGetPorts(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (cmd->isOptionSet("-nocase")) {
        bool nocase = false;
        bool res = cmd->getOptionValue("-nocase", nocase);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", nocase);
    }
    if (cmd->isOptionSet("-regexp")) {
        bool regexp = false;
        bool res = cmd->getOptionValue("-regexp", regexp);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %d \n", regexp);
    }
    if (cmd->isOptionSet("-patterns")) {
        std::vector<std::string> patterns;
        bool res = cmd->getOptionValue("-patterns", patterns);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& patterns_name : patterns) {
            message->info("get value %s \n", patterns_name.c_str());
        }
    }
    return TCL_OK;
}


// multivoltage power commands manager
int parseSdcCreateVoltageArea(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!((cmd->isOptionSet("-name") and cmd->isOptionSet("cell_list")))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-name")) {
        std::string name = "";
        bool res = cmd->getOptionValue("-name", name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", name.c_str());
    }
    if (cmd->isOptionSet("-coordinate")) {
        std::vector<std::string> coordinate;
        bool res = cmd->getOptionValue("-coordinate", coordinate);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& coordinate_name : coordinate) {
            message->info("get value %s \n", coordinate_name.c_str());
        }
    }
    if (cmd->isOptionSet("-guard_band_x")) {
        std::vector<double> guard_band_x;
        bool res = cmd->getOptionValue("-guard_band_x", guard_band_x);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& guard_band_x_name : guard_band_x) {
            message->info("get value %f \n", guard_band_x_name);
        }
    }
    if (cmd->isOptionSet("-guard_band_y")) {
        std::vector<double> guard_band_y;
        bool res = cmd->getOptionValue("-guard_band_y", guard_band_y);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& guard_band_y_name : guard_band_y) {
            message->info("get value %f \n", guard_band_y_name);
        }
    }
    if (cmd->isOptionSet("cell_list")) {
        std::vector<std::string> cell_list;
        bool res = cmd->getOptionValue("cell_list", cell_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& cell_list_name : cell_list) {
            message->info("get value %s \n", cell_list_name.c_str());
        }
    }

    return TCL_OK;
}

int parseSdcSetLevelShifterStrategy(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-rule"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-rule")) {
        std::string rule = "";
        bool res = cmd->getOptionValue("-rule", rule);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get first value %s \n", rule.c_str());
    }
    return TCL_OK;
}

int parseSdcSetLevelShifterThreshold(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-voltage"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-voltage")) {
        double voltage = 0;
        bool res = cmd->getOptionValue("-voltage", voltage);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %f \n", voltage);
    }
    if (cmd->isOptionSet("-percent")) {
        double percent = 0;
        bool res = cmd->getOptionValue("-percent", percent);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %f \n", percent);
    }

    return TCL_OK;
}

int parseSdcSetMaxDynamicPower(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("power"))) {                         //´Ë´¦Óësdc2.1.tclÇø±ðÒ»¸övalue
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("power")) {
        double power = 0;
        bool res = cmd->getOptionValue("power", power);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %f \n", power);
    }
    if (cmd->isOptionSet("-unit")) {
        std::string unit="";
        bool res = cmd->getOptionValue("-unit", unit);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", unit.c_str());
    }

    return TCL_OK;
}

int parseSdcSetMaxLeakagePower(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("power"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("power")) {
        double power = 0;
        bool res = cmd->getOptionValue("power", power);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %f \n", power);
    }
    if (cmd->isOptionSet("-unit")) {
        std::string unit = "";
        bool res = cmd->getOptionValue("-unit", unit);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        message->info("get value %s \n", unit.c_str());
    }
    return TCL_OK;
}

// timing constraints commands
int parseSdcCreateClock(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-period") and (cmd->isOptionSet("-name") or cmd->isOptionSet("port_pin_list")))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-add") and !cmd->isOptionSet("-name")) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockContainer();
    auto container_data = container->getData();

    CreateClockPtr create_clock = std::make_shared<CreateClock>();
    ClockPtr clock = std::make_shared<Clock>();
    if (cmd->isOptionSet("-period")) {
        double period = 0.0;
        bool res = cmd->getOptionValue("-period", period);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock->setPeriod(period);
    }
    if (cmd->isOptionSet("-name")) {
        std::string name = "";
        bool res = cmd->getOptionValue("-name", name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock->setName(name);
    }
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        create_clock->setComment(comment);
    }
    if (cmd->isOptionSet("-waveform")) {
        std::vector<double> waveform_list;
        bool res = cmd->getOptionValue("-waveform", waveform_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &waveform : waveform_list) {
            clock->addWaveform(static_cast<float>(waveform));
        }
    }
    if (cmd->isOptionSet("-add")) {
        create_clock->setAdd();
    }
    std::vector<std::string> port_pin_list;
    if (cmd->isOptionSet("port_pin_list")) { //sdc2.1 not support net object source
        bool res = cmd->getOptionValue("port_pin_list", port_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        if (clock->getName() == "" and !port_pin_list.empty()) {
            clock->setName(port_pin_list.front());
        }
    } else {
        clock->setVirtual();
    }
    container_data->addClock(clock, create_clock);

    if (port_pin_list.empty()) {
        container_data->updateVirtualClock(clock);
    } else {
        for (const auto &pin_name : port_pin_list) {
            bool success = container_data->addClockPin(pin_name, clock, create_clock->isAdd());
            if (!success) {
                //TODO messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcCreateGeneratedClock(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-source") and cmd->isOptionSet("port_pin_list") and
        (!cmd->isOptionSet("-multiply_by")) and cmd->isOptionSet("-divide_by") )) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-add") and !(cmd->isOptionSet("-name") and cmd->isOptionSet("master_clock"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockContainer();
    auto container_data = container->getData();

    CreateGeneratedClockPtr generated_clock = std::make_shared<CreateGeneratedClock>();
    ClockPtr clock = std::make_shared<Clock>();
    if (cmd->isOptionSet("-name")) {
        std::string name = "";
        bool res = cmd->getOptionValue("-name", name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock->setName(name);
    }
    if (cmd->isOptionSet("-source")) {
        std::vector<std::string> source_list;
        bool res = cmd->getOptionValue("-source", source_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &source : source_list) {
            bool success = generated_clock->addSourceMasterPin(source);
            if (!success) {
                //TODO error message
            }
        }
    }
    if (cmd->isOptionSet("-edges")) {
        std::vector<int> edges;
        bool res = cmd->getOptionValue("-edges", edges);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &edge : edges) {
            generated_clock->addEdge(static_cast<float>(edge));
        }
    }
    if (cmd->isOptionSet("-divide_by")) {
        int divide_by = 0;
        bool res = cmd->getOptionValue("-divide_by", divide_by);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        generated_clock->setDividedBy(divide_by);
    }
    if (cmd->isOptionSet("-multiply_by")) {
        int multiply_by = 0;
        bool res = cmd->getOptionValue("-multiply_by", multiply_by);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        generated_clock->setMultiplyBy(multiply_by);
    }
    if (cmd->isOptionSet("-edge_shift")) {
        std::vector<double> edge_shift_list;
        bool res = cmd->getOptionValue("-edge_shift", edge_shift_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &edge_shift : edge_shift_list) {
            generated_clock->addEdgeShift(static_cast<float>(edge_shift));
        }
    }
    if (cmd->isOptionSet("-duty_cycle")) {
        double duty_cycle = 0.0;
        bool res = cmd->getOptionValue("-duty_cycle", duty_cycle);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        generated_clock->setDutyCycle(static_cast<float>(duty_cycle));
    }
    if (cmd->isOptionSet("-invert")) {
        generated_clock->setInvert();
    }
    if (cmd->isOptionSet("-add")) {
        generated_clock->setAdd();
    }
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        generated_clock->setComment(comment);
    }
    std::vector<std::string> port_pin_list;
    if (cmd->isOptionSet("port_pin_list")) { //sdc2.1 not support net object source
        bool res = cmd->getOptionValue("port_pin_list", port_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        if (clock->getName() == "" and !port_pin_list.empty()) {
            clock->setName(port_pin_list.front());
        }
    } else {
        clock->setVirtual();
    }
    if (cmd->isOptionSet("-master_clock")) {
        std::string master_clock;
        bool res = cmd->getOptionValue("-master_clock", master_clock);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const ClockId &master_clock_id = container->getClockId(master_clock);
        if (master_clock_id == kInvalidClockId) {
            //TODO error messages
            return TCL_ERROR;
        }
        generated_clock->setMasterClock(master_clock_id);
    }
    container_data->addClock(clock, generated_clock);

    if (port_pin_list.empty()) {
        container_data->updateVirtualClock(clock);
    } else {
        for (const auto &pin_name : port_pin_list) {
            bool success = container_data->addClockPin(pin_name, clock, generated_clock->isAdd());
            if (!success) {
                //TODO messages
            }
        }
    }

    return TCL_OK;
}

template<typename U>
int parseFromThroughTo(U &path_ptr, Command *cmd, const SdcClockContainerPtr &clock_container) {
    std::vector<std::string> from_list;
    PathNodesPtr &from = path_ptr->getFrom();
    if (cmd->isOptionSet("-from")) {
        bool res = cmd->getOptionValue("-from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    if (cmd->isOptionSet("-rise_from")) {
        bool res = cmd->getOptionValue("-rise_from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        from->setRise();
    }
    if (cmd->isOptionSet("-fall_from")) {
        bool res = cmd->getOptionValue("-fall_from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        from->setFall();
    }
    from->checkFlags();
    for (const auto &from_object_name : from_list) {
        //Priority
        //Pin > Inst > Clock
        bool success = from->addPin(from_object_name);
        if (success) {
            continue;
        }
        success = from->addInst(from_object_name);
        if (success) {
            continue;
        }
        const ClockId &clock_id = clock_container->getClockId(from_object_name);
        if (clock_id == kInvalidClockId) {
            //error messages
            continue;
        }
        from->addClock(clock_id);
    }
    std::vector<std::string> to_list;
    PathNodesPtr &to = path_ptr->getTo();
    if (cmd->isOptionSet("-to")) {
        bool res = cmd->getOptionValue("-to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    if (cmd->isOptionSet("-rise_to")) {
        bool res = cmd->getOptionValue("-rise_to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        to->setRise();
    }
    if (cmd->isOptionSet("-fall_to")) {
        bool res = cmd->getOptionValue("-fall_to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        to->setFall();
    }
    for (const auto &to_object_name : to_list) {
        bool success = to->addPin(to_object_name);
        if (success) {
            continue;
        }
        success = to->addInst(to_object_name);
        if (success) {
            continue;
        }
        const ClockId &clock_id = clock_container->getClockId(to_object_name);
        if (clock_id == kInvalidClockId) {
            //error messages
            continue;
        }
        to->addClock(clock_id);
    }
    to->checkFlags();

    auto parse_through = [&path_ptr](const auto &through_list, bool rise = false, bool fall = false) {
        PathThroughNodesPtr through = std::make_shared<PathThroughNodes>();
        if (rise) {
            through->setRise();
        }
        if (fall) {
            through->setFall();
        }
        through->checkFlags();
        for (const auto &through_object_name : through_list) {
            //Priority
            //Pin > Inst > Net
            bool success = through->addPin(through_object_name);
            if (success) {
                continue;
            }
            success = through->addInst(through_object_name);
            if (success) {
                continue;
            }
            success = through->addNet(through_object_name);
            if (!success) {
                //TODO error messages
            }
        }
        path_ptr->addThrough(through);
    };
    if (cmd->isOptionSet("-through")) {
        std::vector<std::string> through_list;
        bool res = cmd->getOptionValue("-through", through_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        parse_through(through_list);
    }
    if (cmd->isOptionSet("-rise_through")) {
        std::vector<std::string> rise_through_list;
        bool res = cmd->getOptionValue("-rise_through", rise_through_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool rise = true;
        bool fall = false;
        parse_through(rise_through_list, rise, fall);
    }
    if (cmd->isOptionSet("-fall_through")) {
        std::vector<std::string> fall_through_list;
        bool res = cmd->getOptionValue("-fall_through", fall_through_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool rise = false;
        bool fall = true;
        parse_through(fall_through_list, rise, fall);
    }
    path_ptr->sortThroughs();

    return TCL_OK;
}

int parseSdcGroupPath(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( (cmd->isOptionSet("-name") and (!cmd->isOptionSet("-default"))) or
        (cmd->isOptionSet("-default") and (!cmd->isOptionSet("-name"))) or
        (!cmd->isOptionSet("-name")) and
        (cmd->isOptionSet("-from") != cmd->isOptionSet("-rise_from") != cmd->isOptionSet("-fall_from")))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getGroupPathContainer();
    auto container_data = container->getData();
    GroupPathPtr group_path = std::make_shared<GroupPath>();
    container_data->add(group_path);
    const auto &clock_container = sdc->getClockContainer();

    if (cmd->isOptionSet("-name")) {
        std::string name = "";
        bool res = cmd->getOptionValue("-name", name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        group_path->setName(name);
    }
    if (cmd->isOptionSet("-default")) {
        group_path->setDefaultValue();
    }
    if (cmd->isOptionSet("-weight")) {
        double weight = 0.0;
        bool res = cmd->getOptionValue("-weight", weight);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        group_path->setWeight(static_cast<float>(weight));
    }
    parseFromThroughTo(group_path, cmd, clock_container);
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        group_path->setComment(comment);
    }
    return TCL_OK;
}

int parseSdcSetClockGatingCheck(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(  (cmd->isOptionSet("-setup") or cmd->isOptionSet("-hold") or cmd->isOptionSet("-high") or cmd->isOptionSet("-low")) and
            (!(cmd->isOptionSet("-high") and cmd->isOptionSet("-low"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockGatingCheckContainer();
    auto container_data = container->getData();
    SetClockGatingCheckPtr clock_gating_check = std::make_shared<SetClockGatingCheck>();
    if (cmd->isOptionSet("-setup")) {
        double setup = 0.0;
        bool res = cmd->getOptionValue("-setup", setup);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_gating_check->setSetup(setup);
    }
    if (cmd->isOptionSet("-hold")) {
        double hold = 0.0;
        bool res = cmd->getOptionValue("-hold", hold);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_gating_check->setHold(hold);
    }
    if (cmd->isOptionSet("-rise")) {
        clock_gating_check->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        clock_gating_check->setFall();
    }
    if (cmd->isOptionSet("-high")) {
        clock_gating_check->setHigh();
    }
    if (cmd->isOptionSet("-low")) {
        clock_gating_check->setLow();
    }
    clock_gating_check->checkFlags();
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &object_name : object_list) {
            //Priority
            // Pin > Inst > Clock
            bool success = container_data->addToPin(object_name, clock_gating_check);
            if (success) {
                continue;
            }
            success = container_data->addToInst(object_name, clock_gating_check);
            if (success) {
                continue;
            }
            const auto &clock = clock_container->getClock(object_name);
            if (clock) {
                const auto &clock_id = clock->getId();
                container_data->addToClock(clock_id, clock_gating_check);
                continue;
            }
            //TODO not match any object error messages
        }
    } else {
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_id = design_container->getDesignId();
        container_data->addToCurrentDesign(design_cell_id, clock_gating_check);
    }

    return TCL_OK;
}

int parseSdcSetClockGroups(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(  cmd->isOptionSet("-physically_exclusive") != cmd->isOptionSet("-logically_exclusive") !=
            cmd->isOptionSet("-asynchronous") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockGroupsContainer();
    auto container_data = container->getData();
    SetClockGroupsPtr clock_groups = std::make_shared<SetClockGroups>();
    if (cmd->isOptionSet("-name")) {
        std::string name = "";
        bool res = cmd->getOptionValue("-name", name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_groups->setName(name);
    }
    if (cmd->isOptionSet("-physically_exclusive")) {
        bool physically_exclusive = false;
        bool res = cmd->getOptionValue("-physically_exclusive", physically_exclusive);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_groups->setRelationType(RelationshipType::kPhysicallyExclusive);
    }
    if (cmd->isOptionSet("-logically_exclusive")) {
        bool logically_exclusive = false;
        bool res = cmd->getOptionValue("-logically_exclusive", logically_exclusive);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_groups->setRelationType(RelationshipType::kLogicallyExclusive);
    }
    if (cmd->isOptionSet("-asynchronous")) {
        bool asynchronous = false;
        bool res = cmd->getOptionValue("-asynchronous", asynchronous);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_groups->setRelationType(RelationshipType::kLogicallyExclusive);
    }
    if (cmd->isOptionSet("-allow_paths")) {
        bool allow_paths = false;
        bool res = cmd->getOptionValue("-allow_paths", allow_paths);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_groups->setRelationType(RelationshipType::kAsynchronousAllowPaths);
    }
    const auto &clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("-group")) {
        std::vector<std::string> clock_names;
        bool res = cmd->getOptionValue("-group", clock_names);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //TODO need to be fixed
        std::vector<ClockId> clock_ids;
        for (const auto &clock_name : clock_names) {
            const ClockId &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            clock_ids.emplace_back(clock_id);
        }
        clock_groups->addToGroups(std::move(clock_ids));
    }
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_groups->setComment(comment);
    }
    container_data->addGroup(clock_groups);
    std::vector<ClockId> all_clocks;
    clock_container->getClockIds(all_clocks);
    container_data->buildClockRelationship(all_clocks);
    return TCL_OK;
}

int parseSdcSetClockLatency(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(  cmd->isOptionSet("delay") and cmd->isOptionSet("object_list") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockLatencyContainer();
    auto container_data = container->getData();
    SetClockLatencyPtr clock_latency = std::make_shared<SetClockLatency>();
    if (cmd->isOptionSet("delay")) {
        double delay = 0.0;
        bool res = cmd->getOptionValue("delay", delay);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_latency->setDelay(delay);
    }
    if (cmd->isOptionSet("-rise")) {
        clock_latency->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        clock_latency->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        clock_latency->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        clock_latency->setMax();
    }
    if (cmd->isOptionSet("-dynamic")) {
        clock_latency->setDynamic();
    }
    if (cmd->isOptionSet("-source")) {
        clock_latency->setSource();
    }
    if (cmd->isOptionSet("-early")) {
        clock_latency->setEarly();
    }
    if (cmd->isOptionSet("-late")) {
        clock_latency->setLate();
    }
    clock_latency->checkFlags();
    ClockLatencyOnPinPtr pin_clock_latency = std::make_shared<ClockLatencyOnPin>(*clock_latency);
    const auto &clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock_list;
        bool res = cmd->getOptionValue("-clock", clock_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &clock_name : clock_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            pin_clock_latency->addClock(clock_id);
        }
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            //Priority: Pin > Clock
            bool success = container_data->addToPin(object_name, pin_clock_latency);
            if (success) {
                continue;
            }
            const auto &clock_id = clock_container->getClockId(object_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            container_data->addToClock(clock_id, clock_latency);
        }
    }

    return TCL_OK;
}

int parseSdcSetSense(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("object_list") and (cmd->isOptionSet("-positive") !=
        cmd->isOptionSet("-negative") != cmd->isOptionSet("-pulse") !=
        cmd->isOptionSet("-stop_propagation") !=
        (cmd->isOptionSet("-non_unate") and cmd->isOptionSet("-clocks")) ) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getSenseContainer();
    auto container_data = container->getData();
    SetSensePtr sense = std::make_shared<SetSense>();

    if (cmd->isOptionSet("-type")) {
        std::string type;
        bool res = cmd->getOptionValue("-type", type);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool success = sense->setType(type);
        if (!success) {
            //error messages
        }
    }
    if (cmd->isOptionSet("-non_unate")) {
        sense->setNonUnate();
    }
    if (cmd->isOptionSet("-clocks")) {
        std::vector<std::string> clocks_list;
        bool res = cmd->getOptionValue("-clocks", clocks_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        container->setClockContainer(clock_container);
        for (const auto &clock_name : clocks_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            sense->addClock(clock_id);
        }
    }
    if (cmd->isOptionSet("-positive")) {
        sense->setPositive();
    }
    if (cmd->isOptionSet("-negative")) {
        sense->setNegative();
    }
    if (cmd->isOptionSet("-clock_leaf")) {
        sense->setClockLeaf();
    }
    if (cmd->isOptionSet("-stop_propagation")) {
        sense->setStopPropation();
    }
    if (cmd->isOptionSet("-pulse")) {
        std::string pulse;
        bool res = cmd->getOptionValue("-pulse", pulse);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool success = sense->setPulse(pulse);
        if (!success) {
            //error messages
        }
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->addToPin(object_name, sense);
            if (!success) {
                // error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetClockTransition(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(  cmd->isOptionSet("transition") and cmd->isOptionSet("clock_list") and
            (!(cmd->isOptionSet("-rise") and cmd->isOptionSet("-fall"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockTransitionContainer();
    auto container_data = container->getData();
    SetClockTransitionPtr clock_transition = std::make_shared<SetClockTransition>();
    if (cmd->isOptionSet("transition")) {
        double transition = 0.0;
        bool res = cmd->getOptionValue("transition", transition);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_transition->setTransition(transition);
    }
    if (cmd->isOptionSet("-rise")) {
        clock_transition->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        clock_transition->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        clock_transition->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        clock_transition->setMax();
    }
    if (cmd->isOptionSet("clock_list")) {
        std::vector<std::string> clock_list;
        bool res = cmd->getOptionValue("clock_list", clock_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &clock_name : clock_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            container_data->add(clock_id, clock_transition);
        }
    }

    return TCL_OK;
}

int parseSdcSetClockUncertainty(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(  cmd->isOptionSet("uncertainty") and cmd->isOptionSet("object_list") != 
            ((cmd->isOptionSet("-from") != cmd->isOptionSet("-rise_from") != cmd->isOptionSet("-fall_from")) and
            (cmd->isOptionSet("-to") != cmd->isOptionSet("-rise_to") != cmd->isOptionSet("-fall_to"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getClockUncertaintyContainer();
    auto container_data = container->getData();
    SetClockUncertaintyPtr clock_uncertainty = std::make_shared<SetClockUncertainty>();
    if (cmd->isOptionSet("uncertainty")) {
        double uncertainty = 0.0;
        bool res = cmd->getOptionValue("uncertainty", uncertainty);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        clock_uncertainty->setUncertainty(uncertainty);
    }
    if (cmd->isOptionSet("-setup")) {
        clock_uncertainty->setSetup();
    }
    if (cmd->isOptionSet("-hold")) {
        clock_uncertainty->setHold();
    }
    clock_uncertainty->checkFlags();
    InterClockUncertaintyPtr inter_clock_uncertainty = std::make_shared<InterClockUncertainty>(*clock_uncertainty); 
    std::vector<std::string> from_list;
    if (cmd->isOptionSet("-from")) {
        bool res = cmd->getOptionValue("-from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->setRiseFrom();
        inter_clock_uncertainty->setFallFrom();
    }
    if (cmd->isOptionSet("-rise_from")) {
        bool res = cmd->getOptionValue("-rise_from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->setRiseFrom();
        inter_clock_uncertainty->resetFallFrom();
    }
    if (cmd->isOptionSet("-fall_from")) {
        bool res = cmd->getOptionValue("-fall_from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->resetRiseFrom();
        inter_clock_uncertainty->setFallFrom();
    }
    std::vector<std::string> to_list;
    if (cmd->isOptionSet("-to")) {
        bool res = cmd->getOptionValue("-to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->setRiseTo();
        inter_clock_uncertainty->setFallTo();
    }
    if (cmd->isOptionSet("-rise_to")) {
        bool res = cmd->getOptionValue("-rise_to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->setRiseTo();
        inter_clock_uncertainty->resetFallTo();
    }
    if (cmd->isOptionSet("-fall_to")) {
        bool res = cmd->getOptionValue("-fall_to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->resetRiseTo();
        inter_clock_uncertainty->setFallTo();
    }
    if (cmd->isOptionSet("-rise")) {
        bool rise = false;
        bool res = cmd->getOptionValue("-rise", rise);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->setRiseTo();
        inter_clock_uncertainty->resetFallTo();
    }
    if (cmd->isOptionSet("-fall")) {
        bool fall = false;
        bool res = cmd->getOptionValue("-fall", fall);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        inter_clock_uncertainty->setFallTo();
        inter_clock_uncertainty->resetRiseTo();
    }
    bool is_inter_clock_uncertainty = !from_list.empty() and !to_list.empty();
    const auto &clock_container = sdc->getClockContainer();
    if (!is_inter_clock_uncertainty) {
        if (cmd->isOptionSet("object_list")) {
            std::vector<std::string> object_list;
            bool res = cmd->getOptionValue("object_list", object_list);
            if (!res) {
                //TODO messages
                return TCL_ERROR;
            }
            for (const auto &object_name : object_list) {
                //Priority : Pin > Clock
                bool success = container_data->addToPin(object_name, clock_uncertainty);
                if (success) {
                    continue;
                }
                const auto &clock_id = clock_container->getClockId(object_name);
                if (clock_id == kInvalidClockId) {
                    //error messages
                    continue;
                }
                container_data->addToClock(clock_id, clock_uncertainty);
            }
        }
    } else {
        for (const auto &from_clock_name : from_list) {
            const auto &from_clock_id = clock_container->getClockId(from_clock_name);
            if (from_clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            for (const auto &to_clock_name : to_list) {
                const auto &to_clock_id = clock_container->getClockId(to_clock_name);
                if (to_clock_id == kInvalidClockId) {
                    //error messages
                    continue;
                }
                container_data->addToInterClock(InterClockPair(from_clock_id, to_clock_id), inter_clock_uncertainty);
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetDataCheck(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(  (cmd->isOptionSet("-rise_from") or cmd->isOptionSet("-from") or cmd->isOptionSet("-fall_from")) and
            (cmd->isOptionSet("-to") or cmd->isOptionSet("-rise_to") or cmd->isOptionSet("-fall_to")) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getDataCheckContainer();
    auto container_data = container->getData();
    SetDataCheckPtr data_check = std::make_shared<SetDataCheck>();
    std::vector<std::string> from_list;
    if (cmd->isOptionSet("-from")) {
        bool res = cmd->getOptionValue("-from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        data_check->setRiseFrom();
        data_check->setFallFrom();
    }
    if (cmd->isOptionSet("-rise_from")) {
        bool res = cmd->getOptionValue("-rise_from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        data_check->setRiseFrom();
        data_check->resetFallFrom();
    }
    if (cmd->isOptionSet("-fall_from")) {
        bool res = cmd->getOptionValue("-fall_from", from_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        data_check->setFallFrom();
        data_check->resetRiseFrom();
    }
    std::vector<std::string> to_list;
    if (cmd->isOptionSet("-to")) {
        bool res = cmd->getOptionValue("-to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        data_check->setRiseTo();
        data_check->setFallTo();
    }
    if (cmd->isOptionSet("-rise_to")) {
        bool res = cmd->getOptionValue("-rise_to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        data_check->setRiseTo();
        data_check->resetFallTo();
    }
    if (cmd->isOptionSet("-fall_to")) {
        bool res = cmd->getOptionValue("-fall_to", to_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        data_check->setFallTo();
        data_check->resetRiseTo();
    }
    if (cmd->isOptionSet("-setup")) {
        data_check->setSetup();
    }
    if (cmd->isOptionSet("-hold")) {
        data_check->setHold();
    }
    data_check->checkFlags();
    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock_list;
        bool res = cmd->getOptionValue("-clock", clock_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &clock_name : clock_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            data_check->addClock(clock_id);
        }
    }
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &from_pin_name : from_list) {
            for (const auto &to_pin_name : to_list) {
                bool success = container_data->addCheck(from_pin_name, to_pin_name, data_check);
                if (!success) {
                    //error message
                }
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetDisableTiming(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("object_list") and (!(cmd->isOptionSet("-to") !=  cmd->isOptionSet("-from"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getDisableTimingContainer();
    auto container_data = container->getData();
    std::string from = "";
    if (cmd->isOptionSet("-from")) {
        bool res = cmd->getOptionValue("-from", from);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    std::string to = "";
    if (cmd->isOptionSet("-to")) {
        bool res = cmd->getOptionValue("-to", to);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->addToInst(object_name, from, to);
            if (success) {
                continue;
            }
            success = container_data->addToCell(object_name, from, to);
            if (success) {
                continue;
            }
            success = container_data->addToPin(object_name, from, to);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetFalsePath(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( (cmd->isOptionSet("-from") or cmd->isOptionSet("-to") or cmd->isOptionSet("-through")) and
            (!(cmd->isOptionSet("-rise") and cmd->isOptionSet("-fall"))) and
            (!(cmd->isOptionSet("-setup") and cmd->isOptionSet("-hold"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getFalsePathContainer();
    auto container_data = container->getData();
    SetFalsePathPtr false_path = std::make_shared<SetFalsePath>();
    container_data->add(false_path);
    const auto &clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("-setup")) {
        false_path->setSetup();
    }
    if (cmd->isOptionSet("-hold")) {
        false_path->setHold();
    }
    if (cmd->isOptionSet("-rise")) {
        false_path->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        false_path->setFall();
    }
    parseFromThroughTo(false_path, cmd, clock_container);
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        false_path->setComment(comment);
    }

    return TCL_OK;
}

int parseSdcSetIdealLatency(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("value") and cmd->isOptionSet("object_list") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getIdealLatencyContainer();
    auto container_data = container->getData();
    SetIdealLatencyPtr latency = std::make_shared<SetIdealLatency>();
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        latency->setValue(static_cast<float>(value));
    }

    if (cmd->isOptionSet("-rise")) {
        latency->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        latency->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        latency->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        latency->setMax();
    }
    latency->checkFlags();
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->add(object_name, latency);
            if (!success) {
                //TODO error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetIdealNetwork(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("object_list") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getIdealNetworkContainer();
    auto container_data = container->getData();
    bool no_propagate = false;
    if (cmd->isOptionSet("-no_propagate")) {
        bool res = cmd->getOptionValue("-no_propagate", no_propagate);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->addToPin(object_name, no_propagate);
            if (success) {
                continue;
            }
            success = container_data->addToNet(object_name, no_propagate);
            if (!success) {
                //TODO error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetIdealTransition(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("value") and cmd->isOptionSet("object_list") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getIdealTransitionContainer();
    auto container_data = container->getData();
    SetIdealTransitionPtr transition = std::make_shared<SetIdealTransition>();
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        transition->setValue(static_cast<float>(value));
    }
    if (cmd->isOptionSet("-rise")) {
        transition->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        transition->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        transition->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        transition->setMax();
    }
    transition->checkFlags();
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->add(object_name, transition);
            if (!success) {
                //TODO error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetInputDelay(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("delay_value") and cmd->isOptionSet("port_pin_list") and
           (!( (cmd->isOptionSet("-clock_fall") or cmd->isOptionSet("-level_sensitive")) and
           (!(cmd->isOptionSet("-clock"))) )) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getInputDelayContainer();
    auto container_data = container->getData();
    SetInputDelayPtr delay = std::make_shared<SetInputDelay>();
    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock_list;
        bool res = cmd->getOptionValue("-clock", clock_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &clock_name : clock_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            delay->addClock(clock_id);
        }
    }
    if (cmd->isOptionSet("-clock_fall")) {
        delay->setClockFall();
    }
    if (cmd->isOptionSet("-level_sensitive")) {
        delay->setLevelSensitive();
    }
    if (cmd->isOptionSet("-rise")) {
        delay->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        delay->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        delay->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        delay->setMax();
    }
    if (cmd->isOptionSet("-add_delay")) {
        delay->setAddDelay();
    }
    if (cmd->isOptionSet("delay_value")) {
        double delay_value = 0.0;
        bool res = cmd->getOptionValue("delay_value", delay_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        delay->setDelayValue(delay_value);
    }
    if (cmd->isOptionSet("-reference_pin")) {
        std::vector<std::string> reference_pin_list;
        bool res = cmd->getOptionValue("-reference_pin", reference_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &reference_pin_name : reference_pin_list) {
            bool success = delay->addReferencePin(reference_pin_name);
            if (!success) {
                //error messages
                continue;
            }
        }
    }
    if (cmd->isOptionSet("-network_latency_included")) {
        delay->setNetworkLatencyIncluded();
    }
    if (cmd->isOptionSet("-source_latency_included")) {
        delay->setSourceLatencyIncluded();
    }
    delay->checkFlags();
    if (cmd->isOptionSet("port_pin_list")) {
        std::vector<std::string> port_pin_list;
        bool res = cmd->getOptionValue("port_pin_list", port_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_pin_name : port_pin_list) {
            bool success = container_data->add(port_pin_name, delay);
            if (!success) {
                //error messages
                continue;
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetMaxDelay(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("delay_value") and (!(cmd->isOptionSet("-rise") and cmd->isOptionSet("-fall"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMaxDelayContainer();
    auto container_data = container->getData();
    SetMaxDelayPtr delay = std::make_shared<SetMaxDelay>();
    container_data->add(delay);
    const auto &clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("delay_value")) {
        double delay_value = 0.0;
        bool res = cmd->getOptionValue("delay_value", delay_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        delay->setDelayValue(static_cast<float>(delay_value));
    }
    if (cmd->isOptionSet("-rise")) {
        delay->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        delay->setFall();
    }
    parseFromThroughTo(delay, cmd, clock_container);
    if (cmd->isOptionSet("-ignore_clock_latency")) {
        delay->setIgnoreClockLatency();
    }
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        delay->setComment(comment);
    }

    return TCL_OK;
}

int parseSdcSetMaxTimeBorrow(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("delay_value") and cmd->isOptionSet("object_list") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMaxTimeBorrowContainer();
    auto container_data = container->getData();
    const auto &clock_container = sdc->getClockContainer();
    SetMaxTimeBorrow borrow;
    if (cmd->isOptionSet("delay_value")) {
        double delay_value = 0.0;
        bool res = cmd->getOptionValue("delay_value", delay_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        borrow.setValue(delay_value);
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            //Priority: Pin > Inst > Clock
            bool success = container_data->addToPin(object_name, borrow);
            if (success) {
                continue;
            }
            success = container_data->addToInst(object_name, borrow);
            if (success) {
                continue;
            }
            const auto &clock_id = clock_container->getClockId(object_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            container_data->addToClock(clock_id, borrow);
        }
    }
    return TCL_OK;
}

int parseSdcSetMinDelay(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("delay_value") and (!(cmd->isOptionSet("-rise") and cmd->isOptionSet("-fall"))) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMinDelayContainer();
    auto container_data = container->getData();
    SetMinDelayPtr delay = std::make_shared<SetMinDelay>();
    container_data->add(delay);
    const auto &clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("delay_value")) {
        double delay_value = 0.0;
        bool res = cmd->getOptionValue("delay_value", delay_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        delay->setDelayValue(static_cast<float>(delay_value));
    }
    if (cmd->isOptionSet("-rise")) {
        delay->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        delay->setFall();
    }
    parseFromThroughTo(delay, cmd, clock_container);
    if (cmd->isOptionSet("-ignore_clock_latency")) {
        delay->setIgnoreClockLatency();
    }
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        delay->setComment(comment);
    }

    return TCL_OK;
}

int parseSdcSetMinPulseWidth(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("value") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMinPulseWidthContainer();
    auto container_data = container->getData();
    SetMinPulseWidthPtr pulse_width = std::make_shared<SetMinPulseWidth>();
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        pulse_width->setValue(static_cast<float>(value));
    }
    if (cmd->isOptionSet("-low")) {
        pulse_width->setLow();
    }
    if (cmd->isOptionSet("-high")) {
        pulse_width->setHigh();
    }
    pulse_width->checkFlags();
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &object_name : object_list) {
            //Priority: Pin > Inst > Clock
            bool success = container_data->addToPin(object_name, pulse_width);
            if (success) {
                continue;
            }
            success = container_data->addToInst(object_name, pulse_width);
            if (success) {
                continue;
            }
            const auto &clock_id = clock_container->getClockId(object_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            container_data->addToClock(clock_id, pulse_width);
        }
    } else {
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_id = design_container->getDesignId();
        container_data->addToCurrentDesign(design_cell_id, pulse_width);
    }

    return TCL_OK;
}

int parseSdcSetMulticyclePath(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("path_multiplier") )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMulticyclePathContainer();
    auto container_data = container->getData();
    SetMulticyclePathPtr multi_cycle_path = std::make_shared<SetMulticyclePath>();
    container_data->add(multi_cycle_path);
    const auto &clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("path_multiplier")) {
        int path_multiplier = 0;
        bool res = cmd->getOptionValue("path_multiplier", path_multiplier);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        multi_cycle_path->setPathMultiplier(static_cast<UInt32>(path_multiplier));
    }
    if (cmd->isOptionSet("-setup")) {
        multi_cycle_path->setSetup();
    }
    if (cmd->isOptionSet("-hold")) {
        multi_cycle_path->setHold();
    }
    if (cmd->isOptionSet("-rise")) {
        multi_cycle_path->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        multi_cycle_path->setFall();
    }
    if (cmd->isOptionSet("-start")) {
        multi_cycle_path->setStart();
    }
    if (cmd->isOptionSet("-end")) {
        multi_cycle_path->setEnd();
    }
    parseFromThroughTo(multi_cycle_path, cmd, clock_container);
    if (cmd->isOptionSet("-comment")) {
        std::string comment = "";
        bool res = cmd->getOptionValue("-comment", comment);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        multi_cycle_path->setComment(comment);
    }

    return TCL_OK;
}

int parseSdcSetOutputDelay(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("delay_value") and cmd->isOptionSet("port_pin_list") and
           (!( (cmd->isOptionSet("-clock_fall") or cmd->isOptionSet("-level_sensitive")) and
           (!(cmd->isOptionSet("-clock"))) )) )) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getOutputDelayContainer();
    auto container_data = container->getData();
    SetOutputDelayPtr delay = std::make_shared<SetOutputDelay>();
    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock_list;
        bool res = cmd->getOptionValue("-clock", clock_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &clock_name : clock_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            delay->addClock(clock_id);
        }
    }
    if (cmd->isOptionSet("-clock_fall")) {
        delay->setClockFall();
    }
    if (cmd->isOptionSet("-level_sensitive")) {
        delay->setLevelSensitive();
    }
    if (cmd->isOptionSet("-rise")) {
        delay->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        delay->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        delay->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        delay->setMax();
    }
    if (cmd->isOptionSet("-add_delay")) {
        delay->setAddDelay();
    }
    if (cmd->isOptionSet("delay_value")) {
        double delay_value = 0.0;
        bool res = cmd->getOptionValue("delay_value", delay_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        delay->setDelayValue(delay_value);
    }
    if (cmd->isOptionSet("-reference_pin")) {
        std::vector<std::string> reference_pin_list;
        bool res = cmd->getOptionValue("-reference_pin", reference_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &reference_pin_name : reference_pin_list) {
            bool success = delay->addReferencePin(reference_pin_name);
            if (!success) {
                //error messages
                continue;
            }
        }
    }
    if (cmd->isOptionSet("-network_latency_included")) {
        delay->setNetworkLatencyIncluded();
    }
    if (cmd->isOptionSet("-source_latency_included")) {
        delay->setSourceLatencyIncluded();
    }
    delay->checkFlags();
    if (cmd->isOptionSet("port_pin_list")) {
        std::vector<std::string> port_pin_list;
        bool res = cmd->getOptionValue("port_pin_list", port_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_pin_name : port_pin_list) {
            bool success = container_data->add(port_pin_name, delay);
            if (!success) {
                //error messages
                continue;
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetPropagatedClock(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("object_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getPropagatedClockContainer();
    auto container_data = container->getData();
    auto clock_container = sdc->getClockContainer();
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            ClockPtr clock = clock_container->getClock(object_name);
            if (clock) {
                clock->setPropagated();
            }
            bool success = container_data->addToPin(object_name);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

// environment commands manager
int parseSdcSetCaseAnalysis(ClientData cld, Tcl_Interp *itp, int argc, const char *argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( cmd->isOptionSet("value") and cmd->isOptionSet("port_or_pin_list") )) {
        //TODO messages
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getCaseAnalysisContainer();
    auto container_data = container->getData();
    SetCaseAnalysis case_analysis;
    if (cmd->isOptionSet("value")) {
        std::string value;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool success = case_analysis.setValue(value);
        if (!success) {
            //error messages
            return TCL_ERROR;
        }
    }
    if (cmd->isOptionSet("port_or_pin_list")) {
        std::vector<std::string> port_or_pin_list;
        bool res = cmd->getOptionValue("port_or_pin_list", port_or_pin_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& pin_name : port_or_pin_list) {
            bool success = container_data->add(pin_name, case_analysis);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetDrive(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("resistance") and cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getDriveContainer();
    auto container_data = container->getData();
    SetDrivePtr drive = std::make_shared<SetDrive>();
    if (cmd->isOptionSet("resistance")) {
        double resistance = 0.0;
        bool res = cmd->getOptionValue("resistance", resistance);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        drive->setResistance(resistance);
    }
    if (cmd->isOptionSet("-rise")) {
        drive->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        drive->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        drive->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        drive->setMax();
    }
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_name : port_list) {
            bool success = container_data->add(port_name, drive);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetDrivingCell(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("port_list") and cmd->isOptionSet("-lib_cell"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getDrivingCellContainer();
    auto container_data = container->getData();
    SetDrivingCellPtr drving_cell = std::make_shared<SetDrivingCell>();
    std::string lib_cell = "";
    if (cmd->isOptionSet("-lib_cell")) {
        bool res = cmd->getOptionValue("-lib_cell", lib_cell);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    if (cmd->isOptionSet("-rise")) {
        drving_cell->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        drving_cell->setFall();
    }
    std::string library = "";
    if (cmd->isOptionSet("-library")) {
        bool res = cmd->getOptionValue("-library", library);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    const auto &analysis_corner_id = sdc->getAnalysisCornerId();
    TCell* tcell = getLibCellInCorner(analysis_corner_id, library, lib_cell);
    if (!tcell) {
        //TODO messages
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-from_pin")) {
        std::string from_pin_name = "";
        bool res = cmd->getOptionValue("-from_pin", from_pin_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        TTerm* from_pin = tcell->getTerm(from_pin_name);
        if (!from_pin) {
            //TODO messages
            return TCL_ERROR;
        }
        drving_cell->setFromTTerm(from_pin->getId());
    }
    if (cmd->isOptionSet("-pin")) {
        std::string to_pin_name = "";
        bool res = cmd->getOptionValue("-pin", to_pin_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        TTerm* to_pin = tcell->getTerm(to_pin_name);
        if (!to_pin) {
            //TODO messages
            return TCL_ERROR;
        }
        drving_cell->setToTTerm(to_pin->getId());
    }
    if (cmd->isOptionSet("-dont_scale")) {
        drving_cell->setDontScale();
    }
    if (cmd->isOptionSet("-no_design_rule")) {
        drving_cell->setNoDesignRule();
    }
    if (cmd->isOptionSet("-input_transition_rise")) {
        double input_transition_rise = 0.0;
        bool res = cmd->getOptionValue("-input_transition_rise", input_transition_rise);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        drving_cell->setInputTransitionRise(input_transition_rise);
    }
    if (cmd->isOptionSet("-input_transition_fall")) {
        double input_transition_fall = 0.0;
        bool res = cmd->getOptionValue("-input_transition_fall", input_transition_fall);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        drving_cell->setInputTransitionFall(input_transition_fall);
    }
    if (cmd->isOptionSet("-min")) {
        drving_cell->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        drving_cell->setMax();
    }
    if (cmd->isOptionSet("-clock")) {
        std::string clock_name;
        bool res = cmd->getOptionValue("-clock", clock_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        const auto &clock_id = clock_container->getClockId(clock_name);
        if (clock_id == kInvalidClockId) {
            //error messages
            return TCL_ERROR;
        }
        drving_cell->setClock(clock_id);
    }
    if (cmd->isOptionSet("-clock_fall")) {
        drving_cell->setClockFall();
    }
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_name : port_list) {
            bool success = container_data->add(port_name, drving_cell);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetFanoutLoad(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("value") and cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getFanoutLoadContainer();
    auto container_data = container->getData();
    SetFanoutLoad fanout;
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        fanout.setValue(static_cast<float>(value));
    }
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_name : port_list) {
            bool success = container_data->add(port_name, fanout);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetInputTransition(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("transition") and cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getInputTransitionContainer();
    auto container_data = container->getData();
    SetInputTransitionPtr input_transition = std::make_shared<SetInputTransition>();
    if (cmd->isOptionSet("-rise")) {
        input_transition->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        input_transition->setFall();
    }
    if (cmd->isOptionSet("-min")) {
        input_transition->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        input_transition->setMax();
    }
    input_transition->checkFlags();
    if (cmd->isOptionSet("transition")) {
        double transition = 0.0;
        bool res = cmd->getOptionValue("transition", transition);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        input_transition->setTransition(transition);
    }
    if (cmd->isOptionSet("-clock")) {
        std::vector<std::string> clock_list;
        bool res = cmd->getOptionValue("-clock", clock_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        for (const auto &clock_name : clock_list) {
            const auto &clock_id = clock_container->getClockId(clock_name);
            if (clock_id == kInvalidClockId) {
                //error messages
                continue;
            }
            input_transition->addClock(clock_id);
        }
    }
    if (cmd->isOptionSet("-clock_fall")) {
        input_transition->setClockFall();
    }
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_name : port_list) {
            bool success = container_data->add(port_name, input_transition);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetLoad(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("value") and cmd->isOptionSet("objects"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getLoadContainer();
    auto container_data = container->getData();
    SetLoadPtr load = std::make_shared<SetLoad>();
    if (cmd->isOptionSet("-min")) {
        load->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        load->setMax();
    }
    if (cmd->isOptionSet("-substract_pin_load")) {
        load->setSubstractPinLoad();
    }
    if (cmd->isOptionSet("-pin_load")) {
        load->setPinLoad();
    }
    if (cmd->isOptionSet("-wire_load")) {
        load->setWireLoad();
    }
    load->checkFlags();
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        load->setCap(static_cast<float>(value));
    }
    if (cmd->isOptionSet("objects")) {
        std::vector<std::string> objects_list;
        bool res = cmd->getOptionValue("objects", objects_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : objects_list) {
            //Priority: Port > Net
            bool success = container_data->addToPort(object_name, load);
            if (success) {
                continue;
            }
            success = container_data->addToNet(object_name, load);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetLogicDc(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getLogicContainer();
    auto container_data = container->getData();
    SetLogic logic;
    logic.setValue(LogicValue::kDontCare);
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& port_name : port_list) {
            bool success = container_data->add(port_name, logic);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetLogicOne(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getLogicContainer();
    auto container_data = container->getData();
    SetLogic logic;
    logic.setValue(LogicValue::kOne);
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& port_name : port_list) {
            bool success = container_data->add(port_name, logic);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetLogicZero(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getLogicContainer();
    auto container_data = container->getData();
    SetLogic logic;
    logic.setValue(LogicValue::kZero);
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& port_name : port_list) {
            bool success = container_data->add(port_name, logic);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetMaxArea(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("area_value"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMaxAreaContainer();
    auto container_data = container->getData();
    SetMaxArea area;
    if (cmd->isOptionSet("area_value")) {
        double area_value = 0.0;
        bool res = cmd->getOptionValue("area_value", area_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        area.setAreaValue(area_value);
    }
    const auto &design_container = sdc->getCurrentDesignContainer();
    const auto &design_cell_id = design_container->getDesignId();
    container_data->add(design_cell_id, area);

    return TCL_OK;
}

int parseSdcSetMaxCapacitance(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("value") and cmd->isOptionSet("object_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMaxCapacitanceContainer();
    auto container_data = container->getData();
    SetMaxCapacitancePtr max_cap = std::make_shared<SetMaxCapacitance>();
    if (cmd->isOptionSet("value")) {
        double capacitance_value = 0.0;
        bool res = cmd->getOptionValue("value", capacitance_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        max_cap->setCapValue(static_cast<float>(capacitance_value));
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->addToPin(object_name, max_cap);
            if (!success) {
                continue;
            }
        }
        //TODO Not support to add to current design
    }
    return TCL_OK;
}

int parseSdcSetMaxFanout(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("fanout_value") and cmd->isOptionSet("object_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMaxFanoutContainer();
    auto container_data = container->getData();
    SetMaxFanout fanout;
    if (cmd->isOptionSet("fanout_value")) {
        double fanout_value = 0.0;
        bool res = cmd->getOptionValue("fanout_value", fanout_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        fanout.setFanoutValue(static_cast<float>(fanout_value));
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_name = design_container->getDesignName();
        const auto &design_cell_id = design_container->getDesignId();
        for (const auto &object_name : object_list) {
            //Priority: Port > Design
            bool success = container_data->addToPort(object_name, fanout);
            if (success) {
                continue;
            }
            if (object_name != design_cell_name) {
                //error messages
                continue;
            }
            container_data->addToCurrentDesign(design_cell_id, fanout);
        }
    }
    return TCL_OK;
}

int parseSdcSetMaxTransition(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("transition_value") and cmd->isOptionSet("object_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMaxTransitionContainer();
    auto container_data = container->getData();
    SetMaxTransitionPtr transition = std::make_shared<SetMaxTransition>();
    if (cmd->isOptionSet("transition_value")) {
        double transition_value = 0.0;
        bool res = cmd->getOptionValue("transition_value", transition_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        transition->setTransitionValue(transition_value);
    }
    if (cmd->isOptionSet("-clock_path")) {
        transition->setClockPath();
    }
    if (cmd->isOptionSet("-fall")) {
        transition->setFall();
    }
    if (cmd->isOptionSet("-rise")) {
        transition->setRise();
    }
    transition->checkFlags();
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        const auto &clock_container = sdc->getClockContainer();
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_name = design_container->getDesignName();
        const auto &design_cell_id = design_container->getDesignId();
        for (const auto &object_name : object_list) {
            //Priority: Pin > Clock > Design
            bool success = container_data->addToPin(object_name, transition);
            if (success) {
                continue;
            }
            ClockPtr clock = clock_container->getClock(object_name);
            if (clock) {
                container_data->addToClock(clock->getId(), transition);
                continue;
            }
            if (object_name != design_cell_name) {
                //error messages
                continue;
            }
            container_data->addToCurrentDesign(design_cell_id, transition);
        }
    }

    return TCL_OK;
}

int parseSdcSetMinCapacitance(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("value") and cmd->isOptionSet("object_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getMinCapacitanceContainer();
    auto container_data = container->getData();
    SetMinCapacitancePtr min_cap = std::make_shared<SetMinCapacitance>();
    if (cmd->isOptionSet("value")) {
        double capacitance_value = 0.0;
        bool res = cmd->getOptionValue("value", capacitance_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        min_cap->setCapValue(static_cast<float>(capacitance_value));
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->addToPin(object_name, min_cap);
            if (!success) {
                //error messages
                continue;
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetOperatingConditions(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-analysis_type"))) {
        std::string analysis_type = "";
        bool res = cmd->getOptionValue("-analysis_type", analysis_type);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get first value %s \n", analysis_type.c_str());
    }
    if (cmd->isOptionSet("library")) {
        std::vector<std::string> library_list;
        bool res = cmd->getOptionValue("library", library_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &library : library_list) {
            message->info("get seccond value %s \n", library.c_str());
        }
    }
    if (cmd->isOptionSet("-max")) {
        std::string max = "";
        bool res = cmd->getOptionValue("-max", max);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get third value %s \n", max.c_str());
    }
    if (cmd->isOptionSet("-min")) {
        std::string min = "";
        bool res = cmd->getOptionValue("-min", min);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get fourth value %s \n", min.c_str());
    }
    if (cmd->isOptionSet("-max_library")) {
        std::vector<std::string> max_library_list;
        bool res = cmd->getOptionValue("-max_library", max_library_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &max_library : max_library_list) {
            message->info("get fifth value %s \n", max_library.c_str());
        }
    }
    if (cmd->isOptionSet("-object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("-object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto & object : object_list) {
            message->info("get sixth value %s \n", object.c_str());
        }
    }
    if (cmd->isOptionSet("-min_library")) {
        std::vector<std::string> min_library_list;
        bool res = cmd->getOptionValue("-min_library", min_library_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &min_library : min_library_list) {
            message->info("get seventh value %s \n", min_library.c_str());
        }
    }
    if (cmd->isOptionSet("condition")) {
        std::string condition = "";
        bool res = cmd->getOptionValue("condition", condition);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get eighth value %s \n", condition.c_str());
    }

    return TCL_OK;
}

int parseSdcSetPortFanoutNumber(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("fanout_number") and cmd->isOptionSet("port_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getPortFanoutNumberContainer();
    auto container_data = container->getData();
    SetPortFanoutNumber fanout;
    if (cmd->isOptionSet("fanout_number")) {
        int fanout_number = 0;
        bool res = cmd->getOptionValue("fanout_number", fanout_number);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        fanout.setFanoutNumber(static_cast<UInt32>(fanout_number));
    }
    if (cmd->isOptionSet("port_list")) {
        std::vector<std::string> port_list;
        bool res = cmd->getOptionValue("port_list", port_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &port_name : port_list) {
            bool success = container_data->add(port_name, fanout);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetResistance(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("value") and cmd->isOptionSet("net_list"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getResistanceContainer();
    auto container_data = container->getData();
    SetResistance resistance;
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        resistance.setValue(static_cast<float>(value));
    }
    if (cmd->isOptionSet("-min")) {
        resistance.setMin();
    }
    if (cmd->isOptionSet("-max")) {
        resistance.setMax();
    }
    resistance.checkFlags();
    if (cmd->isOptionSet("net_list")) {
        std::vector<std::string> net_list;
        bool res = cmd->getOptionValue("net_list", net_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &net_name : net_list) {
            bool success = container_data->add(net_name, resistance);
            if (!success) {
                //error messages
            }
        }
    }

    return TCL_OK;
}

int parseSdcSetTimingDerate(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!( (cmd->isOptionSet("derate_value") and (cmd->isOptionSet("-early") and !(cmd->isOptionSet("-late")))
        or (cmd->isOptionSet("-late") and !(cmd->isOptionSet("-early")))))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getTimingDerateContainer();
    auto container_data = container->getData();
    SetTimingDeratePtr timing_derate = std::make_shared<SetTimingDerate>();
    if (cmd->isOptionSet("derate_value")) {
        double derate_value = 0.0;
        bool res = cmd->getOptionValue("derate_value", derate_value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        timing_derate->setDerateValue(derate_value);
    }
    if (cmd->isOptionSet("-min")) {
        timing_derate->setMin();
    }
    if (cmd->isOptionSet("-max")) {
        timing_derate->setMax();
    }
    if (cmd->isOptionSet("-rise")) {
        timing_derate->setRise();
    }
    if (cmd->isOptionSet("-fall")) {
        timing_derate->setFall();
    }
    if (cmd->isOptionSet("-early ")) {
        timing_derate->setEarly();
    }
    if (cmd->isOptionSet("-late")) {
        timing_derate->setLate();
    }
    if (cmd->isOptionSet("-static")) {
        timing_derate->setStaticType();
    }
    if (cmd->isOptionSet("-dynamic")) {
        timing_derate->setDynamicType();
    }
    if (cmd->isOptionSet("-increment")) {
        timing_derate->setIncrement();
    }
    if (cmd->isOptionSet("-clock")) {
        timing_derate->setClock();
    }
    if (cmd->isOptionSet("-data")) {
        timing_derate->setData();
    }
    if (cmd->isOptionSet("-net_delay")) {
        timing_derate->setNetDelay();
    }
    if (cmd->isOptionSet("-cell_delay")) {
        timing_derate->setCellDelay();
    }
    if (cmd->isOptionSet("-cell_check")) {
        timing_derate->setCellCheck();
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            //Priority: Inst > Cell > Net
            bool success = container_data->addToInst(object_name, timing_derate);
            if (success) {
                continue;
            }
            success = container_data->addToCell(object_name, timing_derate);
            if (success) {
                continue;
            }
            success = container_data->addToNet(object_name, timing_derate);
            if (!success) {
                //error messages
            }
        }
    } else {
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_id = design_container->getDesignId();
        container_data->addToCurrentDesign(design_cell_id, timing_derate);
    }

    return TCL_OK;
}

int parseSdcSetVoltage(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("value"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getVoltageContainer();
    auto container_data = container->getData();
    SetVoltagePtr voltage = std::make_shared<SetVoltage>();
    if (cmd->isOptionSet("value")) {
        double value = 0.0;
        bool res = cmd->getOptionValue("value", value);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        voltage->setMaxCaseVoltage(static_cast<float>(value));
    }
    if (cmd->isOptionSet("-min")) {
        double min = 0.0;
        bool res = cmd->getOptionValue("-min", min);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        voltage->setMinCaseVoltage(static_cast<float>(min));
    }
    if (cmd->isOptionSet("-object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("-object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object_name : object_list) {
            bool success = container_data->addToPowerNet(object_name, voltage);
            if (!success) {
                //error messages
            }
        }
    }
    return TCL_OK;
}

int parseSdcSetWireLoadMinBlockSize(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("size"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getWireLoadMinBlockSizeContainer();
    auto container_data = container->getData();
    SetWireLoadMinBlockSize min_block_size;
    if (cmd->isOptionSet("size")) {
        double size = 0.0;
        bool res = cmd->getOptionValue("size", size);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        min_block_size.setBlockSize(static_cast<float>(size));
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_id = design_container->getDesignId();
        container_data->add(design_cell_id, min_block_size);
    }
    return TCL_OK;
}

int parseSdcSetWireLoadMode(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("mode_name"))) {
        return TCL_ERROR;
    }
    SdcPtr sdc = getSdcFromCmd(cmd);
    assert(sdc);
    auto container = sdc->getWireLoadModeContainer();
    auto container_data = container->getData();
    SetWireLoadMode mode;
    if (cmd->isOptionSet("mode_name")) {
        std::string mode_name = "";
        bool res = cmd->getOptionValue("mode_name", mode_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        bool success = mode.set(mode_name);
        if (!success) {
            //error messages
        }
        const auto &design_container = sdc->getCurrentDesignContainer();
        const auto &design_cell_id = design_container->getDesignId();
        container_data->add(design_cell_id, mode);
    }
    return TCL_OK;
}

int parseSdcSetWireLoadModel(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("-name"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-name")) {
        std::string name = "";
        bool res = cmd->getOptionValue("-name", name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get first value %s \n", name.c_str());
    }
    if (cmd->isOptionSet("-library")) {
        std::vector<std::string> library_list;
        bool res = cmd->getOptionValue("-library", library_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &library : library_list) {
            message->info("get seccond value %s \n", library.c_str());
        }
    }
    if (cmd->isOptionSet("-min")) {
        bool min = false;
        bool res = cmd->getOptionValue("-min", min);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get third value %d \n", min);
    }
    if (cmd->isOptionSet("-max")) {
        bool max = false;
        bool res = cmd->getOptionValue("-max", max);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get fourth value %d \n", max);
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto &object : object_list) {
            message->info("get fifth value %s \n", object.c_str());
        }
    }
    return TCL_OK;
}

int parseSdcSetWireLoadSelectionGroup(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("group_name"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("group_name")) {
        std::string group_name = "";
        bool res = cmd->getOptionValue("group_name", group_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get first value %s \n", group_name.c_str());
    }
    if (cmd->isOptionSet("-library")) {
        std::vector<std::string> library_list;
        bool res = cmd->getOptionValue("-library", library_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& library : library_list) {
            message->info("get seccond value %s \n", library.c_str());
        }
    }
    if (cmd->isOptionSet("-min")) {
        bool min = false;
        bool res = cmd->getOptionValue("-min", min);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get third value %d \n", min);
    }
    if (cmd->isOptionSet("-max")) {
        bool max = false;
        bool res = cmd->getOptionValue("-max", max);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        //Assignment
        message->info("get fourth value %d \n", max);
    }
    if (cmd->isOptionSet("object_list")) {
        std::vector<std::string> object_list;
        bool res = cmd->getOptionValue("object_list", object_list);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        for (const auto& object : object_list) {
            message->info("get fifth value %s \n", object.c_str());
        }
    }
    return TCL_OK;
}

int readSdc(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!(cmd->isOptionSet("sdc_files"))) {
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-version")) {
        std::string version = "2.1";
        bool res = cmd->getOptionValue("-version", version);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        if (version != "2.1") {
            //TODO messages
        }
    }
    std::string analysis_mode_name = "";
    if (cmd->isOptionSet("-mode")) {
        bool res = cmd->getOptionValue("-mode", analysis_mode_name);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
    }
    std::vector<std::string> sdc_files;
    bool res = cmd->getOptionValue("sdc_files", sdc_files);
    if (!res) {
        //TODO messages
        return TCL_ERROR;
    }
    if (sdc_files.empty()) {
        //TODO messages
        return TCL_ERROR;
    }
    AnalysisMode* mode = getOrCreateModeForSdc(analysis_mode_name);
    if (!mode) {
        //TODO messages
        return TCL_ERROR;
    }
    if (!(mode->getSdc())) {
        //TODO messages
        return TCL_ERROR;
    }
    //upate sdc files in mode
    for (const auto &file : sdc_files) {
        mode->addConstraintFile(file);
    }
    std::string tcl_commands;
    getSdcFileContents(tcl_commands, sdc_files, mode->getName());
    if (tcl_commands.empty()) {
        //TODO messages
        return TCL_ERROR;
    }
    return Tcl_Eval(itp, tcl_commands.c_str());
}

int writeSdc(ClientData cld, Tcl_Interp* itp, int argc, const char* argv[]) {
    Command* cmd = CommandManager::parseCommand(argc, argv);
    assert(cmd);
    if (!cmd->isOptionSet("-mode") or !cmd->isOptionSet("sdc_file")) {
        //TODO messages
        return TCL_ERROR;
    }
    if (cmd->isOptionSet("-version")) {
        std::string version = "2.1";
        bool res = cmd->getOptionValue("-version", version);
        if (!res) {
            //TODO messages
            return TCL_ERROR;
        }
        if (version != "2.1") {
            //TODO messages
        }
    }
    std::string analysis_mode_name = "";
    bool res = cmd->getOptionValue("-mode", analysis_mode_name);
    if (!res) {
        //TODO messages
        return TCL_ERROR;
    }
    Timing *timingdb = getTimingLib();
    if (!timingdb) {
        //TODO messages
        return TCL_ERROR;
    }
    AnalysisMode* mode = timingdb->getAnalysisMode(analysis_mode_name);
    if (!mode) {
        //TODO messages
        return TCL_ERROR;
    }
    const auto &sdc = mode->getSdc();
    if (!sdc) {
        //TODO messages
        return TCL_ERROR;
    }
    std::string sdc_file = "";
    res = cmd->getOptionValue("sdc_file", sdc_file);
    if (!res) {
        //TODO messages
        return TCL_ERROR;
    }
    std::ofstream output(sdc_file);
    if (!output) {
        //TODO messages
        return TCL_ERROR;
    }
    output << *sdc << std::endl;

    return TCL_OK;
}

}
}
