/**
 * @file sdc_data.cpp
 * @date 2020-11-25
 * @brief
 *
 * Copyright (C) 2020 NIIC EDA
 *
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 *
 * of the BSD license.  See the LICENSE file for details.
 */

#include "db/timing/sdc/sdc_data.h"

namespace open_edi {
namespace db {

//general purpose commands
const std::string SdcCurrentInstanceContainer::getInstName() const {
    const ObjectId &inst_id = data_->getInstId();
    const Inst* inst = Object::addr<Inst>(inst_id);
    if (!inst) {
        return "";
    }
    return inst->getName();
}

std::ostream &operator<<(std::ostream &os, SdcHierarchySeparatorContainer &rhs) {
    os << "set_hierarchy_separator " << rhs.get() << "\n";
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcUnitsContainer &rhs) {
    os << "set_units ";
    os << "-time " << rhs.getSdcTimeUnits() << "s";
    os << "-capacitance " << rhs.getSdcCapacitanceUnits() << "F";
    os << "-current " << rhs.getSdcCurrentUnits() << "A";
    os << "-voltage " << rhs.getSdcVoltageUnits() << "V";
    os << "-resistance " << rhs.getSdcResistanceUnits() << "Ohm";
    os << "-power " << rhs.getSdcPowerUnits() << "W";
    os << "\n";
    return os;
}

// timing constraint commands
void SdcClockContainer::getClocks(std::vector<ClockPtr> &clocks) const {
    clocks.clear();
    const auto &id_to_ptr_map = data_->getIdToPtr();
    clocks.reserve(id_to_ptr_map.size());
    for (auto id_to_ptr : id_to_ptr_map) {
        clocks.emplace_back(id_to_ptr.second);
    }
}

void SdcClockContainer::getVirtualClocks(std::vector<ClockPtr> &clocks) const {
    clocks.clear();
    const auto &pin_clock_value = data_->getPinClockValue();
    const auto &id_to_ptr_map = data_->getIdToPtr();
    const auto &pin_view = pin_clock_value.left;
    const auto &range = pin_view.equal_range(UNINIT_OBJECT_ID);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &clock_id = it->second;
        const auto &found = id_to_ptr_map.find(clock_id);
        if (found == id_to_ptr_map.end()) {
            //error messages
            continue;
        }
        clocks.emplace_back(found->second);        
    }
}

void SdcClockContainer::getClockNames(std::vector<std::string> &names) const {
    names.clear();
    const auto &name_to_id_map = data_->getNameToId();
    names.reserve(name_to_id_map.size());
    for (auto name_to_id : name_to_id_map) {
        names.emplace_back(name_to_id.first);
    }
}

void SdcClockContainer::getClockIds(std::vector<ClockId> &ids) const {
    ids.clear();
    const auto &name_to_id_map = data_->getNameToId();
    ids.reserve(name_to_id_map.size());
    for (auto name_to_id : name_to_id_map) {
        ids.emplace_back(name_to_id.second);
    }
}

ClockPtr SdcClockContainer::getClock(const ClockId &id) {
    if ((id == kInvalidClockId) or (id >= data_->getCount())) {
        return nullptr;
    }
    const auto &id_to_ptr_map = data_->getIdToPtr();
    const auto &found = id_to_ptr_map.find(id);
    if (found == id_to_ptr_map.end()) {
        return nullptr;
    }
    return found->second;
}

ClockPtr SdcClockContainer::getClock(const std::string &name) {
    const auto &name_to_id = data_->getNameToId();
    const auto &found = name_to_id.find(name);
    if (found == name_to_id.end()) {
        return nullptr;
    }
    const auto &id = found->second;
    return getClock(id);
}

const ClockId &SdcClockContainer::getClockId(const std::string &name) const{
    const auto &name_to_id = data_->getNameToId();
    const auto &found = name_to_id.find(name);
    if (found == name_to_id.end()) {
        //TODO error message
        return kInvalidClockId;
    }
    return found->second;
}

const std::string &SdcClockContainer::getClockName(const ClockId &id) const {
    const ClockPtr &clock = getClock(id);
    if (!clock) {
        return ClockContainerData::default_clock_name_;
    } 
    return clock->getName();
}

void SdcClockContainer::getClockOnPin(std::vector<ClockPtr> &clocks, const ObjectId &pin_id) const {
    clocks.clear();
    const auto &pin_clock_value = data_->getPinClockValue();
    const auto &pin_view = pin_clock_value.left;
    const auto &range = pin_view.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &clock_id = it->second;
        const auto &clock = getClock(clock_id);
        if (!clock) {
            //error messages
            continue;
        }
        clocks.emplace_back(clock);
    }
}

void SdcClockContainer::getPinOnClock(std::vector<ObjectId> &pins, const ClockId &id) const {
    pins.clear();
    const auto &pin_clock_value = data_->getPinClockValue();
    const auto &clock_view = pin_clock_value.right;
    const auto &range = clock_view.equal_range(id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &pin_id = it->second;
        pins.emplace_back(pin_id);
    }
}

bool SdcClockContainer::isClockPin(const ObjectId &pin_id) const {
    const auto &pin_clock_value = data_->getPinClockValue();
    const auto &pin_view = pin_clock_value.left;
    const std::size_t num = pin_view.count(pin_id);
    if (num >= 1) {
        return true;
    }
    return false;
}

std::ostream &operator<<(std::ostream &os, SdcClockContainer &rhs) {
    const auto &pin_clock_value = (rhs.data_)->getPinClockValue();
    const auto &create_clocks = (rhs.data_)->getCreateClocks();
    const auto &create_generated_clocks = (rhs.data_)->getCreateGeneratedClocks();
    const auto &clock_view = pin_clock_value.right;
    for (const auto &id_to_create_clock : create_clocks) {
        const ClockId &clock_id = id_to_create_clock.first;
        const auto &create_clock = id_to_create_clock.second;
        const ClockPtr &clock = rhs.getClock(clock_id);
        os  << "create_clock ";
        os  << "-period " << clock->getPeriod();
        os  << "-name " << clock->getName();
        os  << "-comment " << create_clock->getComment();
        os  << "-waveform {";
        copy(clock->getWaveform().begin(), clock->getWaveform().end(), std::ostream_iterator<float>(os, " "));
        os  << " } ";
        if (create_clock->isAdd()) {
            os  << "-add ";
        }
        const auto &range = clock_view.equal_range(clock_id);
        for (auto it = range.first; it != range.second; ++it) {
            const auto &pin_id = it->second;
            const auto &pin_name = getPinFullName(pin_id);
            if (pin_name.empty()) {
                // error messages
                continue;
            }
            os << pin_name << " ";
        }
        os << "\n";
    }
    for (const auto &id_to_generated_clock : create_generated_clocks) {
        const ClockId &clock_id = id_to_generated_clock.first;
        const auto &generated_clock = id_to_generated_clock.second;
        const ClockPtr &clock = rhs.getClock(clock_id);
        os  << "create_generated_clock ";
        os  << "-name " << clock->getName();
        os  << "-source ";
        for (const auto &pin_id: generated_clock->getSourceMasterPins()) {
            Pin* pin = Object::addr<Pin>(pin_id);
            if (!pin) {
                // error messages
                continue;
            }
            const auto &pin_name = pin->getName();
            os << pin_name << " ";
        }
        os  << "-divided_by " << generated_clock->getDividedBy()
            << "-multiply_by " << generated_clock->getMultiplyBy();
        os  << "-edges {" ;
        copy(generated_clock->getEdges().begin(), generated_clock->getEdges().end(), std::ostream_iterator<int>(os, " "));
        os  << " } ";
        if (generated_clock->isCombinational()) {
            os  << "-combinational ";
        }
        os  << "-duty_cycle " << generated_clock->getDutyCycle();
        if (generated_clock->isInvert()) {
            os  << "-invert ";
        }
        os  << "-edge_shift {";
        copy(generated_clock->getEdgeShifts().begin(), generated_clock->getEdgeShifts().end(), std::ostream_iterator<float>(os, " "));
        os  << " } ";
        if (generated_clock->isAdd()) {
            os  << "-add ";
        }
        const auto &master_clock = generated_clock->getMasterClock();
        os  << "-master_clock " << rhs.getClockName(master_clock);
        os  << "-comment " << generated_clock->getComment();

        const auto &range = clock_view.equal_range(clock_id);
        for (auto it = range.first; it != range.second; ++it) {
            const auto &pin_id = it->second;
            const auto &pin_name = getPinFullName(pin_id);
            if (pin_name.empty()) {
                // error messages
                continue;
            }
            os << pin_name << " ";
        }
        os << "\n";
    }
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcGroupPathContainer &rhs) {
    //TODO
    return os;
}

const SetClockGatingCheckPtr SdcClockGatingCheckContainer::getCurrentDesignCheckTime() const {
    const auto &cell = getTopCell();
    const auto &cell_id = cell->getId();
    const auto &design_to_check = data_->getDesignToCheck();
    const auto &found = design_to_check.find(cell_id);
    if (found == design_to_check.end()) {
        return nullptr;
    }
    return found->second;
}

const std::pair<float, float> SdcClockGatingCheckContainer::getPinCheckTime(bool is_rise, bool is_high, const ObjectId &pin_id) const {
    const auto &design_check = getCurrentDesignCheckTime();
    if (design_check) {
        return std::pair<float, float>(design_check->getSetup(), design_check->getHold());
    }
    //TODO need to consider the instance check?
    const auto &pin_to_check = data_->getPinToCheck();
    const auto &range = pin_to_check.equal_range(pin_id);
    for(auto it = range.first; it != range.second; ++it) {
        const auto &check = it->second;
        if (!check) {
            // error messages
            continue;
        }
        if (check->getRise()!=is_rise or check->getHigh()!=is_high) {
            continue;
        }
        return std::pair<float, float>(check->getSetup(), check->getHold());
    }
    return std::pair<float, float>(0, 0);
}

const std::pair<float, float> SdcClockGatingCheckContainer::getInstCheckTime(bool is_rise, bool is_high, const ObjectId &inst_id) const {
    const auto &design_check = getCurrentDesignCheckTime();
    if (design_check) {
        return std::pair<float, float>(design_check->getSetup(), design_check->getHold());
    }
    const auto &inst_to_check = data_->getInstToCheck();
    const auto &range = inst_to_check.equal_range(inst_id);
    for(auto it = range.first; it != range.second; ++it) {
        const auto &check = it->second;
        if (!check) {
            // error messages
            continue;
        }
        if (check->getRise()!=is_rise or check->getHigh()!=is_high) {
            continue;
        }
        return std::pair<float, float>(check->getSetup(), check->getHold());
    }
    return std::pair<float, float>(0, 0);
}

const std::pair<float, float> SdcClockGatingCheckContainer::getClockCheckTime(bool is_rise, bool is_high, const ClockId &clock_id) const {
    const auto &design_check = getCurrentDesignCheckTime();
    if (design_check) {
        return std::pair<float, float>(design_check->getSetup(), design_check->getHold());
    }
    const auto &clock_to_check = data_->getClockToCheck();
    const auto &range = clock_to_check.equal_range(clock_id);
    for(auto it = range.first; it != range.second; ++it) {
        const auto &check = it->second;
        if (!check) {
            // error messages
            continue;
        }
        if (check->getRise()!=is_rise or check->getHigh()!=is_high) {
            continue;
        }
        return std::pair<float, float>(check->getSetup(), check->getHold());
    }
    return std::pair<float, float>(0, 0);
}

const float SdcClockGatingCheckContainer::getPinCheckTime(bool is_rise, bool is_high, bool is_setup, const ObjectId &pin_id) const {
   const std::pair<float, float>& setup_hold = getPinCheckTime(is_rise, is_high, pin_id);
   return is_setup ? setup_hold.first : setup_hold.second;
}

const float SdcClockGatingCheckContainer::getInstCheckTime(bool is_rise, bool is_high, bool is_setup, const ObjectId &inst_id) const {
   const std::pair<float, float>& setup_hold = getInstCheckTime(is_rise, is_high, inst_id);
   return is_setup ? setup_hold.first : setup_hold.second;
}

const float SdcClockGatingCheckContainer::getClockCheckTime(bool is_rise, bool is_high, bool is_setup, const ClockId &clock_id) const {
   const std::pair<float, float>& setup_hold = getClockCheckTime(is_rise, is_high, clock_id);
   return is_setup ? setup_hold.first : setup_hold.second;
}

std::ostream &operator<<(std::ostream &os, SdcClockGatingCheckContainer &rhs) {
    //TODO
    return os;
}

RelationshipType SdcClockGroupsContainer::getClocksRelationshipType(const ClockId &clock_id, const ClockId &other_clock_id) const {
    UnorderedPair<ClockId, ClockId> clock_pair(clock_id, other_clock_id);
    const auto &clock_relationship = data_->getClockRelationship();
    const auto &found = clock_relationship.find(clock_pair);
    if (found == clock_relationship.end()) {
        return RelationshipType::kUnknown;
    }
    return found->second;
}

std::ostream &operator<<(std::ostream &os, SdcClockGroupsContainer &rhs) {
    //TODO
    return os;
}

void SdcClockLatencyContainer::getClockLatencyOnPin(std::vector<ClockLatencyOnPinPtr> &latencys, const ObjectId &pin_id) const {
    const auto &pin_to_latency = data_->getPinToLatency();
    const auto &range = pin_to_latency.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &latency = it->second;
        latencys.emplace_back(latency);
    }
}

void SdcClockLatencyContainer::getClockLatencyOnClock(std::vector<SetClockLatencyPtr> &latencys, const ClockId &clock_id) const {
    const auto &clock_to_latency = data_->getClockToLatency();
    const auto &range = clock_to_latency.equal_range(clock_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &latency = it->second;
        latencys.emplace_back(latency);
    }
}

std::ostream &operator<<(std::ostream &os, SdcClockLatencyContainer &rhs) {
    //TODO
    return os;
}

void SdcSenseContainer::getPinSense(std::vector<SetSensePtr> &senses, const ObjectId &pin_id) const {
    const auto &pin_sense = data_->getPinSense();
    const auto &range = pin_sense.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &sense = it->second;
        senses.emplace_back(sense);
    }
}

std::ostream &operator<<(std::ostream &os, SdcSenseContainer &rhs) {
    const auto &data = rhs.getData();
    const auto &pin_sense_map = data->getPinSense();
    const auto &clock_container = rhs.getClockContainer();
    for (const auto &pin_to_sense : pin_sense_map) {
        const ObjectId &pin_id = pin_to_sense.first;
        const SetSensePtr &sense = pin_to_sense.second;
        os << "set_sense ";
        os << "-type " << toString(sense->getType()) << " ";
        const Flag &f1 = Flag("-non_unate", sense->getNonUnate());
        const Flag &f2 = Flag("-positive", sense->getPositive());
        const Flag &f3 = Flag("-negative", sense->getNegative());
        const Flag &f4 = Flag("-clock_leaf", sense->getClockLeaf());
        const Flag &f5 = Flag("-stop_propagation", sense->getStopPropation());
        os << ContainerDataPrint::getFlag(f1, f2, f3, f4, f5) << " ";
        os << "-clock { ";
        for (const ClockId &clock_id: sense->getClocks()) {
            os << clock_container->getClockName(clock_id) << " ";
        }
        os << " }";
        os << "\n";
    }
    return os;
}

void SdcClockTransitionContainer::getTransition(std::vector<SetClockTransitionPtr> &transitions, const ClockId &clock_id) const {
    const auto &clock_transitions = data_->getClockTransitions();
    const auto &range = clock_transitions.equal_range(clock_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &transition = it->second;
        transitions.emplace_back(transition);
    }
}

std::ostream &operator<<(std::ostream &os, SdcClockTransitionContainer &rhs) {
    //TODO
    return os;
}

const float SdcClockUncertaintyContainer::getUncertaintyOnInterClocks(bool is_setup, const ClockId &from_clock_id, const ClockId &to_clock_id) const {
    const auto is_hold = not is_setup;
    const auto &clock_pair = std::make_pair(from_clock_id, to_clock_id);
    const auto &inter_clock_uncertainty = data_->getInterClockUncertainty();
    const auto &range = inter_clock_uncertainty.equal_range(clock_pair);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &clock_uncertainty = it->second;
        if (clock_uncertainty->getSetup() == is_setup or clock_uncertainty->getHold() == is_hold) {
            return clock_uncertainty->getUncertainty();
        }
    }
    return 0.0;
}

const float SdcClockUncertaintyContainer:: getUncertaintyOnPin(bool is_setup, const ObjectId &pin_id) const {
    const auto is_hold = not is_setup;
    const auto &pin_uncertainty = data_->getPinUncertainty();
    const auto &range = pin_uncertainty.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &uncertainty = it->second;
        if (uncertainty->getSetup() == is_setup or uncertainty->getHold() == is_hold) {
            return uncertainty->getUncertainty();
        }
    }
    return 0.0;
}

const float SdcClockUncertaintyContainer::getUncertaintyOnClock(bool is_setup, const ClockId &clock_id) const {
    const auto is_hold = not is_setup;
    const auto &clock_uncertainty = data_->getClockUncertainty();
    const auto &range = clock_uncertainty.equal_range(clock_id);
    for (auto it = range.first; it != range.second; ++it) {
        const auto &uncertainty = it->second;
        if (uncertainty->getSetup() == is_setup or uncertainty->getHold() == is_hold) {
            return uncertainty->getUncertainty();
        }
    }
    return 0.0;
}

std::ostream &operator<<(std::ostream &os, SdcClockUncertaintyContainer &rhs) {
    //TODO
    return os;
}

void SdcDataCheckContainer::getDataCheckOnInterPins(std::vector<SetDataCheckPtr>& checks, const ObjectId &from_pin_id, const ObjectId &to_pin_id) const {
    const auto &pin_data_check = data_->getPinDataCheck();
    const auto &range = pin_data_check.equal_range(PinPair(from_pin_id, to_pin_id));
    for (auto it = range.first; it != range.second; ++it) {
        checks.emplace_back(it->second);
    }
}

std::ostream &operator<<(std::ostream &os, SdcDataCheckContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcDisableTimingContainer &rhs) {
    const auto &data = rhs.data_;
    const auto &cell_disable_timing = data->getCellDisableTiming();
    const auto &inst_disable_timing = data->getInstDisableTiming();
    const auto &pin_disable_timing = data->getPinDisableTiming();
    for (const auto &cell_to_timing: cell_disable_timing) {
        const ObjectId &cell_id = cell_to_timing.first;
        const SetDisableTimingPtr &timing = cell_to_timing.second;
        const ObjectId &from_id = timing->getFromId();
        const ObjectId &to_id = timing->getToId();
        os << "set_disable_timing ";
        os << "-from " << ContainerDataPrint::termIdToName(from_id) << " ";
        os << "-to " << ContainerDataPrint::termIdToName(to_id) << " ";
        os << ContainerDataPrint::cellIdToName(cell_id);
        os << "\n";
    }
    for (const auto &inst_to_timing: inst_disable_timing) {
        const ObjectId &inst_id = inst_to_timing.first;
        const SetDisableTimingPtr &timing = inst_to_timing.second;
        const ObjectId &from_id = timing->getFromId();
        const ObjectId &to_id = timing->getToId();
        os << "set_disable_timing ";
        os << "-from " << ContainerDataPrint::pinIdToFullName(from_id) << " ";
        os << "-to " << ContainerDataPrint::pinIdToFullName(to_id) << " ";
        os << ContainerDataPrint::instIdToName(inst_id);
        os << "\n";
    }
    for (const auto &pin_id: pin_disable_timing) {
        os << "set_disable_timing ";
        os << ContainerDataPrint::pinIdToFullName(pin_id);
        os << "\n";
    }
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcFalsePathContainer &rhs) {
    //TODO
    return os;
}

void SdcIdealLatencyContainer::getPinLatency(std::vector<SetIdealLatencyPtr> &latencys, const ObjectId &pin_id) const {
    latencys.clear();
    const auto &pin_to_latency = data_->getPinToLatency();
    const auto &range = pin_to_latency.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        latencys.emplace_back(it->second);
    }
}

void  SdcIdealTransitionContainer::getPinTransition(std::vector<SetIdealTransitionPtr> &transitions, const ObjectId &pin_id) const {
    const auto &pin_ideal_transitions = data_->getPinIdealTransitions();
    const auto &range = pin_ideal_transitions.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        transitions.emplace_back(it->second);
    }
}

std::ostream &operator<<(std::ostream &os, SdcIdealLatencyContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcIdealNetworkContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcIdealTransitionContainer &rhs) {
    //TODO
    return os;
}

void SdcInputDelayContainer::getPinInputDelay(std::vector<SetInputDelayPtr> &delays, const ObjectId &pin_id) const {
    const auto &pin_input_delays = data_->getPinInputDelays();
    const auto &range = pin_input_delays.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        delays.emplace_back(it->second);
    }
}

const float SdcMaxTimeBorrowContainer::getMaxTimeBorrowOnPin(const ObjectId &pin_id) {
    const auto &pin_max_time_borrow = data_->getPinMaxTimeBorrow();
    const auto &found = pin_max_time_borrow.find(pin_id);
    if (found == pin_max_time_borrow.end()) {
        return 0.0;
    }
    const auto &time_borrow = found->second;
    return time_borrow.getValue();
}

std::ostream &operator<<(std::ostream &os, SdcInputDelayContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcMaxDelayContainer &rhs) {
    //TODO
    return os;
}

const float SdcMaxTimeBorrowContainer::getMaxTimeBorrowOnInst(const ObjectId &inst_id) {
    const auto &inst_max_time_borrow = data_->getInstMaxTimeBorrow();
    const auto &found = inst_max_time_borrow.find(inst_id);
    if (found == inst_max_time_borrow.end()) {
        return 0.0;
    }
    const auto &time_borrow = found->second;
    return time_borrow.getValue();
}

const float SdcMaxTimeBorrowContainer::getMaxTimeBorrowOnClock(const ClockId &clock_id) {
    const auto &clock_max_time_borrow = data_->getClockMaxTimeBorrow();
    const auto &found = clock_max_time_borrow.find(clock_id);
    if (found == clock_max_time_borrow.end()) {
        return 0.0;
    }
    const auto &time_borrow = found->second;
    return time_borrow.getValue();
}

std::ostream &operator<<(std::ostream &os, SdcMaxTimeBorrowContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcMinDelayContainer &rhs) {
    //TODO
    return os;
}

void SdcMinPulseWidthContainer::getMinPulseWidthOnPin(std::vector<SetMinPulseWidthPtr> &pulse_width, const ObjectId &pin_id) const {
    pulse_width.clear();
    const auto &pin_min_pulse_width = data_->getPinMinPulseWidth();
    const auto &range = pin_min_pulse_width.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        pulse_width.emplace_back(it->second);
    }
}

void SdcMinPulseWidthContainer::getMinPulseWidthOnInst(std::vector<SetMinPulseWidthPtr> &pulse_width, const ObjectId &inst_id) const {
    pulse_width.clear();
    const auto &inst_min_pulse_width = data_->getInstMinPulseWidth();
    const auto &range = inst_min_pulse_width.equal_range(inst_id);
    for (auto it = range.first; it != range.second; ++it) {
        pulse_width.emplace_back(it->second);
    }
}

void SdcMinPulseWidthContainer::getMinPulseWidthOnClock(std::vector<SetMinPulseWidthPtr> &pulse_width, const ObjectId &clock_id) const {
    pulse_width.clear();
    const auto &clock_min_pulse_width = data_->getClockMinPulseWidth();
    const auto &range = clock_min_pulse_width.equal_range(clock_id);
    for (auto it = range.first; it != range.second; ++it) {
        pulse_width.emplace_back(it->second);
    }
}

std::ostream &operator<<(std::ostream &os, SdcMinPulseWidthContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcMulticyclePathContainer &rhs) {
    //TODO
    return os;
}

void SdcOutputDelayContainer::getPinOutputDelay(std::vector<SetOutputDelayPtr> &delays, const ObjectId &pin_id) const {
    const auto &pin_output_delays = data_->getPinOutputDelays();
    const auto &range = pin_output_delays.equal_range(pin_id);
    for (auto it = range.first; it != range.second; ++it) {
        delays.emplace_back(it->second);
    }
}

std::ostream &operator<<(std::ostream &os, SdcOutputDelayContainer &rhs) {
    //TODO
    return os;
}

bool SdcPropagatedClockContainer::isPropagatedPin(const ObjectId &pin_id) const {
    const auto &pins = data_->getPins();
    const auto &found = pins.find(pin_id);
    if (found != pins.end()) {
        return true;
    }
    return false;
}

std::ostream &operator<<(std::ostream &os, SdcPropagatedClockContainer &rhs) {
    //TODO
    return os;
}

// environment commands
const CaseValue SdcCaseAnalysisContainer::getPinValue(const ObjectId &pin_id) const {
    const auto &pin_to_case_analysis = data_->getPinToCaseAnalysis();
    const auto &found = pin_to_case_analysis.find(pin_id);
    if (found != pin_to_case_analysis.end()) {
        const auto &case_analysis = found->second;
        return case_analysis.getValue();
    }
    return CaseValue::kUnknown;
}

bool SdcCaseAnalysisContainer::isPinConstZero(const ObjectId &pin_id) const {
    const auto& value = getPinValue(pin_id);
    return (value == CaseValue::kZero);
}

bool SdcCaseAnalysisContainer::isPinConstOne(const ObjectId &pin_id) const {
    const auto& value = getPinValue(pin_id);
    return (value == CaseValue::kOne);
}

std::ostream &operator<<(std::ostream &os, SdcCaseAnalysisContainer &rhs) {
    const auto &pin_to_case_analysis = rhs.data_->getPinToCaseAnalysis();
    for ( const auto &pin_value : pin_to_case_analysis ) {
        const auto &pin_id = pin_value.first;
        const auto &pin = Object::addr<Pin>(pin_id);
        if (!pin) {
            //TODO messages
            continue;
        }
        const auto& case_analysis = pin_value.second;
        os << "set_case_analysis " << toString(case_analysis.getValue()) << " " << pin->getName() << "\n";
    }
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcDriveContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcDrivingCellContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcFanoutLoadContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcInputTransitionContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcLoadContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcLogicContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcMaxAreaContainer &rhs) {
    //TODO
    return os;
}

const SetMaxCapacitancePtr SdcMaxCapacitanceContainer::getPinCap(const ObjectId &pin_id, const ObjectId &top_cell_id) const {
    const auto &design_max_caps = data_->getDesignMaxCaps();
    const auto &found = design_max_caps.find(top_cell_id);
    if (found != design_max_caps.end()) {
        return found->second;
    }
    const auto &pin_max_caps = data_->getPinMaxCaps();
    const auto &found_pin = pin_max_caps.find(pin_id);
    if (found_pin != pin_max_caps.end()) {
        return found_pin->second;
    }
    return nullptr;
}

std::ostream &operator<<(std::ostream &os, SdcMaxCapacitanceContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcMaxFanoutContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcMaxTransitionContainer &rhs) {
    //TODO
    return os;
}

const SetMinCapacitancePtr SdcMinCapacitanceContainer::getPinCap(const ObjectId &pin_id, const ObjectId &top_cell_id) const {
    const auto &design_min_caps = data_->getDesignMinCaps();
    const auto &found = design_min_caps.find(top_cell_id);
    if (found != design_min_caps.end()) {
        return found->second;
    }
    const auto &pin_min_caps = data_->getPinMinCaps();
    const auto &found_pin = pin_min_caps.find(pin_id);
    if (found_pin != pin_min_caps.end()) {
        return found_pin->second;
    }
    return nullptr;
}

std::ostream &operator<<(std::ostream &os, SdcMinCapacitanceContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcOperatingConditionsContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcPortFanoutNumberContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcResistanceContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcTimingDerateContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcVoltageContainer &rhs) {
    //TDOD
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcWireLoadMinBlockSizeContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcWireLoadModeContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcWireLoadModelContainer &rhs) {
    //TODO
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcWireLoadSelectionGroupContainer &rhs) {
    //TODO
    return os;
}

//multivoltage power commands
const CreateVoltageAreaPtr SdcVoltageAreaContainer::getCellVoltageArea(const ObjectId &cell_id) const {
   const auto &cell_voltage_area = data_->getCellVoltageArea();
   const auto &found = cell_voltage_area.find(cell_id);
   if (found == cell_voltage_area.end()) {
       // messages;
       return nullptr;
   }
   return found->second;
}

std::ostream &operator<<(std::ostream &os, SdcVoltageAreaContainer &rhs) {
    if (!rhs.data_) {
        // error messages
        return os;
    }
    const auto &cell_voltage_area_map = rhs.data_->getCellVoltageArea();
    for (const auto &cell_to_area : cell_voltage_area_map) {
        const auto &cell_id = cell_to_area.first;
        const auto &voltage_area = cell_to_area.second;
        if (!voltage_area) {
            // error message
            continue;
        }
        const auto &box_vector = voltage_area->getCoordinates();
        const auto &cell = Object::addr<Cell>(cell_id);
        if (!cell) {
            //error message
            continue;
        }
        const auto &cell_name = cell->getName();
        os  << "create_voltage_area "
            << "-name " << voltage_area->getName();
        os  << "-corrdinate ";
        for (const auto &box : box_vector) {
            if (!box) {
                // error messages
                continue;
            }
            os << "{ " << box->getLLX() << " " << box->getLLY() << " " << box->getURX() << " " << box->getURY() << " } ";
        }
        os  << "-guard_band_x " << voltage_area->getGuardBandX()
            << "-guard_band_y " << voltage_area->getGuardBandY()
            << cell_name;
        os  << "\n";
    }
    return os;
}


std::ostream &operator<<(std::ostream &os, SdcLevelShifterStrategyContainer &rhs) {
    os << "set_level_shifter_strategy " << toString(rhs.getLevelShifterStrategy()) << "\n";
    return os;
}

std::ostream &operator<<(std::ostream &os, SdcLevelShifterThresholdContainer &rhs) {
    os  << "set_level_shifter_threshold ";
    os  << "-voltage " << rhs.getVoltage()
        << "-percent " << rhs.getPercent()
        << "\n";
    return os;
}

const float SdcMaxDynamicPowerContainer::getCellPower(const ObjectId &cell_id) const {
    if (!data_) {
        // error message
        return 0.0;
    }
    const auto &cell_power = data_->getDynamicPower();
    const auto &found = cell_power.find(cell_id);
    if (found == cell_power.end()) {
        // message
        return 0.0;
    }
    const auto &power = found->second;
    if (!power) {
        // message
        return 0.0;
    }
    return power->getPowerValue();
}

std::ostream &operator<<(std::ostream &os, SdcMaxDynamicPowerContainer &rhs) {
    if (!rhs.data_) {
        // error message
        return os;
    }
    const auto &cell_power_map = rhs.data_->getDynamicPower();
    for (const auto &cell_to_power : cell_power_map) {
        const auto &cell_id = cell_to_power.first;
        const auto &cell = Object::addr<Cell>(cell_id);
        if (!cell) {
            // message
            continue;
        }
        const auto &cell_name = cell->getName();
        const auto &power_value = cell_to_power.second;
        os  << "set_max_dynamic_power ";
        os  << "-power " << power_value
            << "-unit " << "W"
            << "#cell_name " << cell_name
            << "\n";
    }
    return os;
}

const float SdcMaxLeakagePowerContainer::getCellPower(const ObjectId &cell_id) const {
    if (!data_) {
        // error message
        return 0.0;
    }
    const auto &cell_power = data_->getLeakagePower();
    const auto &found = cell_power.find(cell_id);
    if (found == cell_power.end()) {
        // message
        return 0.0;
    }
    const auto &power = found->second;
    if (!power) {
        // message
        return 0.0;
    }
    return power->getPowerValue();
}

std::ostream &operator<<(std::ostream &os, SdcMaxLeakagePowerContainer &rhs) {
    if (!rhs.data_) {
        // error message
        return os;
    }
    const auto &cell_power_map = rhs.data_->getLeakagePower();
    for (const auto &cell_to_power : cell_power_map) {
        const auto &cell_id = cell_to_power.first;
        const auto &cell = Object::addr<Cell>(cell_id);
        if (!cell) {
            // message
            continue;
        }
        const auto &cell_name = cell->getName();
        const auto &power_value = cell_to_power.second;
        os  << "set_max_leakage_power ";
        os  << "-power " << power_value
            << "-unit " << "W"
            << "#cell_name " << cell_name
            << "\n";
    }
    return os;
}



//object access commands
const std::string SdcCurrentDesignContainer::getDesignName() const {
    const ObjectId &cell_id = data_->getCellId();
    Cell* cell = Object::addr<Cell>(cell_id);
    if (!cell) {
        return "";
    }
    return cell->getName();
}

}
}
