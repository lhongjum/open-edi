/**
 * @file sdc_common.cpp
 * @date 2020-11-25
 * @brief
 *
 * Copyright (C) 2020 NIIC EDA
 *
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 *
 * of the BSD license.  See the LICENSE file for details.
 */


#include "sdc_common.h"

namespace open_edi {
namespace db {

Pin* getPinByFullName(const std::string &full_name) {
    //TODO
    //Maybe separator is not "/"
    const auto &pos = full_name.find_last_of('/');
    const auto &top_cell = getTopCell();
    if (pos == std::string::npos) { // port
        return top_cell->getIOPin(full_name);
    }
    const auto &inst_name = full_name.substr(0, pos);
    const auto &pin_name = full_name.substr(pos+1);
    const auto &inst = top_cell->getInstance(inst_name);
    if (!inst) {
        return nullptr;
    }
    return inst->getPin(pin_name);
}

std::string getPinFullName(const ObjectId &pin_id) {
    //TODO
    //Maybe separator is not "/"
    Pin* pin = Object::addr<Pin>(pin_id);
    if (!pin) {
        // error messages
        return "";
    }
    const auto &pin_name = pin->getName();
    const auto &inst = pin->getInst();
    if (!inst) {
        // error messages
        return "";
    }
    const auto &inst_name = inst->getName();
    auto full_name = inst_name + "/" + pin_name;
    return full_name;
}

bool isCellInOtherCell(const ObjectId &cell_id, const ObjectId &other_cell_id) {
    if ((cell_id == UNINIT_OBJECT_ID) or (other_cell_id == UNINIT_OBJECT_ID)) {
        return false;
    }
    if (cell_id == other_cell_id) {
        return true;
    }
    const Cell *top_cell = getTopCell();
    const ObjectId &top_cell_id = top_cell->getId();
    if (other_cell_id == top_cell_id) {
        return true;
    }
    const Cell *cell = Object::addr<Cell>(cell_id);
    assert(cell);
    if (!cell) {
        // error messages
        return false;
    }
    ObjectId up_cell_id = cell->getOwnerId();
    while (up_cell_id != top_cell_id) {
        if (up_cell_id == other_cell_id) {
            return true;
        }
        up_cell_id = cell->getOwnerId();
    }
    return false;
}

bool isInstInCell(const ObjectId &inst_id, const ObjectId &cell_id) {
    Inst* inst = Object::addr<Inst>(inst_id);
    if (!inst) {
        // error messages
        return false;
    }
    Cell *master_cell = inst->getMaster();
    assert(master_cell);
    bool ret = isCellInOtherCell(master_cell->getId(), cell_id);
    return ret;
}

TCell* getLibCellInCorner(const ObjectId& analysis_corner_id, const std::string &liberty_name, const std::string &cell_name) {
    const auto &analysis_corner = Object::addr<AnalysisCorner>(analysis_corner_id);
    assert(analysis_corner);
    if (!analysis_corner) {
        //TODO messages
        return nullptr;
    }
    LibSet* lib_set = analysis_corner->getLibset();
    assert(lib_set);
    if (!lib_set) {
        //TODO messages
        return nullptr;
    }
    const std::vector<TLib*>& lib_vec = lib_set->getTimingLibs();
    for (const auto &lib : lib_vec) {
        if (!lib) {
            //TODO messages
            continue;
        }
        if (lib->getName() == liberty_name) {
            return lib->getTimingCell(cell_name);
        }
    }
    return nullptr;
}

void getSdcFileContents(std::string &contents, const std::vector<std::string> &files,  const std::string &mode_name) {
    contents = "";
    contents.reserve(50000);
    std::string line = "";
    for (const std::string &file : files) {
        std::ifstream input(file);
        if (!input) {
            //TODO messages
            continue;
        }
        while (std::getline(input, line)) {
            boost::trim(line);
            if (line.empty() or line[0] == '#') {
                continue;
            }
            contents += line + " -mode " + mode_name + " \n";
        }
    }
}

std::string ContainerDataPrint::pinIdToName(const ObjectId &pin_id) {
    return idToName<Pin>(pin_id);
}

std::string ContainerDataPrint::termIdToName(const ObjectId &term_id) {
    return idToName<Term>(term_id);
}

std::string ContainerDataPrint::cellIdToName(const ObjectId &cell_id) {
    return idToName<Cell>(cell_id);
}

std::string ContainerDataPrint::instIdToName(const ObjectId &inst_id) {
    return idToName<Inst>(inst_id);
}

std::string ContainerDataPrint::pinIdToFullName(const ObjectId &pin_id) {
    return getPinFullName(pin_id);
}

std::string ContainerDataPrint::pinIdVectorToNameList(const std::vector<ObjectId> &pin_ids) {
    return idVectorToNameList<Pin>(pin_ids);
}

std::string ContainerDataPrint::termIdVectorToNameList(const std::vector<ObjectId> &term_ids) {
    return idVectorToNameList<Term>(term_ids);
}

std::string ContainerDataPrint::cellIdVectorToNameList(const std::vector<ObjectId> &cell_ids) {
    return idVectorToNameList<Cell>(cell_ids);
}

std::string ContainerDataPrint::instIdVectorToNameList(const std::vector<ObjectId> &inst_ids) {
    return idVectorToNameList<Inst>(inst_ids);
}

std::string ContainerDataPrint::pinIdVectorToFullNameList(const std::vector<ObjectId> &pin_ids) {
    std::string ret = "";
    for (const ObjectId &pin_id: pin_ids) {
        ret += getPinFullName(pin_id);
    }
    return ret;
}

std::string ContainerDataPrint::getFlag(const Flag &p) {
    const std::string &flag_str = p.first;
    const bool &flag = p.second;
    return flag ? flag_str : "";
}

}
}
