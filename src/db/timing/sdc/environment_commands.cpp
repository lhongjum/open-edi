/**
 * @file environment_commands.cpp
 * @date 2020-11-25
 * @brief
 *
 * Copyright (C) 2020 NIIC EDA
 *
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 *
 * of the BSD license.  See the LICENSE file for details.
 */

#include "db/timing/sdc/environment_commands.h"

namespace open_edi {
namespace db {

std::string toString(const CaseValue &value) {
    switch (value) {
        case CaseValue::kZero:
            return "zero";
        case CaseValue::kOne:
            return "one";
        case CaseValue::kRise:
            return "rise";
        case CaseValue::kFall:
            return "fall";
        defalut:
            return "unknown";
    }
    return "unknown";
}

bool SetCaseAnalysis::setValue(std::string& input) {
    if (input=="0" or input=="zero") {
        value_ = CaseValue::kZero;
        return true;
    }
    if (input=="1" or input=="one") {
        value_ = CaseValue::kOne;
        return true;
    }
    if (input=="rising" or input=="rise") {
        value_ = CaseValue::kRise;
        return true;
    }
    if (input=="falling" or input=="fall") {
        value_ = CaseValue::kFall;
        return true;
    }
    value_ = CaseValue::kUnknown;
    return false;
}

bool SetCaseAnalysis::operator==(const SetCaseAnalysis &rhs) const {
    bool ret = (value_ == rhs.getValue());
    return ret;
}

SetDrive::SetDrive() {
    rise_ = false;
    fall_ = false;
    min_ = false;
    max_ = false;
}

void SetDrive::checkFlags() {
    if ((rise_==false) and (fall_==false)) {
        rise_ = true;
        fall_ = true;
    }
    if ((min_==false) and (max_==false)) {
        min_ = true;
        max_ = true;
    }
}

bool SetDrive::operator==(const SetDrive &rhs) const {
    bool ret = ediEqual(resistance_, rhs.resistance_) and (rise_ == rhs.getRise()) and
                (fall_ == rhs.getFall()) and (min_ == rhs.getMin()) and (max_ == rhs.getMax());
    return ret;
}

SetDrivingCell::SetDrivingCell() {
    rise_ = false;
    fall_ = false;
    dont_scale_ = false;
    no_design_rule_ = false;
    min_ = false;
    max_ = false;
    clock_fall_ = false;
}

bool SetDrivingCell::operator==(const SetDrivingCell &rhs) const {
    bool ret = ediEqual(input_transition_rise_, rhs.getInputTransitionRise()) and
                ediEqual(input_transition_fall_, rhs.getInputTransitionFall()) and
                (from_tterm_ == rhs.getFromTTerm()) and (to_tterm_ == rhs.getToTTerm()) and
                (clock_ == rhs.getClock()) and (rise_ == rhs.getRise()) and (fall_ == rhs.getFall()) and
                (dont_scale_ == rhs.getDontScale()) and (no_design_rule_ == rhs.getNoDesignRule()) and
                (min_ == rhs.getMin()) and (max_ == rhs.getMax()) and (clock_fall_ == rhs.getClockFall());
    return ret;
}

bool SetFanoutLoad::operator==(const SetFanoutLoad &rhs) const {
    bool ret = ediEqual(value_, rhs.getValue());
    return ret;
}

SetInputTransition::SetInputTransition() {
    rise_ = false;
    fall_ = false;
    min_ = false;
    max_ = false;
    clock_fall_ = false;
}

void SetInputTransition::checkFlags() {
    if ((rise_==false) and (fall_==false)) {
        rise_ = true;
        fall_ = true;
    }
    if ((min_==false) and (max_==false)) {
        min_ = true;
        max_ = true;
    }
}

bool SetInputTransition::operator==(const SetInputTransition &rhs) const {
    bool ret = ediEqual(transition_, rhs.transition_) and (clocks_ == rhs.clocks_) and
                (rise_ == rhs.getRise()) and (fall_ == rhs.getFall()) and (min_ == rhs.getMin()) and
                (max_ == rhs.getMax()) and (clock_fall_ == rhs.getClockFall());
    return ret;
}

SetLoad::SetLoad() {
    min_ = false;
    max_ = false;
    substract_pin_load_ = false;
    pin_load_ = false;
    wire_load_ = false;
}

void SetLoad::checkFlags() {
    if ((min_==false) and (max_==false)) {
        min_ = true;
        max_ = true;
    }
    if (wire_load_ == pin_load_) {
        wire_load_ = false;
        pin_load_ = true;
    }
}

bool SetLoad::operator==(const SetLoad &rhs) const {
    bool ret = ediEqual(cap_, rhs.cap_) and (min_ == rhs.getMin()) and
                (max_ == rhs.getMax()) and (substract_pin_load_ == rhs.getSubstractPinLoad()) and
                (pin_load_ == rhs.getPinLoad()) and (wire_load_ == rhs.getWireLoad());
    return ret;
}

std::string toString(const LogicValue &value) {
    switch (value) {
        case LogicValue::kZero:
            return "zero";
        case LogicValue::kOne:
            return "one";
        case LogicValue::kDontCare:
            return "dontcare";
        defalut:
            return "unknown";
    }
    return "unknown";
}

bool SetLogic::operator==(const SetLogic &rhs) const {
    bool ret = (value_ == rhs.getValue());
    return ret;
}

bool SetMaxArea::operator==(const SetMaxArea &rhs) const {
    bool ret = ediEqual(area_value_, rhs.getAreaValue());
    return ret;
}

bool SetMaxCapacitance::operator==(const SetMaxCapacitance &rhs) const {
    bool ret = ediEqual(cap_value_, rhs.getCapValue());
    return ret;
}

bool SetMaxFanout::operator==(const SetMaxFanout &rhs) const {
    bool ret = ediEqual(fanout_value_, rhs.getFanoutValue());
    return ret;
}

SetMaxTransition::SetMaxTransition() {
    clock_path_ = false;
    fall_ = false;
    rise_ = false;
}

void SetMaxTransition::checkFlags() {
    if ((rise_==false) and (fall_==false)) {
        rise_ = true;
        fall_ = true;
    }
}

bool SetMaxTransition::operator==(const SetMaxTransition &rhs) const {
    bool ret = ediEqual(transition_value_, rhs.getTransitionValue()) and (clock_path_ == rhs.getClockPath()) and
                (fall_ == rhs.getFall()) and (rise_ == rhs.getRise());
    return ret;
}

bool SetMinCapacitance::operator==(const SetMinCapacitance &rhs) const {
    bool ret = ediEqual(cap_value_, rhs.getCapValue());
    return ret;
}

bool SetPortFanoutNumber::operator==(const SetPortFanoutNumber &rhs) const {
    bool ret = (fanout_number_ == rhs.getFanoutNumber());
    return ret;
}

SetResistance::SetResistance() {
    min_ = false;
    max_ = false;
}

void SetResistance::checkFlags() {
    if ((min_==false) and (max_==false)) {
        min_ = true;
        max_ = true;
    }
}

bool SetResistance::operator==(const SetResistance &rhs) const {
    bool ret = ediEqual(value_, rhs.getValue()) and (min_ == rhs.getMin()) and (max_ == rhs.getMax());
    return ret;
}

SetTimingDerate::SetTimingDerate() {
    min_ = false;
    max_ = false;
    rise_ = false;
    fall_ = false;
    early_ = false;
    late_ = false;
    static_type_ = false;
    dynamic_type_ = false;
    increment_ = false;
    clock_ = false;
    data_ = false;
    net_delay_ = false;
    cell_delay_ = false;
    cell_check_ = false;
}

bool SetTimingDerate::operator==(const SetTimingDerate &rhs) const {
    bool ret = ediEqual(derate_value_, rhs.getDerateValue()) and
                (min_ == rhs.getMin()) and (max_ == rhs.getMax()) and (rise_ == rhs.getRise()) and (fall_ == rhs.getFall()) and
               (early_ == rhs.getEarly()) and (late_ == rhs.getLate()) and (static_type_ == rhs.getStaticType()) and
               (dynamic_type_ == rhs.getDynamicType()) and (increment_ == rhs.getIncrement()) and (clock_ == rhs.getClock()) and
               (data_ == rhs.getData()) and (net_delay_ == rhs.getNetDelay()) and (cell_delay_ == rhs.getCellDelay()) and
               (cell_check_ == rhs.getCellCheck());
    return ret;
}

bool SetVoltage::operator==(const SetVoltage &rhs) const {
    bool ret = ediEqual(max_case_voltage_, rhs.getMaxCaseVoltage()) and ediEqual(min_case_voltage_, rhs.getMinCaseVoltage());
    return ret;
}

bool SetWireLoadMinBlockSize::operator==(const SetWireLoadMinBlockSize &rhs) const {
    bool ret = ediEqual(block_size_, rhs.getBlockSize());
    return ret;
}

bool SetWireLoadMode::set(const std::string &mode_name) {
    mode_ = open_edi::util::toEnumByString<WireLoadMode>(mode_name.c_str());
    if (mode_ == WireLoadMode::kUnknown) {
        return false;
    }
    return true;
}

bool SetWireLoadMode::operator==(const SetWireLoadMode &rhs) const {
    bool ret = (mode_ == rhs.getMode());
    return ret;
}

SetWireLoadModel::SetWireLoadModel() {
    min_ = false;
    max_ = false;
}

template<>
bool ContainerDataAccess::hasLowerInsertPriority(const SetWireLoadModelPtr &v1, const SetWireLoadModelPtr &v2) {
    return true;
}

SetWireLoadSelectionGroup::SetWireLoadSelectionGroup() {
    min_ = false;
    max_ = false;
}

template<>
bool ContainerDataAccess::hasLowerInsertPriority(const SetWireLoadSelectionGroupPtr &v1, const SetWireLoadSelectionGroupPtr &v2) {
    return true;
}



}
}
