#ifndef OPEN_EDI_LAYOUT_LAYOUT_H_
#define OPEN_EDI_LAYOUT_LAYOUT_H_

#include <QColor>
#include <QList>
#include <QString>
#include <QTime>
#include <qmath.h>

#include "../widget/panel/components_listener.h"
#include "../widget/panel/layer_listener.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "graphics_scene.h"
#include "items/li_die_area.h"
#include "items/li_instance.h"
#include "items/li_manager.h"
#include "items/li_net.h"
#include "util/util.h"

namespace open_edi {
namespace gui {

#define LAYOUT_INSTANCE (Layout::getInstance())

class Layout : public LayerListener,
               public ComponentListener {
  public:
    ~Layout();

    static Layout* getInstance() {
        if (!inst_) {
            inst_ = new Layout;
        }
        return inst_;
    }

    LI_DieArea*  li_die_area;
    LI_Instance* li_instances;
    LI_Pin*      li_pins;
    LI_Manager*  li_manager{LI_MANAGER};

    void            setViewSize(int w, int h);
    QList<QString>& getAllLayerNames();
    void            refreshAllComponents();
    void            refreshFitDraw();
    auto            getDieAreaW() { return die_area_w_; };
    auto            getDieAreaH() { return die_area_h_; };
    auto            getScaleFactor() { return scale_factor_; };
    auto            getPhysicalW() { return display_screen_w_size_; };
    auto            getPhysicalH() { return display_screen_h_size_; };
    auto            getAnchorPoint() { return std::tuple<int, int>(li_manager->getAnchorLLX(), li_manager->getAnchorLLY()); };
    void            createLayoutItems();

    //calculate scale factor according screen size
    int calculateScaleFactor(int screen_w, int screen_h, int db_area_w, int db_area_h);

    std::tuple<int, int> initDieArea();
    void                 setSelectedArea(QPointF p1, QPointF p2);
    void                 setSelectedArea(int x, int y, int w, int h);
    void                 setSelectedArea(open_edi::db::Box& area);
    open_edi::db::Box    translateTofitScreen(open_edi::db::Box& box,
                                              int                screen_w,
                                              int                screen_h);

    // set width of selected area in db uint for caculating scale factor
    auto setSelectedAreaW(int w) { selected_area_w_ = w; };
    auto getSelectedAreaW() { return selected_area_w_; };
    // set height of selected area in db uint for caculating scale factor
    auto setSelectedAreaH(int h) { selected_area_h_ = h; };
    auto getSelectedAreaH() { return selected_area_h_; };
    // select whole db area
    void selectWholeArea();
    // view fit area
    void viewFitArea();

    virtual void setLayerVisible(QString name, bool v) override;
    virtual void setLayerColor(QString name, QColor color) override;
    virtual void setLayerSelectable(QString name, bool v) override;
    virtual void setComponentVisible(QString name, bool v) override;
    virtual void setComponentSelectable(QString name, bool v) override;
    virtual void setComponentBrushStyle(QString name, Qt::BrushStyle brush_style) override;

  private:
    //real chip area width in db uint
    int die_area_w_{0};
    //real chip area height in db uint
    int die_area_h_{0};
    //real selected chip area width in db uint for caculating scale factor
    int selected_area_w_{0};
    //real selected chip area height in db uint for caculating scale factor
    int selected_area_h_{0};
    //physical screen view width size
    int display_screen_w_size_{0};
    //physical screen view height size
    int display_screen_h_size_{0};
    //scale factor from db to screen
    int scale_factor_{1};
    //singleton instance
    static Layout* inst_;
};
} // namespace gui
} // namespace open_edi

#endif