
#include "graphics_scene.h"

namespace open_edi {
namespace gui {

GraphicsScene::GraphicsScene(QObject* parent) {
    selectionRectItem_ = new QGraphicsRectItem;
    selectionRectItem_->setPen(QPen(QColor(120, 170, 255, 255), 0));
    selectionRectItem_->setBrush(QBrush(QColor(150, 200, 255, 80), Qt::SolidPattern));
    selectionRectItem_->setZValue(1000);
    QGraphicsScene::addItem(selectionRectItem_);
}

GraphicsScene::~GraphicsScene() {
}

void GraphicsScene::setSelectionRect(QPointF p1, QPointF p2) {
    selectionRectItem_->setRect(QRectF(p1, p2));
}

void GraphicsScene::drawBackground(QPainter* painter, const QRectF& rect) {
    painter->fillRect(rect, QBrush(QColor("#393939")));

    QPen pen("red");
    painter->setPen(pen);
    QTransform transform(1/5, 0, 0, 1/5, 0, 0);
    // QTransform transform;
    // transform.scale(5,5);
    painter->setTransform(transform);
    painter->drawLine(QPoint(0, 0), QPoint(100, 100));

    // QTransform transform(1 / factor, 0, 0, 1 / factor, 0, 0);
}

} // namespace gui
} // namespace open_edi
