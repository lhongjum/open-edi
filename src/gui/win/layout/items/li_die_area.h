#ifndef LI_DIE_AREA_H
#define LI_DIE_AREA_H

#include <QImage>
#include <qmath.h>

#include "../graphicitems/lgi_die_area.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "li_base.h"
#include "util/util.h"

namespace open_edi {
namespace gui {
class LI_DieArea : public LI_Base {
  public:
    explicit LI_DieArea(int* scale_factor);
    LI_DieArea(const LI_DieArea& other) = delete;
    LI_DieArea& operator=(const LI_DieArea& rhs) = delete;
    ~LI_DieArea();

    virtual void preDraw() override;
    LGI_DieArea* getGraphicItem();
    virtual bool isMainLI() override;
    virtual void preFitDraw() { preDraw(); };
    void setDieAreaSize(int w, int h){die_area_w_ = w, die_area_h_ = h;};

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_DieArea* item_{nullptr};
    int          die_area_w_{0};
    int          die_area_h_{0};
};
} // namespace gui
} // namespace open_edi

#endif
