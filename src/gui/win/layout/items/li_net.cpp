#include "li_net.h"

namespace open_edi {
namespace gui {

LI_Net::LI_Net(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Net;
    item_->setLiBase(this);
    type     = kNet;
    li_wires = new LI_Wire(scale_factor);
    name_    = "Net";
    li_mgr_->addLI(this);
    visible_ = false;
}

LI_Net::~LI_Net() {
}

LGI_Net* LI_Net::getGraphicItem() {
    return item_;
}

bool LI_Net::hasSubLI() {
    return true;
}

bool LI_Net::isMainLI() {
    return true;
}

void LI_Net::setVisible(bool visible) {
    visible_ = visible;
    item_->setVisible(visible);
    li_wires->setVisible(visible);
}

void LI_Net::draw(QPainter* painter) {
    painter->setPen(pen_);
    LI_Base::draw(painter);
}

void LI_Net::preDraw() {
    if (!visible_) {
        return;
    }

    refreshBoundSize();
    img_->fill(Qt::transparent);

    li_wires->refreshBoundSize();
    li_wires->fillImage();

    QPainter painter(img_);
    painter.setPen(pen_);
    painter.setWindow(0,
                      scene_h_,
                      scene_w_ + VIEW_SPACE,
                      -scene_h_ - VIEW_SPACE);

    auto     tc       = open_edi::db::getTopCell();
    uint64_t num_nets = tc->getNumOfNets();
    // printf("Nets %d ;\n", num_nets);

    auto factor = *scale_factor_;

    auto arr_ptr = tc->getNetArray();
    if (arr_ptr) {
        for (auto iter = arr_ptr->begin(); iter != arr_ptr->end(); iter++) {
            auto net = open_edi::db::Object::addr<open_edi::db::Net>(*iter);
            if (!net) continue;
            // printf("net in\n");
            // counting wire
            if (li_wires->isVisible()) {
                auto wire_array = net->getWireArray();
                if (wire_array) {
                    // printf("wire_array in\n");
                    for (auto wire_iter = wire_array->begin(); wire_iter != wire_array->end(); ++wire_iter) {
                        auto wire = open_edi::db::Object::addr<open_edi::db::Wire>(*wire_iter);
                        li_wires->drawWires(*wire);
                    }
                }
            }

            // printf("net in\n");
        }
    }

    item_->setMap(img_);
    item_->setItemSize(scene_w_, scene_h_);

    li_wires->getGraphicItem()->setMap(img_);
    li_wires->getGraphicItem()->setItemSize(scene_w_, scene_h_);
}

} // namespace gui
} // namespace open_edi