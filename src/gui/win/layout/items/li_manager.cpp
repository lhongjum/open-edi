#include <QDebug>
#include <QThreadPool>

#include "li_base.h"
#include "li_die_area.h"
#include "li_instance.h"
#include "li_layer.h"
#include "li_manager.h"

namespace open_edi {
namespace gui {

#define USE_MUTI_THREAD 1

LI_Manager::LI_Manager() {
}

void LI_Manager::preDrawAllItems() {
    for (auto item : li_list_) {
        if (item->isMainLI()) {
#if USE_MUTI_THREAD == 1
            QThreadPool::globalInstance()->start(item);
#else
            item->preDraw();
#endif
        }
    }
#if USE_MUTI_THREAD == 1
    QThreadPool::globalInstance()->waitForDone();
#endif
}

QList<LI_Base*> LI_Manager::getLiList() {
    return li_list_;
}

void LI_Manager::addLI(LI_Base* li) {
    li_list_.append(li);
    li_map_[li->getName()] = li;
}

void LI_Manager::setLiVisibleByName(QString name, bool v) {

    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setVisible(v);
    }
}

void LI_Manager::setLiBrushStyleByName(QString name, Qt::BrushStyle brush_style) {
    if (li_map_.find(name) != li_map_.end()) {
        auto li = li_map_[name];
        li->setBrushStyle(brush_style);
    }
}

LI_Base* LI_Manager::getLiByName(QString name) {
    if (li_map_.find(name) != li_map_.end()) {
        return li_map_[name];
    }
    return nullptr;
}

void LI_Manager::addLayer(open_edi::db::Layer* layer) {
    layer_list_.append(layer);
    auto name = layer->getName();
    // printf("%s\n", name);
    layer_name_list_.append(name);
    // layer_map_[name] = new LI_Layer(name);
}

void LI_Manager::setLayerVisibleByName(QString name, bool v) {
}

void LI_Manager::setLayerColorByName(QString name, QColor color) {
}

LI_Base* LI_Manager::getLayerByName(QString name) {
    return nullptr;
}

QList<QString>& LI_Manager::getLayerNameList() {
    return layer_name_list_;
}
/**
 * LI_Manager 
 * set selected area to db, save objects to each LI 
 * 
 * @param  {QPointF} p1 : db uint coordinate of low left point
 * @param  {QPointF} p2 : db uint coordinate of up right point
 */
void LI_Manager::setSelectedArea(QPointF p1, QPointF p2) {

    open_edi::db::Box area(p1.rx(), p1.ry(), p2.rx(), p2.ry());

    //fetch DB in selected area
    std::vector<open_edi::db::Object*> result;
    open_edi::db::fetchDB(area, &result);

    //save current point after offset
    saveAnchorLLX(area.getLLX());
    saveAnchorLLY(area.getLLY());

    auto li_diearea = dynamic_cast<LI_DieArea*>(getLiByName("Die area"));
    li_diearea->setOffset(-area.getLLX(), -area.getLLY());

    auto li_inst = dynamic_cast<LI_Instance*>(getLiByName("Instance"));

    //set offset to make displya correctly
    li_inst->setOffset(-area.getLLX(), -area.getLLY());
    li_inst->cleaInst();

    // std::vector<open_edi::db::Inst*> inst;

    for (auto obj : result) {
        auto obj_type = obj->getObjectType();
        // qDebug() << obj_type;
        switch (obj_type) {
        case open_edi::db::ObjectType::kObjectTypeInst:
            li_inst->addInst(static_cast<open_edi::db::Inst*>(obj));
            break;
        case open_edi::db::ObjectType::kObjectTypePin:
            break;
        case open_edi::db::ObjectType::kObjectTypeNet:
            break;
        case open_edi::db::ObjectType::kObjectTypeWire:
            break;
        case open_edi::db::ObjectType::kObjectTypeVia:
            break;
        default:
            break;
        }
    }
}

void LI_Manager::setSelectedArea(int x, int y, int w, int h) {
    //fetch DB in selected area
    open_edi::db::Box                  area(x, y, x + w, y + h);
    std::vector<open_edi::db::Object*> result;
    open_edi::db::fetchDB(area, &result);

    //save current point after offset
    saveAnchorLLX(area.getLLX());
    saveAnchorLLY(area.getLLY());

    auto li_diearea = dynamic_cast<LI_DieArea*>(getLiByName("Die area"));
    li_diearea->setOffset(-area.getLLX(), -area.getLLY());

    auto li_inst = dynamic_cast<LI_Instance*>(getLiByName("Instance"));

    //set offset to make displya correctly
    li_inst->setOffset(-area.getLLX(), -area.getLLY());
    li_inst->cleaInst();

    // std::vector<open_edi::db::Inst*> inst;

    for (auto obj : result) {
        auto obj_type = obj->getObjectType();
        // qDebug() << obj_type;
        switch (obj_type) {
        case open_edi::db::ObjectType::kObjectTypeInst:
            li_inst->addInst(static_cast<open_edi::db::Inst*>(obj));
            break;
        case open_edi::db::ObjectType::kObjectTypePin:
            break;
        case open_edi::db::ObjectType::kObjectTypeNet:
            break;
        case open_edi::db::ObjectType::kObjectTypeWire:
            break;
        case open_edi::db::ObjectType::kObjectTypeVia:
            break;
        default:
            break;
        }
    }
}

void LI_Manager::setSelectedArea(open_edi::db::Box area) {

    //fetch DB in selected area
    std::vector<open_edi::db::Object*> result;
    open_edi::db::fetchDB(area, &result);

    //save current point after offset
    saveAnchorLLX(area.getLLX());
    saveAnchorLLY(area.getLLY());

    auto li_diearea = dynamic_cast<LI_DieArea*>(getLiByName("Die area"));
    li_diearea->setOffset(-area.getLLX(), -area.getLLY());

    auto li_inst = dynamic_cast<LI_Instance*>(getLiByName("Instance"));

    //set offset to make displya correctly
    li_inst->setOffset(-area.getLLX(), -area.getLLY());
    li_inst->cleaInst();

    // std::vector<open_edi::db::Inst*> inst;

    for (auto obj : result) {
        auto obj_type = obj->getObjectType();
        // qDebug() << obj_type;
        switch (obj_type) {
        case open_edi::db::ObjectType::kObjectTypeInst:
            li_inst->addInst(static_cast<open_edi::db::Inst*>(obj));
            break;
        case open_edi::db::ObjectType::kObjectTypePin:
            break;
        case open_edi::db::ObjectType::kObjectTypeNet:
            break;
        case open_edi::db::ObjectType::kObjectTypeWire:
            break;
        case open_edi::db::ObjectType::kObjectTypeVia:
            break;
        default:
            break;
        }
    }
}

void LI_Manager::preFitDraw() {
    for (auto item : li_list_) {
        if (item->isMainLI()) {
            item->preFitDraw();
        }
    }
}

LI_Manager* LI_Manager::inst_ = nullptr;
} // namespace gui
} // namespace open_edi