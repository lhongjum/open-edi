#ifndef EDI_GUI_LI_WIRES_H_
#define EDI_GUI_LI_WIRES_H_

#include <QPainter>
#include <qmath.h>
#include "../graphicitems/lgi_wire.h"
#include "../graphics_scene.h"
#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "li_base.h"
#include "util/util.h"

namespace open_edi {
namespace gui {
class LI_Wire : public LI_Base {
  public:
    explicit LI_Wire(int* scale_factor);
    LI_Wire(const LI_Wire& other) = delete;
    LI_Wire& operator=(const LI_Wire& rhs) = delete;
    ~LI_Wire();

    virtual void preDraw() override;
    LGI_Wire*    getGraphicItem();
    void         drawWires(open_edi::db::Wire& wire);
    void         fillImage();
    virtual void preFitDraw(){};

  protected:
    virtual void draw(QPainter* painter);

  private:
    LGI_Wire* item_;
};
} // namespace gui
} // namespace open_edi

#endif
