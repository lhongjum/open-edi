#include "li_instance.h"

#include <QDebug>

namespace open_edi {
namespace gui {

LI_Instance::LI_Instance(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Instance;
    item_->setLiBase(this);
    pen_.setColor(QColor("#909090"));
    type    = kInstance;
    li_pins = new LI_Pin(scale_factor);
    name_   = "Instance";
    li_mgr_->addLI(this);

    //defualt visible
    visible_ = true;
}

LI_Instance::~LI_Instance() {
}

LGI_Instance* LI_Instance::getGraphicItem() {
    return item_;
}

bool LI_Instance::isMainLI() {
    return true;
}

void LI_Instance::draw(QPainter* painter) {
    painter->setPen(pen_);
    LI_Base::draw(painter);
}

void LI_Instance::preDraw() {

    if (!visible_) {
        return;
    }
    refreshBoundSize();
    li_pins->refreshBoundSize();
    li_pins->fillImage();
#if DRAW_MODE == 1
    img_->fill(Qt::transparent);
    QPainter painter(img_);
    painter.setPen(pen_);
    painter.drawRect(QRect(0,0,img_->width()-1,img_->height()-1));
    auto img_w = img_->width();
    auto img_h = img_->height();
    // painter.setWindow(0,
    //                   scene_h_,
    //                   scene_w_ + VIEW_SPACE,
    //                   -scene_h_ - VIEW_SPACE);
#elif DRAW_MODE == 2
    painter_path_ = QPainterPath();
#endif

    auto factor = *scale_factor_;
    // printf("COMPONENTS %d ;\n", num_components);
    Transform transform(1 / (float)factor, 1 / (float)factor, getOffsetX(), getOffsetY());

    // open_edi::db::Box die_area_box(0,0,die_area_w_,die_area_h_);
    // auto li_die_areabox = transform.translate(die_area_box);

    // painter.drawRect(QRect(li_die_areabox.getLLX(),scene_h_ - li_die_areabox.getURY(),li_die_areabox.getWidth(),li_die_areabox.getHeight()));

    for (auto iter = inst_vec_.begin(); iter != inst_vec_.end(); ++iter) {
        auto instance = (*iter);
        auto insbox   = instance->getBox();

        // qDebug() << insbox.getLLX() << insbox.getLLY() << insbox.getURX() << insbox.getURY();

        auto li_box = transform.translate(insbox);
        auto insllx = li_box.getLLX();
        auto inslly = scene_h_ - li_box.getLLY();
        auto insurx = li_box.getURX();
        auto insury = scene_h_ - li_box.getURY();
        auto width  = li_box.getWidth();
        auto height = li_box.getHeight();
        // qDebug() << insllx << inslly << insurx << insury;
#if DRAW_MODE == 1
        if (width >= (4) || height >= (4)) {
            painter.drawRect(QRectF(
              (insllx),
              (insury),
              width,
              height));

            if (width >= (4) && height >= (4)) {
                li_pins->drawPins(*instance);
            }

            if (width >= (4) && height >= (4)) {

                switch (instance->getOrient()) {
                case open_edi::util::Orient::kN /* North, R0 in OpenAccess */:
                case open_edi::util::Orient::kFW /* Flipped West, MX90 in OpenAccess */:
                    painter.drawLine(QPoint(insllx, inslly - (height >> 2)), QPoint(insllx + (width >> 2), inslly));
                    break;
                case open_edi::util::Orient::kS /* South, R180 in OpenAcces */:
                case open_edi::util::Orient::kFE /* Flipped East, MY90 in OpenAccess */:
                    painter.drawLine(QPoint(insurx, insury + (height >> 2)), QPoint(insurx - (width >> 2), insury));
                    break;
                case open_edi::util::Orient::kW /* East, R270 in OpenAccess */:
                case open_edi::util::Orient::kFS /* Flipped South, MX in OpenAccess */:
                    painter.drawLine(QPoint(insllx, insury + (height >> 2)), QPoint(insllx + (width >> 2), insury));
                    break;
                case open_edi::util::Orient::kE /* West, R90 in OpenAccess */:
                case open_edi::util::Orient::kFN /* Flipped north, MY in OpenAccess */:
                    painter.drawLine(QPoint(insurx, inslly - (height >> 2)), QPoint(insurx - (width >> 2), inslly));
                    break;

                default:
                    printf("error: instance orient unknow\n");
                    break;
                }
            }

            if (width >= (16) && height >= (16)) {
                //draw name
                auto name = instance->getName().c_str();
                // painter.drawText(insllx + (width >> 2), inslly + (height >> 1), QString(name));
                painter.drawText(QRectF(
                                   (insllx),
                                   (insury),
                                   width,
                                   height),
                                 0,
                                 name);
            }
        }
#elif DRAW_MODE == 2
        if (width >= factor >> 2 || height >= factor >> 2) {
            painter_path_.addRect(QRectF(
              (insllx) / factor,
              (inslly) / factor,
              width / factor,
              height / factor));
            if (width > factor * 4 && height > factor * 4) {
                li_pins->drawPins(*instance);
            }
        }
#endif
    }

    item_->setMap(img_);
    item_->setItemSize(scene_w_, scene_h_);

    li_pins->getGraphicItem()->setMap(img_);
    li_pins->getGraphicItem()->setItemSize(scene_w_, scene_h_);
}

void LI_Instance::preFitDraw() {
    if (!visible_) {
        return;
    }
    refreshBoundSize();
    li_pins->refreshBoundSize();
    li_pins->fillImage();
#if DRAW_MODE == 1
    img_->fill(Qt::transparent);
    QPainter painter(img_);
    painter.setPen(pen_);
    painter.setWindow(0,
                      scene_h_,
                      scene_w_ + VIEW_SPACE,
                      -scene_h_ - VIEW_SPACE);

#elif DRAW_MODE == 2
    painter_path_ = QPainterPath();
#endif

    auto     tc               = open_edi::db::getTopCell();
    uint64_t num_components   = tc->getNumOfInsts();
    auto     components       = tc->getInstances();
    auto     component_vector = open_edi::db::Object::addr<open_edi::db::ArrayObject<open_edi::db::ObjectId>>(components);

    auto factor = *scale_factor_;

    // painter.setTransform(transform);
    // printf("COMPONENTS %d ;\n", num_components);
    Transform transform(1 / (float)factor, 1 / (float)factor);
    for (auto iter = component_vector->begin(); iter != component_vector->end(); ++iter) {
        auto instance = open_edi::db::Object::addr<open_edi::db::Inst>(*iter);
        auto insbox   = instance->getBox();
        auto li_box   = transform.translate(insbox);
        auto insllx   = li_box.getLLX();
        auto inslly   = li_box.getLLY();
        auto insurx   = li_box.getURX();
        auto insury   = li_box.getURY();
        auto width    = li_box.getWidth();
        auto height   = li_box.getHeight();
#if DRAW_MODE == 1
        if (width >= 2 || height >= 2) {
            painter.drawRect(QRectF(
              (insllx),
              (inslly),
              width,
              height));
            if (width > (4) && height > (4)) {
                li_pins->drawPins(*instance);

                if (width >= (4) && height >= (4)) {
                    switch (instance->getOrient()) {
                    case open_edi::util::Orient::kN /* North, R0 in OpenAccess */:
                    case open_edi::util::Orient::kFW /* Flipped West, MX90 in OpenAccess */:
                        painter.drawLine(QPoint(insllx, inslly + (height >> 2)), QPoint(insllx + (width >> 2), inslly));
                        break;
                    case open_edi::util::Orient::kS /* South, R180 in OpenAcces */:
                    case open_edi::util::Orient::kFE /* Flipped East, MY90 in OpenAccess */:
                        painter.drawLine(QPoint(insurx, insury - (height >> 2)), QPoint(insurx - (width >> 2), insury));
                        break;
                    case open_edi::util::Orient::kW /* East, R270 in OpenAccess */:
                    case open_edi::util::Orient::kFS /* Flipped South, MX in OpenAccess */:
                        painter.drawLine(QPoint(insllx, insury - (height >> 2)), QPoint(insllx + (width >> 2), insury));
                        break;
                    case open_edi::util::Orient::kE /* West, R90 in OpenAccess */:
                    case open_edi::util::Orient::kFN /* Flipped north, MY in OpenAccess */:
                        painter.drawLine(QPoint(insurx, inslly + (height >> 2)), QPoint(insurx - (width >> 2), inslly));
                        break;

                    default:
                        printf("error: instance orient unknow\n");
                        break;
                    }
                }
            }
#elif DRAW_MODE == 2
        if (width >= factor >> 2 || height >= factor >> 2) {
            painter_path_.addRect(QRectF(
              (insllx) / factor,
              (inslly) / factor,
              width / factor,
              height / factor));
            if (width > factor * 4 && height > factor * 4) {
                li_pins->drawPins(*instance);
            }
        }
#endif
        }

        item_->setMap(img_);
        item_->setItemSize(scene_w_, scene_h_);

        li_pins->getGraphicItem()->setMap(img_);
        li_pins->getGraphicItem()->setItemSize(scene_w_, scene_h_);
    }
} // namespace gui

// void LI_Instance::preDraw() {
//     if (!visible_) {
//         return;
//     }
//     refreshBoundSize();
//     li_pins->refreshBoundSize();
//     li_pins->fillImage();
// #if DRAW_MODE == 1
//     img_->fill(Qt::transparent);
//     QPainter painter(img_);
//     painter.setPen(pen_);
//     painter.setWindow(0,
//                       scene_h_,
//                       scene_w_ + VIEW_SPACE,
//                       -scene_h_ - VIEW_SPACE);
// #elif DRAW_MODE == 2
//     painter_path_ = QPainterPath();
// #endif

//     auto     tc               = open_edi::db::getTopCell();
//     uint64_t num_components   = tc->getNumOfInsts();
//     auto     components       = tc->getInstances();
//     auto     component_vector = open_edi::db::Object::addr<open_edi::db::ArrayObject<open_edi::db::ObjectId>>(components);

//     auto factor = *scale_factor_;
//     // printf("COMPONENTS %d ;\n", num_components);
//     for (auto iter = component_vector->begin(); iter != component_vector->end(); ++iter) {
//         auto instance = open_edi::db::Object::addr<open_edi::db::Inst>(*iter);
//         auto insbox   = instance->getBox();
//         auto insllx   = insbox.getLLX();
//         auto inslly   = insbox.getLLY();
//         auto insurx   = insbox.getURX();
//         auto insury   = insbox.getURY();
//         auto width    = insbox.getWidth();
//         auto height   = insbox.getHeight();
// #if DRAW_MODE == 1
//         if (width >= (factor >> 2) || height >= (factor >> 2)) {
//             insllx = (insllx) / factor;
//             inslly = (inslly) / factor;
//             insurx = (insurx) / factor;
//             insury = (insury) / factor;
//             width  = width / factor;
//             height = height / factor;

//             painter.drawRect(QRectF(
//               (insllx),
//               (inslly),
//               width,
//               height));
//             // if (width > (factor << 2) && height > (factor << 2)) {
//             li_pins->drawPins(*instance);

//             if (width >= (factor >> 4) && height >= (factor >> 4)) {
//                 switch (instance->getOrient()) {
//                 case open_edi::util::Orient::kN /* North, R0 in OpenAccess */:
//                 case open_edi::util::Orient::kFW /* Flipped West, MX90 in OpenAccess */:
//                     painter.drawLine(QPoint(insllx, inslly + (height >> 2)), QPoint(insllx + (width >> 2), inslly));
//                     break;
//                 case open_edi::util::Orient::kS /* South, R180 in OpenAcces */:
//                 case open_edi::util::Orient::kFE /* Flipped East, MY90 in OpenAccess */:
//                     painter.drawLine(QPoint(insurx, insury - (height >> 2)), QPoint(insurx - (width >> 2), insury));
//                     break;
//                 case open_edi::util::Orient::kW /* East, R270 in OpenAccess */:
//                 case open_edi::util::Orient::kFS /* Flipped South, MX in OpenAccess */:
//                     painter.drawLine(QPoint(insllx, insury - (height >> 2)), QPoint(insllx + (width >> 2), insury));
//                     break;
//                 case open_edi::util::Orient::kE /* West, R90 in OpenAccess */:
//                 case open_edi::util::Orient::kFN /* Flipped north, MY in OpenAccess */:
//                     painter.drawLine(QPoint(insurx, inslly + (height >> 2)), QPoint(insurx - (width >> 2), inslly));
//                     break;

//                 default:
//                     printf("error: instance orient unknow\n");
//                     break;
//                 }
//             }
//         }
// #elif DRAW_MODE == 2
//         if (width >= factor >> 2 || height >= factor >> 2) {
//             painter_path_.addRect(QRectF(
//               (insllx) / factor,
//               (inslly) / factor,
//               width / factor,
//               height / factor));
//             if (width > factor * 4 && height > factor * 4) {
//                 li_pins->drawPins(*instance);
//             }
//         }
// #endif
//     }

//     item_->setMap(img_);
//     item_->setItemSize(scene_w_, scene_h_);

//     li_pins->getGraphicItem()->setMap(img_);
//     li_pins->getGraphicItem()->setItemSize(scene_w_, scene_h_);
// }

bool LI_Instance::hasSubLI() {
    return true;
}

} // namespace gui
} // namespace open_edi