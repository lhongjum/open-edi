#ifndef LI_BASE_H
#define LI_BASE_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QPainterPath>
#include <QPen>
#include <QPixmap>
#include <QRunnable>

#include "li_manager.h"

#define NO_DRAW   0
#define IMG_MODE  1
#define PATH_MODE 2
#define DRAW_MODE IMG_MODE

namespace open_edi {
namespace gui {

class LI_Base : public QRunnable {
  public:
    // LI_Base(const LI_Base& other) = delete;
    // LI_Base& operator=(const LI_Base& rhs) = delete;

    LI_Base() { setAutoDelete(false); };
    explicit LI_Base(QString name) {
        name_ = name;
        setAutoDelete(false);
    };
    explicit LI_Base(int* scale_factor) {
        this->scale_factor_ = scale_factor;
        setAutoDelete(false);
    };
    ~LI_Base(){};

    virtual void           preDraw()    = 0;
    virtual void           preFitDraw() = 0;
    virtual void           refreshBoundSize();
    virtual void           setVisible(bool visible);
    virtual QGraphicsItem* getGraphicItem() = 0;
    virtual void           draw(QPainter* painter);
    virtual bool           hasSubLI() { return false; };
    virtual void           update();
    virtual bool           isMainLI() { return false; };
    virtual QString        getName();
    virtual void           setName(QString name) { name_ = name; };
    virtual bool           isVisible() { return visible_; };
    virtual QBrush         getBrush() { return brush_; };
    virtual void           setBrush(QBrush brush) { brush_ = brush; };
    virtual void           setBrushStyle(Qt::BrushStyle brush_style) { brush_.setStyle(brush_style); };
    virtual QPen           getPen() { return pen_; };
    virtual void           setPen(QPen pen) { pen_ = pen; };
    virtual void           run() override;
    virtual void           setOffsetX(int offset_x) { offset_x_ = offset_x; };
    virtual void           setOffsetY(int offset_y) { offset_y_ = offset_y; };
    virtual void           setOffset(int offset_x, int offset_y) { offset_x_ = offset_x, offset_y_ = offset_y; };
    virtual int            getOffsetX() { return offset_x_; };
    virtual int            getOffsetY() { return offset_y_; };

    enum ObjType {
        kDieArea,
        kInstance,
        kPin,
        kNet,
        kWire,
        kLayer
    };
    ObjType type;

  protected:
    int          scene_w_{1};
    int          scene_h_{1};
    QPixmap*     img_{nullptr};
    bool         visible_{false};
    int*         scale_factor_{nullptr};
    QPainterPath painter_path_;
    QString      name_{""};
    bool         main_li{false};
    LI_Manager*  li_mgr_{LI_MANAGER};
    QBrush       brush_;
    QPen         pen_;
    int          offset_x_{0};
    int          offset_y_{0};
};

} // namespace gui
} // namespace open_edi

#endif