#include "li_wire.h"

namespace open_edi {
namespace gui {
LI_Wire::LI_Wire(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_Wire;
    item_->setLiBase(this);
    pen_.setColor(QColor(0xff, 0, 0, 0xff));
    brush_   = QBrush(QColor(0xff, 0, 0, 0xff), Qt::Dense5Pattern);
    type     = kWire;
    name_    = "Wire";
    visible_ = true;
    li_mgr_->addLI(this);
}

LI_Wire::~LI_Wire() {
}

LGI_Wire* LI_Wire::getGraphicItem() {
    return item_;
}

void LI_Wire::draw(QPainter* painter) {
    painter->setPen(pen_);
    painter->setBrush(brush_);
    LI_Base::draw(painter);
}

void LI_Wire::drawWires(open_edi::db::Wire& wire) {
    if (!visible_) {
        return;
    }
    QPainter painter(img_);

    painter.setWindow(0,
                      scene_h_,
                      scene_w_ + VIEW_SPACE,
                      -scene_h_ - VIEW_SPACE);

    auto factor = *scale_factor_;
    auto llx    = (wire.getX()) / factor;
    auto lly    = (wire.getY()) / factor;

    auto box_width  = wire.getBBox().getWidth() / factor;
    auto box_height = wire.getBBox().getHeight() / factor;

    painter.setBrush(brush_);
    painter.fillRect(QRect(llx, lly, box_width, box_height), brush_);

    painter.setPen(pen_);
    painter.drawRect(llx, lly, box_width, box_height);
}

void LI_Wire::fillImage() {
    img_->fill(Qt::transparent);
}

void LI_Wire::preDraw() {
    if (!visible_) {
        return;
    }
    refreshBoundSize();

    img_->fill(Qt::transparent);

    item_->setMap(img_);
    item_->setItemSize(scene_w_, scene_h_);
}

} // namespace gui
} // namespace open_edi