#include <QPainter>

#include "li_die_area.h"
#include "../util/transform.h"

namespace open_edi {
namespace gui {

LI_DieArea::LI_DieArea(int* scale_factor) : LI_Base(scale_factor) {
    item_ = new LGI_DieArea;
    item_->setLiBase(this);
    pen_.setColor(QColor("#909090"));
    name_ = "Die area";
    type  = kDieArea;
    li_mgr_->addLI(this);
    setVisible(true);
    item_->setVisible(true);
}

LI_DieArea::~LI_DieArea() {
}

void LI_DieArea::preDraw() {
    refreshBoundSize();
#if DRAW_MODE == 1
    img_->fill(Qt::transparent);
    QPainter painter(img_);
    painter.setPen(pen_);

    painter.setWindow(0,
                      scene_h_,
                      scene_w_ + VIEW_SPACE,
                      -scene_h_ - VIEW_SPACE);
    auto factor = *scale_factor_;

    Transform transform(1 / (float)factor, 1 / (float)factor, getOffsetX(), getOffsetY());

    open_edi::db::Box box(0,0,die_area_w_,die_area_h_);

    auto area = transform.translate(box);

    // auto      w = die_area_w_ / factor;
    // auto      h = die_area_h_ / factor;
    // painter.drawRect(QRectF(area, 0, scene_w_, scene_h_));
    painter.drawRect(QRectF(area.getLLX(),area.getLLY(),area.getWidth(),area.getHeight()));

#elif DRAW_MODE == 2
    painter_path_ = QPainterPath();
    painter_path_.addRect(QRectF(0, 0, scene_w_, scene_h_));
#endif
    item_->setMap(img_);
    item_->setItemSize(scene_w_, scene_h_);
}

LGI_DieArea* LI_DieArea::getGraphicItem() {
    return item_;
}

bool LI_DieArea::isMainLI() {
    return true;
}

void LI_DieArea::draw(QPainter* painter) {
    painter->setPen(pen_);
    LI_Base::draw(painter);
}

} // namespace gui
} // namespace open_edi