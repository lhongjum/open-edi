#ifndef OPEN_EDI_LAYOUT_TRANSFORM_H_
#define OPEN_EDI_LAYOUT_TRANSFORM_H_

#include "db/core/cell.h"
#include "db/core/db.h"
#include "db/io/write_def.h"
#include "db/util/array.h"
#include "db/util/property_definition.h"
#include "util/util.h"

namespace open_edi {
namespace gui {

class LiBox {
  public:
    auto getLLX() { return llx; };
    auto getLLY() { return lly; };
    auto getURX() { return urx; };
    auto getURY() { return ury; };
    auto getWidth() { return w; };
    auto getHeight() { return h; };

    int llx;
    int lly;
    int urx;
    int ury;
    int w;
    int h;

    ~LiBox(){};
};

class Transform {

  public:
    Transform(){};
    Transform(float sx, float sy, float offset_x = 0, float offset_y = 0);
    ~Transform();

    template<typename T>
    auto translate(T&& box) {
        open_edi::db::Box li_box;
        li_box.setLLX((box.getLLX() + offset_x_) * scale_x_);
        li_box.setLLY((box.getLLY() + offset_y_) * scale_y_);
        li_box.setURX((box.getURX() + offset_x_) * scale_x_);
        li_box.setURY((box.getURY() + offset_y_) * scale_y_);
        return std::move(li_box);
    }
    // open_edi::db::Box&& translate(open_edi::db::Box&& box);
    int  translateX(int x);
    int  translateY(int y);
    void setOffsetX(float offset_x) { offset_x_ = offset_x; };
    void setOffsetY(float offset_y) { offset_y_ = offset_y; };
    void setScale(float sx, float sy) { scale_x_ = sx, scale_y_ = sy; };

  private:
    float scale_x_{1};
    float scale_y_{1};
    float offset_x_{0};
    float offset_y_{0};
};
} // namespace gui
} // namespace open_edi
#endif
