#include "transform.h"

namespace open_edi {
namespace gui {
Transform::Transform(float sx, float sy, float offset_x, float offset_y) {
    scale_x_  = sx;
    scale_y_  = sy;
    offset_x_ = offset_x;
    offset_y_ = offset_y;
}

Transform::~Transform() {
}

// open_edi::db::Box&& Transform::translate(open_edi::db::Box& box) {
//     open_edi::db::Box li_box;
//     li_box.setLLX((box.getLLX() + offset_x_) * scale_x_);
//     li_box.setLLY((box.getLLY() + offset_y_) * scale_y_);
//     li_box.setURX((box.getURX() + offset_x_) * scale_x_);
//     li_box.setURY((box.getURY() + offset_y_) * scale_y_);
//     return std::move(li_box);
// }

// open_edi::db::Box&& Transform::translate(open_edi::db::Box&& box) {
//     open_edi::db::Box li_box;
//     li_box.setLLX((box.getLLX() + offset_x_) * scale_x_);
//     li_box.setLLY((box.getLLY() + offset_y_) * scale_y_);
//     li_box.setURX((box.getURX() + offset_x_) * scale_x_);
//     li_box.setURY((box.getURY() + offset_y_) * scale_y_);
//     return std::move(li_box);
// }

int Transform::translateX(int x) {
    return x * scale_x_ + offset_x_;
}

int Transform::translateY(int y) {
    return y * scale_y_ + offset_y_;
}
} // namespace gui
} // namespace open_edi
