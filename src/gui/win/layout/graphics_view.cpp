#include <QGuiApplication>
#include <QSizePolicy>

#include "graphics_view.h"

namespace open_edi {
namespace gui {

GraphicsView::GraphicsView(QWidget* parent) : QGraphicsView(parent) {

    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setOptimizationFlags(QGraphicsView::DontSavePainterState);
    setMouseTracking(true);
    layout = LAYOUT_INSTANCE;
    layout->createLayoutItems();

    scene_ = new GraphicsScene;
    for (auto li : layout->li_manager->getLiList()) {
        scene_->addItem(li->getGraphicItem());
    }

    setScene(scene_);

#if USE_OPENGL == 1
    setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
#endif

    // setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

GraphicsView::~GraphicsView() {
}

void GraphicsView::__zoom(qreal value) {

    qreal s = value;

    auto last_point = layout->getAnchorPoint();
    auto x          = std::get<0>(last_point);
    auto y          = std::get<1>(last_point);

    auto point = calcOriginPointOffset(x,
                                       y,
                                       layout->getSelectedAreaW(),
                                       layout->getSelectedAreaH(),
                                       s);

    auto new_x = std::get<0>(point);
    auto new_y = std::get<1>(point);
    auto new_w = std::get<2>(point);
    auto new_h = std::get<3>(point);

    //set slected area
    layout->setSelectedArea(new_x, new_y, new_w, new_h);

    // open_edi::db::Box db_box(new_x, new_y, new_x + new_w, new_y + new_h);

    // auto box = layout->translateTofitScreen(db_box, width(), height());

    // layout->setSelectedArea(box);

    // layout->setSelectedArea(box.getLLX(), box.getLLY(), box.getWidth(), box.getHeight());

    //shall set selected area first
    layout->calculateScaleFactor(width(),
                                 height(),
                                 layout->getSelectedAreaW(),
                                 layout->getSelectedAreaH());

    //refresh all components
    refreshAllComponents();
}
/**
 * calculate new coordinate after zoom in/out according to factor
 * 
 * @param  {int} x            : anchor x in db unit
 * @param  {int} y            : anchor y in db unit
 * @param  {int} w            : selection w in db unit
 * @param  {int} h            : selection h in db unit
 * @param  {qreal} factor     : scale factor calculated by zoom action
 * @return {std::tuple<int,}  : new x, y , w, h
 */
std::tuple<int, int, int, int> GraphicsView::calcOriginPointOffset(int x, int y, int w, int h, qreal factor) {

    auto s = 1 / factor;

    // new width and height which needs to display
    auto p_w = w * s;
    auto p_h = h * s;

    //new x, y after offset
    auto p_x = (w - p_w) / 2 + x;
    auto p_y = (h - p_h) / 2 + y;

    return std::tuple<int, int, int, int>(p_x, p_y, p_w, p_h);
}

open_edi::db::Box GraphicsView::adjustPointsToDbBox(QPoint p_start, QPoint p_end) {
    auto point_start = mapToScene(p_start);
    auto x1          = (point_start.rx() + scene_->width() / 2) * layout->getScaleFactor();
    auto y1          = -(point_start.ry() - scene_->height() / 2) * layout->getScaleFactor();

    auto point_end = mapToScene(p_end);
    auto x2        = (point_end.rx() + scene_->width() / 2) * layout->getScaleFactor();
    auto y2        = -(point_end.ry() - scene_->height() / 2) * layout->getScaleFactor();

    auto llx = x1 > x2 ? x2 : x1;
    auto lly = y1 > y2 ? y2 : y1;
    auto urx = x1 > x2 ? x1 : x2;
    auto ury = y1 > y2 ? y1 : y2;

    return open_edi::db::Box(llx, lly, urx, ury);
}

void GraphicsView::slotZoomIn(bool) {
    __zoom(0.8);
}

void GraphicsView::slotZoomOut(bool) {
    __zoom(1.2);
}
/**
 * GraphicsView 
 * refresh all components.
 * 
 */
void GraphicsView::refreshAllComponents() {

    //calculate scale factor according logic size

    // auto s = layout->calculateScaleFactor(width(),
    //                                       height(),
    //                                       layout->getSelectedAreaW(),
    //                                       layout->getSelectedAreaH());

    auto w = layout->getPhysicalW();
    auto h = layout->getPhysicalH();

    //set scene and item size
    scene_->setSceneRect((-w) / 2,
                         (-h) / 2,
                         w,
                         h);

    layout->refreshAllComponents();

    viewport()->update();
}

void GraphicsView::refreshFitDraw() {

    auto fit_width  = (width() / 10) * 7;
    auto fit_height = (height() / 10) * 7;

    //shall set selected area first
    layout->calculateScaleFactor(fit_width,
                                 fit_height,
                                 layout->getDieAreaW(),
                                 layout->getDieAreaH());

    //get width and height for display after calculate scale factor
    auto w = layout->getPhysicalW();
    auto h = layout->getPhysicalH();

    scene_->setSceneRect((-w) / 2,
                         (-h) / 2,
                         w,
                         h);

    layout->viewFitArea();

    viewport()->update();
}

void GraphicsView::initViewSize() {

    layout->initDieArea();
}

void GraphicsView::slotReadLayer() {
    // layout->getAllLayerNames();
}

void GraphicsView::setPinsVisible(bool visible) {
    layout->li_pins->setVisible(visible);
}

void GraphicsView::wheelEvent(QWheelEvent* event) {
    qreal factor = qPow(1.1, event->delta() / 240.0);

    __zoom(factor);
}

void GraphicsView::mouseMoveEvent(QMouseEvent* event) {
    auto point = mapToScene(event->pos());
    auto x     = (point.rx() + scene_->width() / 2) * layout->getScaleFactor();
    auto y     = -(point.ry() - scene_->height() / 2) * layout->getScaleFactor();
    emit sendPos(x, y);

    if (area_selected_) {
        QPoint point_moving_ = event->pos();
        scene_->setSelectionRect(mapToScene(point_start_), mapToScene(point_moving_));
    }
}

void GraphicsView::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::RightButton) {
        point_start_   = event->pos();
        area_selected_ = true;
    }
}

void GraphicsView::mouseReleaseEvent(QMouseEvent* event) {

    if (event->button() == Qt::RightButton) {

        area_selected_ = false;

        //reset display area
        scene_->setSelectionRect(mapToScene(event->pos()), mapToScene(event->pos()));

        auto db_box = adjustPointsToDbBox(point_start_, event->pos());

        auto box = layout->translateTofitScreen(db_box, width(), height());

        auto anchor_point = layout->getAnchorPoint();
        auto llx          = std::get<0>(anchor_point);
        auto lly          = std::get<1>(anchor_point);

        Transform transform(1,
                            1,
                            llx /*current lly point in db unit*/,
                            lly /*current lly point in db unit*/);
        auto      area = transform.translate(box);

        layout->setSelectedArea(area);

        refreshAllComponents();
    }
}

GraphicsView* GraphicsView::inst_ = nullptr;
} // namespace gui
} // namespace open_edi