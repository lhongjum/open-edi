#include "layout.h"
#include "./items/li_die_area.h"
namespace open_edi {
namespace gui {
Layout::~Layout() {
}

QList<QString>& Layout::getAllLayerNames() {
    auto tc         = open_edi::db::getTopCell();
    auto tech_lib   = tc->getTechLib();
    auto num_layers = tech_lib->getNumLayers();
    // printf("number of laryers: %d\n", num_layers);
    for (int i = 0; i < num_layers; i++) {
        auto layer = tech_lib->getLayer(i);
        li_manager->addLayer(layer);
    }

    return li_manager->getLayerNameList();
}

void Layout::refreshAllComponents() {
    QTime time;
    time.start();

    li_manager->preDrawAllItems();

    auto time_elapsed = time.elapsed();
    printf("total elapsed time %d (ms)\n", time_elapsed);
}

void Layout::refreshFitDraw() {
    QTime time;
    time.start();

    li_manager->preFitDraw();

    auto time_elapsed = time.elapsed();
    printf("total elapsed time %d (ms)\n", time_elapsed);
}

void Layout::createLayoutItems() {
    LI_Base* li = new LI_DieArea(&scale_factor_);
    li          = new LI_Instance(&scale_factor_);
    li          = new LI_Net(&scale_factor_);
}

/**
 * Layout 
 * calculate scale factor according screen size
 * @param  {int} screen_w  : screen width
 * @param  {int} screen_h  : screen height
 * @param  {int} db_area_w : selected db area width
 * @param  {int} db_area_h : selected db area height
 * @return {int}           : scale factor
 */
int Layout::calculateScaleFactor(int screen_w, int screen_h, int db_area_w, int db_area_h) {

    //calculate scale factor according current sceen size
    auto factor_x = (db_area_w) / (screen_w);
    auto factor_y = (db_area_h) / (screen_h);

    //keep same ratio for both x and y
    scale_factor_ = factor_x > factor_y ?
                      qCeil(factor_x) :
                      qCeil(factor_y);

    //display size , the max is screen size
    display_screen_w_size_ = qCeil((db_area_w) / scale_factor_);
    display_screen_h_size_ = qCeil((db_area_h) / scale_factor_);

    return scale_factor_;
}

std::tuple<int, int> Layout::initDieArea() {
    auto tc = open_edi::db::getTopCell();

    auto  poly = tc->getFloorplan()->getDieAreaPolygon();
    float factor_x, factor_y;

    auto numPoints = poly->getNumPoints();

    die_area_w_ = die_area_h_ = 0;

    //find die area
    for (int i = 0; i < numPoints; i++) {
        auto point   = poly->getPoint(i);
        auto point_x = point.getX();
        auto point_y = point.getY();

        if (point_x && point_y) {
            selected_area_w_ = die_area_w_ = point_x;
            selected_area_h_ = die_area_h_ = point_y;

            auto li_diearea = (LI_DieArea*)(li_manager->getLiByName("Die area"));
            li_diearea->setDieAreaSize(die_area_w_, die_area_h_);
            // auto li_inst = (LI_Instance*)(li_manager->getLiByName("Instance"));
            // li_inst->setDieAreaSize(die_area_w_, die_area_h_);
        }
    }

    return std::tuple<int, int>(die_area_w_, die_area_h_);
}

void Layout::setSelectedArea(QPointF p1, QPointF p2) {
    setSelectedAreaW(p2.rx() - p1.rx());
    setSelectedAreaH(p2.ry() - p1.ry());
    li_manager->setSelectedArea(p1, p2);
}

void Layout::setSelectedArea(int x, int y, int w, int h) {
    setSelectedAreaW(w);
    setSelectedAreaH(h);
    li_manager->setSelectedArea(x, y, w, h);
}

void Layout::setSelectedArea(open_edi::db::Box& area) {

    setSelectedAreaW(area.getWidth());
    setSelectedAreaH(area.getHeight());

    li_manager->setSelectedArea(area);
}

open_edi::db::Box Layout::translateTofitScreen(open_edi::db::Box& box,
                                               int                screen_w,
                                               int                screen_h) {
    //calculate scale factor according current sceen size
    auto factor_w = (box.getWidth()) / (screen_w);
    auto factor_h = (box.getHeight()) / (screen_h);

    //get max factor as scale factor for keeping same ratio for both x and y
    auto factor = factor_w > factor_h ?
                    qCeil(factor_w) :
                    qCeil(factor_h);

    scale_factor_ = factor;

    //get new screen
    auto ns_w = box.getWidth() / factor;
    auto ns_h = box.getHeight() / factor;

    //get adjsust w and h
    auto box_w = screen_w * factor;
    auto box_h = screen_h * factor;

    //display size , the max is screen size
    display_screen_w_size_ = screen_w;
    display_screen_h_size_ = screen_h;

    int llx = box.getLLX();
    int lly = box.getLLY();

    if (screen_w - ns_w > 10) {
        auto adj = factor * (screen_w - ns_w);
        llx      = llx - adj / 2;
    }

    if (screen_h - ns_h > 10) {
        auto adj = factor * (screen_h - ns_h);
        lly      = lly - adj / 2;
    }

    open_edi::db::Box new_box(llx, lly, llx + box_w, lly + box_h);

    return new_box;
}

void Layout::selectWholeArea() {
    setSelectedAreaW(die_area_w_);
    setSelectedAreaH(die_area_h_);
    li_manager->setSelectedArea(QPointF(0, 0), QPointF(die_area_w_, die_area_h_));
}

void Layout::viewFitArea() {
    li_manager->resetOriginPoint();
    selectWholeArea();
    refreshAllComponents();
}

void Layout::setLayerVisible(QString name, bool v) {
}

void Layout::setLayerColor(QString name, QColor color) {
}

void Layout::setLayerSelectable(QString name, bool v) {
}

void Layout::setComponentVisible(QString name, bool v) {

    if (li_manager->getLiByName(name)) {

        li_manager->setLiVisibleByName(name, v);
        li_manager->getLiByName(name)->preDraw();
        li_manager->getLiByName(name)->update();
    }
}

void Layout::setComponentSelectable(QString name, bool v) {
}

void Layout::setComponentBrushStyle(QString name, Qt::BrushStyle brush_style) {

    if (li_manager->getLiByName(name)) {

        li_manager->setLiBrushStyleByName(name, brush_style);
        li_manager->getLiByName(name)->preDraw();
        li_manager->getLiByName(name)->update();
    }
}

Layout* Layout::inst_ = nullptr;
} // namespace gui
} // namespace open_edi