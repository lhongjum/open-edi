#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAction>
#include <QGraphicsView>
#include <QMainWindow>
#include <QMap>
#include <QMdiArea>
#include <QApplication>
#include <QDockWidget>
#include <QScreen>

#include "./common/docks_manager.h"
#include "./layout/graphics_view.h"
#include "tcl.h"

namespace open_edi {
namespace gui {

#define MAIN_WINDOW MainWindow::getInstance()

class ActionHandler;
class MDIWindow;
class RibbonMenuBar;
class ActionGroupManager;
class StatusBar;

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    static MainWindow* getInstance() {
        if (!instance_) {
            instance_ = new MainWindow;
        }
        return instance_;
    }
    void setTclInterp(Tcl_Interp* intrep);

  protected:
    bool eventFilter(QObject* obj, QEvent* e) override;
    void closeEvent(QCloseEvent* e) override;

  private:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow() override;

    void init();
    void addActions();
    void createCentralWindow();
    void loadTheme(const QString& file);

  signals:
    void windowChanged(bool);

  private slots:
    void slotInitial();
    void slotNavItemChanged(bool open);

  private:
    static MainWindow*      instance_;
    QMdiArea*               mdi_area_{nullptr};
    MDIWindow*              current_subwindow_{nullptr};
    RibbonMenuBar*          ribbon_{nullptr};
    GraphicsView*           graphics_view_{nullptr};
    StatusBar*              status_bar_{nullptr};

    Tcl_Interp*             interp_{nullptr};
    ActionHandler*          action_handler_{nullptr};
    ActionGroupManager*     action_manager_{nullptr};
    DocksManager*           docks{nullptr};

    QMap<QString, QAction*> action_map_;
    QList<MDIWindow*>       window_list_{nullptr};
    QDockWidget*            navigation_panel_{nullptr};

    QScreen*                screen_;
};

} // namespace gui
} // namespace open_edi
#endif // MAINWINDOW_H
