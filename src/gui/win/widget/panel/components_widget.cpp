#include "components_widget.h"

namespace open_edi {
namespace gui {

ComponentsWidget::ComponentsWidget(QWidget* parent) : TreeWidgetBase(parent) {

    QList<QBrush> style_array{
      QBrush("Black", Qt::SolidPattern),
      QBrush("Black", Qt::Dense2Pattern),
      QBrush("Black", Qt::Dense4Pattern),
      QBrush("Black", Qt::Dense6Pattern),
      QBrush("Black", Qt::HorPattern),
      QBrush("Black", Qt::VerPattern),
      QBrush("Black", Qt::CrossPattern),
      QBrush("Black", Qt::BDiagPattern),
      QBrush("Black", Qt::FDiagPattern),
      QBrush("Black", Qt::DiagCrossPattern),
    };

    pal = new Palette(std::move(style_array));

    pal->setName("Pattern:  ");

    auto top_item = __createTopItem(this, "Cell");

    __createSubItems(top_item,
                     {{ItemAttributes{"Pin Shape", style_array[1]}},
                      {ItemAttributes{"OBS", style_array[2]}}});

    top_item = __createTopItem(this, "Instance");

    __createSubItems(__createSubItem(top_item, "Cell"),
                     {{ItemAttributes{"Std Cell", style_array[1]}},
                      {ItemAttributes{"Physical Cell", style_array[2]}},
                      {ItemAttributes{"Macro", style_array[3]}},
                      {ItemAttributes{"IO Cell", style_array[4]}}});

    __createSubItems(__createSubItem(top_item, "Physical Status"),
                     {{ItemAttributes{"Placed", style_array[1]}},
                      {ItemAttributes{"Fixed", style_array[2]}},
                      {ItemAttributes{"Cover", style_array[3]}},
                      {ItemAttributes{"Unplaced", style_array[4]}}});

    __createSubItems(__createSubItem(top_item, "Power"),
                     {{ItemAttributes{"Power Switch", style_array[1]}},
                      {ItemAttributes{"Level Shifter", style_array[2]}},
                      {ItemAttributes{"Isolation", style_array[3]}}});

    top_item = __createTopItem(this, "Net");

    __createSubItems(top_item,
                     {
                       {ItemAttributes{"Signal", style_array[1]}},
                       {ItemAttributes{"Tie Hi / Lo", style_array[2]}},
                       {ItemAttributes{"Clock", style_array[3]}},
                       {ItemAttributes{"Power", style_array[4]}},
                       {ItemAttributes{"Ground", style_array[5]}},
                     });

    top_item = __createTopItem(this, "Track");

    __createSubItems(top_item,
                     {{ItemAttributes{"Prefer", style_array[1]}},
                      {ItemAttributes{"Non-Prefer", style_array[2]}}});

    top_item = __createTopItem(this, "Row");

    __createSubItems(top_item,
                     {{ItemAttributes{"Std-Row", style_array[1]}},
                      {ItemAttributes{"Site", style_array[2]}}});

    top_item = __createTopItem(this, "Blockage");

    __createSubItems(__createSubItem(top_item, "Placement"),
                     {
                       {ItemAttributes{"Soft Blkg", style_array[1]}},
                       {ItemAttributes{"Hard Blkg", style_array[2]}},
                       {ItemAttributes{"Partial Blkg", style_array[3]}},
                     });

    __createSubItems(__createSubItem(top_item, "Routing"),
                     {
                       {ItemAttributes{"Routing Guide", style_array[1]}},
                       {ItemAttributes{"Routing Halo", style_array[2]}},
                       {ItemAttributes{"Routing Blkg", style_array[3]}},
                     });

    top_item = __createTopItem(this, "Map");

    __createSubItems(top_item,
                     {{ItemAttributes{"Cell Density Map", style_array[1]}},
                      {ItemAttributes{"Congestion Map", style_array[2]}},
                      {ItemAttributes{"Pin Density Map", style_array[3]}}});

    top_item = __createTopItem(this, "Grid");
    __createSubItems(top_item,
                     {{ItemAttributes{"Manufacture", style_array[1]}},
                      {ItemAttributes{"User Defined", style_array[2]}},
                      {ItemAttributes{"Gcell", style_array[3]}}});

    connect(this, &ComponentsWidget::itemClicked, this, &ComponentsWidget::slotItemClicked);
    connect(pal, &Palette::signalBtnOKClicked, this, &TreeWidgetBase::slotItemColorChange);
}

ComponentsWidget::~ComponentsWidget() {
}

void ComponentsWidget::addComponentListener(ComponentListener* listener) {
    component_listener_list_.append(listener);
}

void ComponentsWidget::slotItemClicked(QTreeWidgetItem* item, int column) {
    switch (column) {
    case kName:
        break;
    case kColor: {
        auto name = item->text(kName).toLocal8Bit().constData();
        pal->getItemName(name);
        pal->exec();
    } break;
    case kVisible:
        for (auto list : component_listener_list_) {
            list->setComponentVisible(item->text(kName), item->checkState(column));
        }
        break;
    case kSelectable:
        break;
    default:
        break;
    }
}

void ComponentsWidget::slotItemColorChange(const char* item_name, QBrush brush) {
    QString name(item_name);
    if (lable_map_.find(name) != lable_map_.end()) {
        lable_map_[name]->setBrush(brush);
        lable_map_[name]->update();
        for (auto list : component_listener_list_) {
            list->setComponentBrushStyle(item_name, brush.style());
        }
    }
}

} // namespace gui
} // namespace open_edi