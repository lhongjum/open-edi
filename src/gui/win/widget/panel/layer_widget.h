#ifndef LAYER_WIDGET_H
#define LAYER_WIDGET_H

#include <QAbstractTableModel>
#include <QHeaderView>
#include <QList>
#include <QListWidget>
#include <QMap>
#include <QPushButton>
#include <QStackedWidget>
#include <QTableView>
#include <QTreeWidget>
#include <QWidget>
#include <map>

#include "layer_listener.h"
#include "tree_widget_base.h"

namespace open_edi {
namespace gui {

class LayerWidget : public TreeWidgetBase {
    Q_OBJECT
  public:
    explicit LayerWidget(QWidget* parent = nullptr);
    ~LayerWidget();

    void addLayerListener(LayerListener* listener);
    void refreshTree(QList<QString> name_list);

  private:
    QList<LayerListener*> layer_listener_list_;
    QTreeWidgetItem*      layer_top_item;
  private slots:
    virtual void slotItemClicked(QTreeWidgetItem* item, int column) override;
    virtual void slotItemColorChange(const char* item_name, QBrush brush) override;
};

} // namespace gui
} // namespace open_edi

#endif // LAYER_WIDGET_H
