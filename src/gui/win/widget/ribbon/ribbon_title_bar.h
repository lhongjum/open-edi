#ifndef RIBBON_TITLE_BAR_H
#define RIBBON_TITLE_BAR_H

#include <QLabel>
#include <QWidget>
#include <QToolButton>

namespace open_edi {
namespace gui {

class QuickAccessBar;
class RibbonTitleBar : public QWidget {
    Q_OBJECT

  public:
    explicit RibbonTitleBar(QWidget* parent = nullptr);
    ~RibbonTitleBar() override;
    void init();

    void     setWindowTitle(const QString& title);
    void     setTitleBarIcon(const QString& icon);
    void     addQuickAction(QAction* action);
    QAction* addQuickAction(const QIcon& icon, const QString& text);

protected:
    void    mouseMoveEvent(QMouseEvent* e) override;
    void    mousePressEvent(QMouseEvent* e) override;
    void    mouseReleaseEvent(QMouseEvent* e) override;
    void    mouseDoubleClickEvent(QMouseEvent* e) override;
    bool    eventFilter(QObject* obj, QEvent* e) override;

    void    updateMaximize();
  signals:

  public slots:
    void    onClicked();

  private:
    QLabel*         title_;
    QLabel*         icon_;
    QuickAccessBar* left_accessbar_;
    QToolButton*    skin_;
    QToolButton*    minimize_bt_;
    QToolButton*    maximize_bt_;
    QToolButton*    close_bt_;
    QPoint          start_;
    QPoint          end_;
    bool            left_button_pressed;
};

} // namespace gui
} // namespace open_edi
#endif // RIBBON_TITLE_BAR_H
