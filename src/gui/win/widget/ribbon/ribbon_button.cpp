#include "ribbon_button.h"
#include "draw_helper.h"
#include <QAction>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QStyleOptionToolButton>
#include <QStyleOptionFocusRect>
#include <QStylePainter>

#include <QGuiApplication>
#include <QScreen>

namespace open_edi {
namespace gui {

#define LARGE_SIZE 75
#define SMALL_SIZE 25
#define MINI_SIZE 25

RibbonButton::RibbonButton(QWidget* parent) : QToolButton(parent) {
    setObjectName("RibbonToolButton");
    setToolButtonStyle(Qt::ToolButtonIconOnly);
//    setAutoRaise(true);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    setMouseTracking(true);
    size_ = kMini;
}

RibbonButton::~RibbonButton() {
}

void RibbonButton::setButtonSize(const ButtonSize &size){
    size_ = size;

    QScreen* screen = QGuiApplication::primaryScreen();
    int height = screen->availableGeometry().height()/27;

    if(kLarge == size_){
        setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        setMinimumHeight(height);
    }else if(kSmall == size_) {
        setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        setMinimumHeight(height/3 -1);
    }else {
        setToolButtonStyle(Qt::ToolButtonIconOnly);
        setMinimumHeight(height/3 -1);
    }
}

bool RibbonButton::event(QEvent* e)
{
    switch(e->type())
    {
    case QEvent::WindowDeactivate:
    case QEvent::ActionChanged:
    case QEvent::ActionRemoved:
    case QEvent::ActionAdded:
        mouse_on_control_ = false;
        break;
    default:
        break;
    }

    return QToolButton::event(e);
}

void RibbonButton::paintEvent(QPaintEvent* e){
    switch (size_) {
    case kLarge:
        paintLargeButton(e);
        return;
    case kSmall:
        paintSmallButton(e);
        return;
    case kMini:
        paintMiniButton(e);
        return;
    }
}

void RibbonButton::mousePressEvent(QMouseEvent* e) {
    if (e->button() == Qt::LeftButton && popupMode() == MenuButtonPopup) {
        if(size_ == kLarge){
            QRect this_rect = rect();
            QRect rect = this_rect.adjusted(0,height()/2, 0, 0);
            if(rect.isValid() && rect.contains(e->pos())){
                menu_button_pressed_ = true;
                showMenu();
                return;
            }
        }else{
            if(icon_rect_.isValid() && icon_rect_.contains(e->pos())){
                    menu_button_pressed_=true;
                    showMenu();
                    return;
            }
        }
    }

    menu_button_pressed_ = false;
    QToolButton::mousePressEvent(e);
}

void RibbonButton::mouseMoveEvent(QMouseEvent* e){
    bool mouse_on_control = false;
    if(size_ == kLarge){
        mouse_on_control = (e->pos().y() > height()/2);
    }else {
        mouse_on_control = !icon_rect_.contains(e->pos());
    }

    if(mouse_on_control_ != mouse_on_control){
        mouse_on_control_ = mouse_on_control;
        update();
    }
    QToolButton::mouseMoveEvent(e);
}

void RibbonButton::mouseReleaseEvent(QMouseEvent* e){
    QToolButton::mouseReleaseEvent(e);
    menu_button_pressed_ = false;
}

void RibbonButton::focusOutEvent(QFocusEvent *e)
{
    QToolButton::focusOutEvent(e);
    menu_button_pressed_ = false;
}

void RibbonButton::leaveEvent(QEvent *e)
{
    menu_button_pressed_ = false;
    QToolButton::leaveEvent(e);
}

bool RibbonButton::hitButton(const QPoint &pos) const{
    if(QAbstractButton::hitButton(pos))
        return !menu_button_pressed_;
    return false;
}

void RibbonButton::resizeEvent(QResizeEvent* e){
    QStyleOptionToolButton opt;
    initStyleOption(&opt);

    icon_rect_ = QRect(0,0,height(),height());
    QSize iconSize = DrawHelper::iconActualSize(opt.icon,&opt,icon_rect_.size().boundedTo(opt.iconSize));
    if(iconSize.width() > icon_rect_.width())
    {
        icon_rect_.setWidth(iconSize.width());
    }
    QToolButton::resizeEvent(e);
}

void RibbonButton::paintLargeButton(QPaintEvent* e){

    QStylePainter p(this);
    QStyleOptionToolButton opt;
    initStyleOption(&opt);
    if((opt.features & QStyleOptionToolButton::MenuButtonPopup) || (opt.features & QStyleOptionToolButton::HasMenu)){
        if(!rect().contains(mapFromGlobal(QCursor::pos()))){
            opt.state &= ~QStyle::State_MouseOver;
        }
    }
    bool auto_raise = opt.state & QStyle::State_AutoRaise;
    QStyle::State bflags = opt.state & ~QStyle::State_Sunken;
    if (auto_raise)
    {
        if (!(bflags & QStyle::State_MouseOver) || !(bflags & QStyle::State_Enabled)) {
            bflags &= ~QStyle::State_Raised;
        }
    }
    if (opt.state & QStyle::State_Sunken)
    {
        if (opt.activeSubControls & QStyle::SC_ToolButton)
        {
            bflags |= QStyle::State_Sunken;
        }
        else if (opt.activeSubControls & QStyle::SC_ToolButtonMenu)
        {
            bflags |= QStyle::State_MouseOver;
        }
    }

    QStyleOption tool(0);
    tool.palette = opt.palette;

    if ((opt.subControls & QStyle::SC_ToolButton)&&(opt.features & QStyleOptionToolButton::MenuButtonPopup))
    {

        tool.rect = opt.rect;
        tool.state &= ~QStyle::State_Sunken;
        tool.state = bflags;
        if(opt.activeSubControls &= QStyle::SC_ToolButtonMenu)
        {
            style()->drawPrimitive(QStyle::PE_PanelButtonTool, &tool, &p, this);
            p.save();
            p.setPen(border_color_);
            p.setBrush(Qt::NoBrush);
            p.drawRect(opt.rect.adjusted(1,1,-1,-1));
            p.drawLine(0, opt.rect.height()/2,opt.rect.width(),opt.rect.height()/2);
            p.restore();
        }
        else
        {
            style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
        }
    }
    else if((opt.subControls & QStyle::SC_ToolButton)&& (opt.features & QStyleOptionToolButton::HasMenu))
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
    }
    else if(opt.subControls & QStyle::SC_ToolButton)
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        if(opt.state & QStyle::State_Sunken)
        {
            tool.state &= ~QStyle::State_MouseOver;
        }
        style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
    }

    if(((opt.state & QStyle::State_MouseOver) && (opt.features & QStyleOptionToolButton::MenuButtonPopup))
            ||((isChecked()) && (opt.features & QStyleOptionToolButton::MenuButtonPopup)) )
    {
        p.save();
        p.setPen(border_color_);
        p.setBrush(Qt::NoBrush);
        p.drawRect(opt.rect.adjusted(1,1,-1,-1));
        p.drawLine(0, opt.rect.height()/2,opt.rect.width(), opt.rect.height()/2);
        p.restore();
    }

    if(opt.features & QStyleOptionToolButton::HasMenu){
        tool.rect = opt.rect.adjusted(0,68,0,0);
        style()->drawPrimitive(QStyle::PE_IndicatorArrowDown, &tool, &p, this);
    }

    if (opt.state & QStyle::State_HasFocus)
    {
        QStyleOptionFocusRect fr;
        fr.QStyleOption::operator=(opt);
        fr.rect.adjust(3, 3, -3, -3);
        if (opt.features & QStyleOptionToolButton::MenuButtonPopup)
            fr.rect.adjust(0, 0, -style()->pixelMetric(QStyle::PM_MenuButtonIndicator,&opt, this), 0);
        style()->drawPrimitive(QStyle::PE_FrameFocusRect, &fr, &p, this);
    }

    drawIconAndLabel(p,opt);
}

void RibbonButton::paintSmallButton(QPaintEvent* e){

    QStylePainter p(this);
    QStyleOptionToolButton opt;
    initStyleOption(&opt);
    if(opt.features & QStyleOptionToolButton::MenuButtonPopup||opt.features & QStyleOptionToolButton::HasMenu)
    {
        if(!this->rect().contains(this->mapFromGlobal(QCursor::pos())))
        {
            opt.state &= ~QStyle::State_MouseOver;
        }
    }

    bool autoRaise = opt.state & QStyle::State_AutoRaise;
    QStyle::State bflags = opt.state & ~QStyle::State_Sunken;
    QStyle::State mflags = bflags;
    if (autoRaise)
    {
        if (!(bflags & QStyle::State_MouseOver) || !(bflags & QStyle::State_Enabled)) {
            bflags &= ~QStyle::State_Raised;
        }
    }
    if (opt.state & QStyle::State_Sunken)
    {
        if (opt.activeSubControls & QStyle::SC_ToolButton)
        {
            bflags |= QStyle::State_Sunken;
            mflags |= QStyle::State_MouseOver | QStyle::State_Sunken;
        }
        else if (opt.activeSubControls & QStyle::SC_ToolButtonMenu)
        {
            mflags |= QStyle::State_Sunken;
            bflags |= QStyle::State_MouseOver;
        }
    }

    QStyleOption tool(0);
    tool.palette = opt.palette;

    if ((opt.subControls & QStyle::SC_ToolButton)&&(opt.features & QStyleOptionToolButton::MenuButtonPopup))
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        if(opt.activeSubControls &= QStyle::SC_ToolButtonMenu)
        {
            style()->drawPrimitive(QStyle::PE_PanelButtonTool, &tool, &p, this);
            p.save();
            p.setPen(border_color_);
            p.setBrush(Qt::NoBrush);
            p.drawRect(opt.rect.adjusted(0,0,0,-1));
            p.drawRect(icon_rect_);
            p.restore();
        }
        else
        {
            style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
        }
    }
    else if((opt.subControls & QStyle::SC_ToolButton) && (opt.features & QStyleOptionToolButton::HasMenu))
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
    }
    else if(opt.subControls & QStyle::SC_ToolButton)
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        if(opt.state & QStyle::State_Sunken){
            tool.state &= ~QStyle::State_MouseOver;
        }
        style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
    }

    if(((opt.state & QStyle::State_MouseOver) && (opt.features & QStyleOptionToolButton::MenuButtonPopup))
            ||((isChecked()) && (opt.features & QStyleOptionToolButton::MenuButtonPopup)) )
    {
        p.save();
        p.setPen(border_color_);
        p.setBrush(Qt::NoBrush);
        p.drawRect(opt.rect.adjusted(0,0,-1,-1));
        p.drawRect(icon_rect_);
        p.restore();
    }
    drawIconAndLabel(p,opt);
}

void RibbonButton::paintMiniButton(QPaintEvent* e){

    QStylePainter p(this);
    QStyleOptionToolButton opt;
    initStyleOption(&opt);
    if(opt.features & QStyleOptionToolButton::MenuButtonPopup||opt.features & QStyleOptionToolButton::HasMenu)
    {
        if(!this->rect().contains(this->mapFromGlobal(QCursor::pos())))
        {
            opt.state &= ~QStyle::State_MouseOver;
        }
    }

    bool autoRaise = opt.state & QStyle::State_AutoRaise;
    QStyle::State bflags = opt.state & ~QStyle::State_Sunken;
    QStyle::State mflags = bflags;
    if (autoRaise)
    {
        if (!(bflags & QStyle::State_MouseOver) || !(bflags & QStyle::State_Enabled)) {
            bflags &= ~QStyle::State_Raised;
        }
    }
    if (opt.state & QStyle::State_Sunken)
    {
        if (opt.activeSubControls & QStyle::SC_ToolButton)
        {
            bflags |= QStyle::State_Sunken;
            mflags |= QStyle::State_MouseOver | QStyle::State_Sunken;
        }
        else if (opt.activeSubControls & QStyle::SC_ToolButtonMenu)
        {
            mflags |= QStyle::State_Sunken;
            bflags |= QStyle::State_MouseOver;
        }
    }

    QStyleOption tool(0);
    tool.palette = opt.palette;

    if ((opt.subControls & QStyle::SC_ToolButton)&&(opt.features & QStyleOptionToolButton::MenuButtonPopup))
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        if(opt.activeSubControls &= QStyle::SC_ToolButtonMenu)
        {
            style()->drawPrimitive(QStyle::PE_PanelButtonTool, &tool, &p, this);
            p.save();
            p.setPen(border_color_);
            p.setBrush(Qt::NoBrush);
            p.drawRect(opt.rect.adjusted(0,0,0,-1));
            p.drawRect(icon_rect_);
            p.restore();
        }
        else
        {
            if(mouse_on_control_){
                tool.rect.adjust(icon_rect_.width(),0,0,0);
            }
            else{
                tool.rect = icon_rect_;
            }
            style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
        }
    }
    else if((opt.subControls & QStyle::SC_ToolButton) && (opt.features & QStyleOptionToolButton::HasMenu))
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
    }
    else if(opt.subControls & QStyle::SC_ToolButton)
    {
        tool.rect = opt.rect;
        tool.state = bflags;
        if(opt.state & QStyle::State_Sunken){
            tool.state &= ~QStyle::State_MouseOver;
        }
        style()->drawPrimitive(QStyle::PE_PanelButtonBevel, &tool, &p, this);
    }

    if(((opt.state & QStyle::State_MouseOver) && (opt.features & QStyleOptionToolButton::MenuButtonPopup))
            ||
           ((isChecked()) && (opt.features & QStyleOptionToolButton::MenuButtonPopup)) )//checked
    {
        p.save();
        p.setPen(border_color_);
        p.setBrush(Qt::NoBrush);
        p.drawRect(opt.rect.adjusted(0,0,-1,-1));
        p.drawRect(icon_rect_);
        p.restore();
    }
    drawIconAndLabel(p,opt);
}

void RibbonButton::drawIconAndLabel(QPainter& p, const QStyleOptionToolButton& opt)
{
    if(size_ == kLarge)
    {
        QRect rect = opt.rect;
        int shiftX = 0;
        int shiftY = 0;
        if (opt.state & (QStyle::State_Sunken | QStyle::State_On))
        {
            shiftX = style()->pixelMetric(QStyle::PM_ButtonShiftHorizontal, &opt, this);
            shiftY = style()->pixelMetric(QStyle::PM_ButtonShiftVertical, &opt, this);
        }
        bool hasArrow = opt.features & QStyleOptionToolButton::Arrow;
        if (((!hasArrow && opt.icon.isNull()) && !opt.text.isEmpty())
            || opt.toolButtonStyle == Qt::ToolButtonTextOnly)
        {
            int alignment = Qt::AlignCenter | Qt::TextShowMnemonic;
            if (!style()->styleHint(QStyle::SH_UnderlineShortcut, &opt, this))
                alignment |= Qt::TextHideMnemonic;
            rect.translate(shiftX, shiftY);
            p.setFont(opt.font);
            style()->drawItemText(&p, rect, alignment, opt.palette,opt.state & QStyle::State_Enabled, opt.text, QPalette::ButtonText);
        }
        else
        {
            QPixmap pm;
            QSize pmSize = opt.iconSize;
            if (!opt.icon.isNull())
            {
                QIcon::State state = opt.state & QStyle::State_On ? QIcon::On : QIcon::Off;
                QIcon::Mode mode;
                if (!(opt.state & QStyle::State_Enabled))
                    mode = QIcon::Disabled;
                else if ((opt.state & QStyle::State_MouseOver) && (opt.state & QStyle::State_AutoRaise))
                    mode = QIcon::Active;
                else
                    mode = QIcon::Normal;
                pm = opt.icon.pixmap(window()->windowHandle(), opt.rect.size().boundedTo(opt.iconSize), mode, state);
                pmSize = pm.size() / pm.devicePixelRatio();
            }

            if (opt.toolButtonStyle != Qt::ToolButtonIconOnly)
            {
                p.setFont(opt.font);
                QRect pr = rect,
                tr = rect;
                int alignment = Qt::TextShowMnemonic;
                if (!style()->styleHint(QStyle::SH_UnderlineShortcut, &opt, this))
                    alignment |= Qt::TextHideMnemonic;

                if (opt.toolButtonStyle == Qt::ToolButtonTextUnderIcon)
                {
                    pr.setHeight(pmSize.height() + 6);
                    tr.adjust(0, pr.height() - 1, 0, -1);
                    pr.translate(shiftX, shiftY);
                    if (!hasArrow) {
                        style()->drawItemPixmap(&p, pr, Qt::AlignCenter, pm);
                    }
                    else
                    {
                        drawArrow(style(), &opt, pr, &p, this);
                    }
                    tr.adjust(0,3,0,0);
                    alignment |= Qt::AlignHCenter | Qt::AlignTop;
                }
                else
                {
                    pr.setWidth(pmSize.width() + 8);
                    tr.adjust(pr.width(), 0, 0, 0);
                    pr.translate(shiftX, shiftY);
                    if (!hasArrow) {
                        style()->drawItemPixmap(&p, QStyle::visualRect(opt.direction, rect, pr), Qt::AlignCenter, pm);
                    } else {
                        drawArrow(style(), &opt, pr, &p, this);
                    }
                    alignment |= Qt::AlignLeft | Qt::AlignVCenter;
                }
                tr.translate(shiftX, shiftY);
                style()->drawItemText(&p, QStyle::visualRect(opt.direction, rect, tr), alignment, opt.palette,
                             opt.state & QStyle::State_Enabled, opt.text, QPalette::ButtonText);
            }
            else
            {
                rect.translate(shiftX, shiftY);
                if (hasArrow) {
                    drawArrow(style(), &opt, rect, &p, this);
                }
                else
                {
                    style()->drawItemPixmap(&p, rect, Qt::AlignCenter, pm);
                }
            }
        }
    }
    else
    {
        QPixmap pm;
        QSize pmSize = opt.iconSize;
        if (!opt.icon.isNull())
        {
            pm = DrawHelper::iconToPixmap(opt.icon,this,&opt,opt.rect.size().boundedTo(opt.iconSize));
            pmSize = pm.size() / pm.devicePixelRatio();

            if (opt.toolButtonStyle != Qt::ToolButtonIconOnly)
            {
                p.save();
                p.setFont(opt.font);

                QRect pr = icon_rect_;
                QRect tr = opt.rect.adjusted(pr.width(),0,-1,0);
                int alignment = Qt::TextShowMnemonic;
                if (!style()->styleHint(QStyle::SH_UnderlineShortcut, &opt, this))
                {
                    alignment |= Qt::TextHideMnemonic;
                }

                if (opt.toolButtonStyle != Qt::ToolButtonTextUnderIcon){
                    style()->drawItemPixmap(&p, QStyle::visualRect(opt.direction, opt.rect, pr), Qt::AlignCenter, pm);
                    alignment |= Qt::AlignLeft | Qt::AlignVCenter;
                }

                style()->drawItemText(&p, QStyle::visualRect(opt.direction, opt.rect, tr), alignment,
                                      opt.palette,opt.state & QStyle::State_Enabled, opt.text,QPalette::ButtonText);
                p.restore();
            }
            else
            {
                style()->drawItemPixmap(&p, opt.rect, Qt::AlignCenter, pm);
            }
        }
        else
        {
            int alignment = Qt::TextShowMnemonic;
            if (!style()->styleHint(QStyle::SH_UnderlineShortcut, &opt, this))
            {
                alignment |= Qt::TextHideMnemonic;
            }
            style()->drawItemText(&p, QStyle::visualRect(opt.direction, opt.rect, opt.rect.adjusted(2,1,-2,-1)), alignment, opt.palette,
                                             opt.state & QStyle::State_Enabled, opt.text,
                                             QPalette::ButtonText);
        }

        if (opt.features & QStyleOptionToolButton::HasMenu)
        {
            QStyleOptionToolButton tool = opt;
            tool.rect = opt.rect.adjusted(opt.rect.width()-8,0,0,0);
            style()->drawPrimitive(QStyle::PE_IndicatorArrowDown, &tool, &p, this);
        }
    }
}

void RibbonButton::drawArrow(const QStyle *style, const QStyleOptionToolButton *toolbutton, const QRect &rect, QPainter *painter, const QWidget *widget)
{
    QStyle::PrimitiveElement pe;
    switch (toolbutton->arrowType) {
    case Qt::LeftArrow:
        pe = QStyle::PE_IndicatorArrowLeft;
        break;
    case Qt::RightArrow:
        pe = QStyle::PE_IndicatorArrowRight;
        break;
    case Qt::UpArrow:
        pe = QStyle::PE_IndicatorArrowUp;
        break;
    case Qt::DownArrow:
        pe = QStyle::PE_IndicatorArrowDown;
        break;
    default:
        return;
    }
    QStyleOption arrowOpt = *toolbutton;
    arrowOpt.rect = rect;
    style->drawPrimitive(pe, &arrowOpt, painter, widget);
}

} // namespace gui
} // namespace open_edi
