#ifndef RIBBON_BUTTON_H
#define RIBBON_BUTTON_H

#include <QToolButton>

namespace open_edi {
namespace gui {

class RibbonButton : public QToolButton {
    Q_OBJECT
  public:
    enum ButtonSize{
        kLarge,
        kSmall,
        kMini
    };

    explicit RibbonButton(QWidget* parent = nullptr);
    virtual ~RibbonButton();
    void setButtonSize(const ButtonSize &size);

  signals:
    void menuActionClicked(QMouseEvent* e);

  protected:
    bool event(QEvent* e);
    void paintEvent(QPaintEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void focusOutEvent(QFocusEvent* e);
    void leaveEvent(QEvent* e);
    void resizeEvent(QResizeEvent* e);
    void mouseMoveEvent(QMouseEvent* e);
    bool hitButton(const QPoint &pos) const;

    void paintLargeButton(QPaintEvent* e);
    void paintSmallButton(QPaintEvent* e);
    void paintMiniButton(QPaintEvent* e);
    void drawIconAndLabel(QPainter& p, const QStyleOptionToolButton& opt);

private:
    static void drawArrow(const QStyle *style, const QStyleOptionToolButton *toolbutton,
                          const QRect &rect, QPainter *painter, const QWidget *widget = 0);

private:
    ButtonSize size_{kSmall};
    bool menu_button_pressed_{false};
    bool mouse_on_control_{false};
    QRect icon_rect_;
    QColor border_color_{242,202,88};
};

} // namespace gui
} // namespace open_edi
#endif // RIBBON_BUTTON_H
