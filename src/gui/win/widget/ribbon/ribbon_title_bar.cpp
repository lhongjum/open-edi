#include "ribbon_title_bar.h"

#include <QHBoxLayout>
#include <QMouseEvent>
#include <QApplication>

#include "quick_access_bar.h"
#include "util/util.h"

namespace open_edi {
namespace gui {

RibbonTitleBar::RibbonTitleBar(QWidget* parent)
  : QWidget(parent), title_(new QLabel)
  , left_accessbar_(new QuickAccessBar){
    setObjectName("RibbonTitleBar");
    init();
}

RibbonTitleBar::~RibbonTitleBar() {
}

void RibbonTitleBar::init() {
    setWindowFlags(Qt::FramelessWindowHint);

    icon_ = new QLabel(this);
    icon_->setFixedHeight(22);
    icon_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    icon_->setScaledContents(true);

    title_ = new QLabel(this);
    title_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    skin_ = new QToolButton(this);
    skin_->setFixedSize(27, 22);
    skin_->setObjectName("SkinButton");
    skin_->setIcon(QIcon(QString::fromStdString(open_edi::util::getResourcePath())+"tool/skin.png"));

    minimize_bt_ = new QToolButton(this);
    minimize_bt_->setFixedSize(27, 22);
    minimize_bt_->setObjectName("minimizeButton");
    minimize_bt_->setIcon(QIcon(QString::fromStdString(open_edi::util::getResourcePath())+"tool/min.png"));

    maximize_bt_ = new QToolButton(this);
    maximize_bt_->setFixedSize(27, 22);
    maximize_bt_->setObjectName("maximizeButton");
    maximize_bt_->setIcon(QIcon(QString::fromStdString(open_edi::util::getResourcePath())+"tool/max.png"));

    close_bt_ = new QToolButton(this);
    close_bt_->setFixedSize(27, 22);
    close_bt_->setObjectName("closeButton");
    close_bt_->setIcon(QIcon(QString::fromStdString(open_edi::util::getResourcePath())+"tool/close.png"));

    QHBoxLayout* layout = new QHBoxLayout();
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(icon_);
//    layout->addWidget(left_accessbar_);
    layout->addStretch(1);
    layout->addWidget(title_);
    layout->addStretch(1);
    layout->addWidget(skin_);
    layout->addWidget(minimize_bt_);
    layout->addWidget(maximize_bt_);
    layout->addWidget(close_bt_);
    setLayout(layout);

    setProperty("RibbonTitleBar", true);
    setObjectName("RibbonTitleBar");

    connect(minimize_bt_, &QToolButton::clicked, this, &RibbonTitleBar::onClicked);
    connect(maximize_bt_, &QToolButton::clicked, this, &RibbonTitleBar::onClicked);
    connect(close_bt_, &QToolButton::clicked, this, &RibbonTitleBar::onClicked);
}

void RibbonTitleBar::setWindowTitle(const QString& title) {
    title_->setAlignment(Qt::AlignCenter);
    title_->setText(title);
}

void RibbonTitleBar::setTitleBarIcon(const QString& icon){
    icon_->setPixmap(QPixmap(icon));
}

void RibbonTitleBar::addQuickAction(QAction* action) {
    left_accessbar_->addAction(action);
}

QAction* RibbonTitleBar::addQuickAction(const QIcon& icon, const QString& text) {
    return left_accessbar_->addAction(icon, text);
}

void RibbonTitleBar::mouseDoubleClickEvent(QMouseEvent* e){
    Q_UNUSED(e)

    maximize_bt_->click();
}

void RibbonTitleBar::mousePressEvent(QMouseEvent* e){
    if(e->button() == Qt::LeftButton){
        left_button_pressed = true;
        start_ = e->globalPos();
    }
}

void RibbonTitleBar::mouseMoveEvent(QMouseEvent* e){
    if(left_button_pressed){
        parentWidget()->parentWidget()->move(parentWidget()->parentWidget()->geometry().topLeft()+e->globalPos() - start_);
        start_ = e->globalPos();
    }
}

void RibbonTitleBar::mouseReleaseEvent(QMouseEvent* e){
    if(e->button() == Qt::LeftButton){
        left_button_pressed = false;
    }
}

bool RibbonTitleBar::eventFilter(QObject* obj, QEvent* e){
    switch (e->type())
    {
    case QEvent::WindowTitleChange:{
        QWidget* widget = qobject_cast<QWidget*>(obj);
        if(widget){
            title_->setText(widget->windowTitle());
            return true;
        }
        break;
    }
    case QEvent::WindowIconChange:{
        QWidget* widget = qobject_cast<QWidget*>(obj);
        if(widget){
            QIcon icon = widget->windowIcon();
            icon_->setPixmap(icon.pixmap(icon_->size()));
            return true;
        }
        break;
    }
    case QEvent::WindowStateChange:
    case QEvent::Resize:
        updateMaximize();
        return true;
    }
    return QWidget::eventFilter(obj, e);
}

void RibbonTitleBar::updateMaximize(){
    if(window()->isTopLevel()){
        bool maximize = window()->isMaximized();
        if(maximize){
            maximize_bt_->setToolTip(tr("Restore"));
            maximize_bt_->setProperty("maximizeProperty", "restore");
        }else{
            maximize_bt_->setProperty("maximizeProperty", "maximize");
            maximize_bt_->setToolTip(tr("Maximize"));
        }
        maximize_bt_->setStyle(QApplication::style());
    }
}

void RibbonTitleBar::onClicked(){
    QToolButton* button = qobject_cast<QToolButton*>(sender());
    if(window()->isTopLevel()){
        if(button == minimize_bt_){
            window()->showMinimized();
        }else if (button == maximize_bt_) {
            window()->isMaximized()? window()->showNormal() : window()->showMaximized();
        }else if(button == close_bt_){
            window()->close();
        }
    }
}

} // namespace gui
} // namespace open_edi
